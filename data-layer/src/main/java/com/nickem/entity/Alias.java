package com.nickem.entity;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Table("aliases")
public class Alias {

    @Id
    @Column("id")
    private String id;

    @Column("created_at")
    private Date createdAt;

    @Column("record_id")
    private String recordId;

    @Column("alias_generator")
    private String aliasGenerator;

    @Column("public_alias")
    private String publicAlias;

}
