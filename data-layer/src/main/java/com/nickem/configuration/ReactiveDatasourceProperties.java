package com.nickem.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "data-layer.datasource")
public class ReactiveDatasourceProperties {

    private String driver;
    private String url;
    private String name;
    private String schema;
    private String username;
    private String password;

    private PoolConfig pool;

    @Data
    static class PoolConfig {
        private int initialSize = 2;
        private int maxSize = 32;
    }

}
