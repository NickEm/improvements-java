package com.nickem.configuration;

import static io.r2dbc.pool.PoolingConnectionFactoryProvider.INITIAL_SIZE;
import static io.r2dbc.pool.PoolingConnectionFactoryProvider.MAX_SIZE;
import static io.r2dbc.spi.ConnectionFactoryOptions.DATABASE;
import static io.r2dbc.spi.ConnectionFactoryOptions.DRIVER;
import static io.r2dbc.spi.ConnectionFactoryOptions.PASSWORD;
import static io.r2dbc.spi.ConnectionFactoryOptions.PROTOCOL;
import static io.r2dbc.spi.ConnectionFactoryOptions.USER;

import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import io.r2dbc.spi.Option;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.r2dbc.connection.R2dbcTransactionManager;
import org.springframework.transaction.ReactiveTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@RequiredArgsConstructor
@EnableR2dbcRepositories(basePackages = "com.nickem.repository")
@EntityScan(basePackages = "com.nickem.entity")
@EnableTransactionManagement
public class DatasourceConfiguration extends AbstractR2dbcConfiguration {

    private final ReactiveDatasourceProperties properties;

    @Override
    @Bean("connectionFactory")
    public ConnectionFactory connectionFactory() {
        final ConnectionFactoryOptions baseOptions = ConnectionFactoryOptions.parse(properties.getUrl());
        return ConnectionFactories.get(ConnectionFactoryOptions.builder()
            .from(baseOptions)
            .option(DRIVER, properties.getDriver())
            .option(PROTOCOL, "postgresql")
            .option(USER, properties.getUsername())
            .option(PASSWORD, properties.getPassword())
            .option(DATABASE, properties.getName())
            .option(Option.valueOf("schema"), properties.getSchema())
            /* These properties make sense only if the provider is PoolingConnectionFactoryProvider*/
            .option(INITIAL_SIZE, properties.getPool().getInitialSize())
            .option(MAX_SIZE, properties.getPool().getMaxSize())
            .build());
    }

    @ReadingConverter
    static class ReadDateConverter implements Converter<LocalDateTime, Date> {

        @Override
        public Date convert(final LocalDateTime time) {
            return Date.from(time.atZone(ZoneId.systemDefault()).toInstant());
        }
    }

    @Bean
    @Override
    public R2dbcCustomConversions r2dbcCustomConversions() {
        final List<Converter<?, ?>> converterList = List.of(new ReadDateConverter());
        return new R2dbcCustomConversions(getStoreConversions(), converterList);
    }

    @Bean
    public ReactiveTransactionManager transactionManager(
            @Qualifier("connectionFactory") final ConnectionFactory connectionFactory) {
        return new R2dbcTransactionManager(connectionFactory);
    }

}
