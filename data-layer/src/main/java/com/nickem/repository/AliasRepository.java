package com.nickem.repository;

import com.nickem.entity.Alias;
import java.util.List;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface AliasRepository extends ReactiveCrudRepository<Alias, String> {

    Mono<Alias> findByRecordId(final String recordId);

    Flux<Alias> findByRecordIdIn(final List<String> recordIds);
}
