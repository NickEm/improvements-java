package com.nickem.service;

import com.google.common.collect.Iterables;
import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.dto.LogEntry;
import com.nickem.dto.mapper.LogEntryMapper;
import com.nickem.entity.Alias;
import com.nickem.repository.AliasRepository;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class AliasService {

    public static final int PARTITION_SIZE = 50;

    private final AliasRepository aliasRepository;

    public Mono<Alias> findByRecordId(final String recordId) {
        return aliasRepository.findByRecordId(recordId);
    }

    @SneakyThrows
    public Mono<Void> process() {
        final LogEntryMapper recordMapper = new LogEntryMapper();
        final CsvParser<LogEntry> csvParser = new CsvParser<>(recordMapper);

        final String inputName = "tntxtgcohmi-retrieved-2022-10-25-1.csv";
        final String outputName = "tntxtgcohmi-retrieved-2022-10-25-1-result.csv";
        final ClassPathResource classPathResource = new ClassPathResource(inputName);
        final CsvParseResult<LogEntry> result = csvParser.parseFile(classPathResource.getInputStream());
        return Mono.fromRunnable(() -> {
            log.info("Result size is {}", result.entities().size());
            final ClassPathResource resultResource = new ClassPathResource(outputName);
            try (CSVWriter writer = new CSVWriter(new FileWriter(resultResource.getFile()))) {
                int processedCount = 0;
                Iterable<List<LogEntry>> partitions = Iterables.partition(result.entities(), PARTITION_SIZE);

                for (List<LogEntry> entities : partitions) {
                    final List<String> recordIds = entities.stream().map(e -> e.getMessage().getRecordId().getValue())
                        .collect(Collectors.toList());
                    final List<Alias> aliases = aliasRepository.findByRecordIdIn(recordIds).collectList().toFuture().get();
                    for (LogEntry entity : entities) {
                        String[] entry = new String[2];
                        entry[0] = entity.getTimestamp();
                        entry[1] = aliases.stream().filter(e -> e.getRecordId().equals(entity.getMessage().getRecordId().getValue())).findFirst().orElseThrow(() -> new RuntimeException("NOP")).getPublicAlias();

                        writer.writeNext(entry);
                        log.info("Processed entries: {}", processedCount++);
                    }
                }
                writer.flush();
            } catch (IOException | InterruptedException | ExecutionException e){
                log.warn("Exception is raised on write", e);
            }
        });
    }



}
