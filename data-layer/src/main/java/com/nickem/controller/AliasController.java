package com.nickem.controller;

import com.nickem.entity.Alias;
import com.nickem.service.AliasService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/aliases")
public class AliasController {

    private final AliasService aliasService;

    @GetMapping
    private Mono<Alias> get(@RequestParam("recordId") final String recordId) {
        return aliasService.findByRecordId(recordId);
    }

    @PostMapping("/process")
    private Mono<Void> process() {
        return aliasService.process();
    }

}
