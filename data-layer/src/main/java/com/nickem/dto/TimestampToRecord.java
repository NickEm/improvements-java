package com.nickem.dto;

import java.util.Date;
import lombok.Data;

@Data
public class TimestampToRecord {

    Date timestamp;
    String alias;

}
