package com.nickem.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuditLog {

    @JsonProperty("@type")
    private String type;

    private RecordId recordId;

    private String recordType;
    private String actionType;
    private Object routeId;
    private String phase;
    private String hostName;
    private String aliasGenerator;
    private Object tags;

}
