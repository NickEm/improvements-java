package com.nickem.dto.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;
import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;
import com.nickem.dto.AuditLog;
import com.nickem.dto.LogEntry;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Set;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogEntryMapper implements CsvRecordMapper<LogEntry> {

    private static final ObjectMapper jsonMapper = new ObjectMapper();
    private static final String TIMESTAMP = "timestamp";
    private static final String MESSAGE = "message";

    private static final Set<DateTimeFormatter> SUPPORTED_FORMATS = ImmutableSet.of(
//        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"),
//        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"),
//        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"),
//        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS"),
//        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")/*,
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")*/
    );

    @SneakyThrows
    public LogEntry map(String[] fields, CsvColumnMapper mapper) {
        final String timestamp = fields[mapper.getIndexByFieldName(TIMESTAMP)];
        final AuditLog message = jsonMapper.readValue(fields[mapper.getIndexByFieldName(MESSAGE)], AuditLog.class);
        return new LogEntry(timestamp, message);
    }

    public Date convertDate(String date) {
        Date result = null;
        for (DateTimeFormatter format : SUPPORTED_FORMATS) {
            try {
                result = Date.from(LocalDateTime.parse(date, format).toInstant(ZoneOffset.UTC));
                break;
            } catch (DateTimeParseException ex) {
                log.warn("Unable to parse [{}] with format [{}]", date, format);
            }
        }

        if (result == null) {
            throw new IllegalArgumentException(String.format("Exception while parsing date from [%s]", date));
        }
        return result;
    }

}
