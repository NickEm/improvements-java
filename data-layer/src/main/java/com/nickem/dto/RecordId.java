package com.nickem.dto;

import lombok.Data;

@Data
public class RecordId {

    private String value;

}
