package com.nickem.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LogEntry {

    String timestamp;
    AuditLog message;

}
