##### Completable future
* [Tutorial with chaining explanation](https://www.deadcoderising.com/java8-writing-asynchronous-code-with-completablefuture/)
* [AllOfImprovements](https://4comprehension.com/improving-completablefutureallof-anyof-api-java-methods/)


##### Selenium
* [Selenium wait ](https://stackoverflow.com/questions/16057031/webdriver-how-to-wait-until-the-element-is-clickable-in-webdriver-c-sharp/16057521)
* [Selenide cheet sheet](https://gist.github.com/mkpythonanywhereblog/947633ba1bf0bc239639)


#### Useful

* [25-useful-libraries-to-known-java](https://towardsdatascience.com/25-lesser-known-java-libraries-you-should-try-ff8abd354a94)

####

* [reactive-programming-with-spring](https://dzone.com/articles/an-introduction-to-reactive-programming-with-sprin) 


### Generate mock server for swagger Open API definition
* https://github.com/danielgtaylor/apisproutF
* https://stoplight.io/open-source/prism

### TODO
* https://www.codewars.com/kata/5a62173ee626c5f0e40000e9
* LeetCode 1302. Deepest Leaves Sum (Algorithm Explained)
* https://www.youtube.com/watch?v=hM9tzzlRcnM&list=PLU_sdQYzUj2keVENTP0a5rdykRSgg9Wp-&index=178

#### Java File Encyption
* https://proandroiddev.com/security-best-practices-symmetric-encryption-with-aes-in-java-7616beaaade9
* https://www.veracode.com/blog/research/encryption-and-decryption-java-cryptography
  * KeyGenerator.getInstance("AES") 
* https://jenkov.com/tutorials/java-cryptography/index.html




TODO: Correct hashcode implementation
```java
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + this.getTransaction().getTransactionAmount().hashCode();
        hash = 31 * hash + this.getTransaction().getTransactionId().hashCode();
        hash = 31 * hash + this.getTransaction().getWalletReference().hashCode();
        return hash;
    }
```