```java
        int pageSize = 100;
        int currentItems = 0;
        long chatId = 0L;
        Set<String> uniqueNames = new TreeSet<>();
        while (currentItems < maxItems) {
            final List<RandomUser> randomUsers = getRandomUsers(httpClient, pageSize);

            final List<UserRequest> resultEntities = new ArrayList<>();
            for (RandomUser e : randomUsers) {
                final String name = prepareName(e);
                if (!uniqueNames.contains(name)) {
                    resultEntities.add(UserRequest.builder()
                        .botName(StringUtils.abbreviate(e.getEmployment().getTitle(), 64))
                        .recipient(name)
                        .chatId(chatId++)
                        .language(prepareLanguage())
                        .status(RecipientStatus.ACTIVE)
                        .actionState(prepareActionState())
                        .build()
                    );
                    uniqueNames.add(name);
                }
            }

            currentItems = writeToFile(maxItems, filePath, currentItems, resultEntities);
        }
```