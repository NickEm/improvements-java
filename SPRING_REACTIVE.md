#### Parallel calls to fetch the data of the same type
```
public Flux<User> fetchUsers(List<Integer> userIds) {
    return Flux.fromIterable(userIds)
            .parallel()
            .runOn(Schedulers.elastic())
            .flatMap(i -> findById(i))
            .ordered((u1, u2) -> u2.getId() - u1.getId());
}
```

#### Parallel calls to fetch the data of the different type
```
Mono<User> user = findById(userId).subscribeOn(Schedulers.elastic());
Mono<Department> department = getDepartmentByUserId(userId).subscribeOn(Schedulers.elastic());
return Mono.zip(user, department, userDepartmentDTOBiFunction);
```