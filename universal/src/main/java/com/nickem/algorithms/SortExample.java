package com.nickem.algorithms;

public class SortExample {

    public static void main(String[] args) {
        int[][] table = new int[][]{{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (i <= j) {
                    final int prev = table[j][i];
                    table[j][i] = table[i][j];
                    table[i][j] = prev;
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(table[i][j] + ",");
            }
            System.out.println();
        }

    }
}
