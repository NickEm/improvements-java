package com.nickem.zip;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.util.Base64;
import java.util.Objects;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class ZipExample {

  private final static String base64context = "H4sIAAAAAAAA/1TXdzQbDKPHcSKxN6HGg9pb1Ir9ELW1eKzaRVE1iyg1Uwmt2ptW1SxBERUUFatitGbtkdSqUSNKjco995577/u+//6+/3/O+UEQTK8AlFQQBJMdNZ00yhh1xkRJQ1mKYDICIphgEASTLgQBsvmPBLoNRID0IAiQNgQBUqdm+N/ESE8JKEUAf0IQwB0IPzX9/82UVBBmIO0ThWfp8BvcswAALQUQARz6ODyff0R//Wf+CLfmZeqKio8Indi/BTXhd2Sb0eBnD1vLhcTL/npJVYgwpORtNInl0Z27T3uXrP9UqYoXJEpUwL4tJPXFJ5S7D19WOd7Muba9oa+o6bw7AQngIJStv83cNB8nHDTcPESqi0H5wOgo4rEbbuDVtFr96BXKTlq9VkUff8SqPLM3KJ/XdWQO86s5KLjeHqVuT2HBxCBsu6W+Yewb5lDsa/zbeCHNb0sYYq4YrqvGUE2PwEm8sybGwt5mdWVj2XRlR+KVy/Wenj3OYDuOVmCefNBEb+Da9uetpUjhgtc/SfuNKmCBpJMsubWuYlqiFIcJrl2r5ES7LHruffCChAjnDugDjPZi05EbR5FdaDLaqHSALkkq1hQjNcV7qzZqka6C5fXORPisKmqnpL838kZgHhte73tOf8GwkM7tk/bUKwvr5m3coqTezj+Ap/HRL+Tv7I4JlX3qJd8EaGnkFg70o59s7E2DhH408kS5MRyUc3VUW/Rtrrzbf8/ekzle6roOXbxK10cqbh88dBahqxyFh2iaknWUwedmOLMoo4DdmUwVES9Wi5DdvkgNKhF63iID3o95rj8lciXFW7iC2azqJWK3JndhqGVFDM8bCRNJ3eZWB4uDu8aSZC4NWLBNqDUu2+L3Z6R1zgwRnny8KA/SDq9esvjlfVTWXKqZaXc/0DcEE8VPwfyI82mVu4bPnVzKuL9YOhy8jXR8L7V6fBlaeXt3NaSVahYChRc0T7eS12mhAGPZQ3mSwegS+fM92SxxOa3EaeezNB968R3+UnSVmaS0ySSx6p+mGpPCudeKFxRYhqBEweEwKv1tpdvb99pS7yh3kdLN/Vgj1iUmeWHPBJYXitUjNZotPNtnFMWxTTlG3G5jNOc5z84I1Q3nsEglWYBH4H5yLnf6xK/D0UsFJuPXL7oGS6UqNnXyQNqDLcsE+JKY6dRN6I1El9Vhv2gBJ+ZZWxdRTKJOFEv+4iW5Te0mofeD52aD+n3A5YMneI9N50wf68qplizFoAPMSYtMkP+1ZUkfcRqDjpkF4/V/TmR2CxC0JUm9AVQaj6gcKiXVM7gNmjZyxLgNoMt/lsZ11n9ZJkWj5kFF4Ryxo5fbA3teG1z1D+f3SAwiG3p47ZFFMJCeMQ0z8xkj+ieXfpt4QgG/f0qAX9TuC3Nv+8ZYMLCvUnIx3sI9GyI+YqYBjra6gT7rZHBnMBLZ5hSEco8ZyXmdjPiAlV09t7SWFy7CCfc25hN+74bkpsEZVT2MS3XbzzRrc8g0aTwF6T0RO4YNL8+ibHawnKmJL2c9nx/zPx8usxtyI0ilnAOi7mbUFIu+CwPVMKVQE4EiZiOvVoKVhKg4b9MdGtLNMxRhR1YfYsviYVALrrfvKoDzp8Iz3LHHxqfjy9eKbnxY9Ys82NHN1trIpol89Fq9dbFs0gOVNLnCDWtpiZc7LBQu2xoF1c38YyHwwzMlPdHHTOrhF5cJHyxRLB302IpeUheOZk/jqXswDRI66Hs3xZousUTktyWZ/7eHpVTd4JGRZJBVayTjoWNikxxUqiHCXlLxi02qRtxO5/H0rezfaphQNjJyTsToTRjxh75qlG+0v6qxjT1Z+6G9KDZGneXixYBfqyIUwvb/OtJxKgIFABSKEJF/TWBFLiBnoNKecCk6ej9ksCn10witFINAdTgEQfP9X65SlyJopiEImnEAJQUEQYP/N3Ehkv+GKTWEF8gp/Q3I063dBNX9Nrg2fJrPJFtSxvbfsMYjaDJafeOTHyf0cVdz63bYOSPelMpeZBTcUQTzeuzJq9MVSTs+eJb7ulOwrUfZWK2dFYfU8MH8UKxk/BtT9tW8kFKLSqylmTQ0Ef3eck67nSqxzy5vdTw8trLfarj8uPJva5hyxACwb3nfJaNK5K6IgZNSW/Pzp1KV58Nrlx6vrX51NXbH/vG6QHNwR5PCX3NpUUFKOjpti9qMHWQFCOSLgpImajzPwSWUeceKKj+r4OVXgotgujUazTRORCXQNgwzHOTU+fH/HvUKEtcqbtD6aRilGnh87IzbX3TE2oYQYGArsdgPm8sHgXsAw7JJeUT76liir0HDcMSm09G6OnzTgSg1Ap74OvD2IDiv5sheyjh5gFfrp1CwaYCZ7pJQvWuF5grANe5z/I9G+HvvCFBT0IPdY1FLtZETnFOZTil22thmV0FlY4b14+mMEmu289O1T9Zx7qYW/Kd5ZAnNlWwJ1R7wh/04c8M6TXOluoiL1q35TLTM27bPD+PqFXVR+l981dMBKmur3HqrHr0cFWsfo1BXufkFEAKpFl/MRM7B7lmV6O1kMwizUnH19+Wm1Vpb6ivQt0ocoaSSfTc/ZwWBCXx4T2n/HPv7Ja+y/lKqsQ4H7VcaN+I66NxwA7TbQN74WxhWkBRlVqpnitxGOSWFbiWyE/vQRBT33lw5xo7HPNEjf3aWBdnVc9jn/fj8Sy3BZ69KEpm/lSCYedgtoy4hAJmVnWE+lXrm2+Yr71+6IfFV/S8Ki0dTb7x+IUPEElk6BWugCXHUHaECMlSRzXXztlycr3O5r7K52eBuq1C3sRTX5rmc20gD/+DlF8tDxOoMD0MyMkuEYne4nyWYhdCDLxstOmi/YOwBP2ktl9+83hgJ6Gh0VlY+4UwUk1VkNnJmcrBfDn3XZWFalQDuRcYI8tCxjAcMjYrPl59Sz9LMUvdwfeRo1tl5aWD34/0iSGam9s0d1/rzD822lANBHLNeU0VbflmLz6OEutQksZ80bzi77pXxJLzwvnVnDPNh3g7euvE4arJ75dCsEe8WgatlLjXIRB20ftcJNd7PYhKQyXOcVVGxq/dkMzmhj4QlNXzdCNuNar1rFNdGdse0NChRW6T4toh1P3U0JJ6OG22XWKeT2ssMu6IL8v4+iNfyuj33NLMDiD5y8c7+0swneHOzOVvNFwRdXxx8cLP0zmQ+eFKJiVBU3mV/saup9bUA0zAWKkL0UZmkmANQyecgGQuNHjpFlNJytbrPdkzqoe77yMC+Vfihl+kgBajwFYSVlNOn5IHbpOuO7mr/O3HOm+vER4lUveyiUb7rd3po7ikw3L0UsrNvyJDg78Py3Z89k5NVTrvVcq1vpO/vH4jubI/nF+JRYPcTCRbQ50P62NgWXb5ouhh8sxNssPI7KX57fu/8H6tmLMcrFnBR8ivHwK0PM++dbZb5fWn912/3P45ackYikfFRwT51x9UcOolsNxi4pxknyzqTp3Qjqs2evz4VDXMumtvaEhmz1fEB/By2kcEH59jUiZZ1yzV6ktpbFpI6F7qt3MIyL6iVjmDOlTIcPM7lyLmzSL416lcMbi4ujUeJtPsxA1hanU9O5vEK+Q71ZitxDYV8BS72RjY8/s5nKE8nf5wS3szJmxwPGVNod7D1jC+7i5cj9Th2JtvL/0zyuaq0WHgd15uxHhC8Fl42PtaM6novPK4+FoZAmwzamK+LKffttQXrfvVBvfGI1IK3A9ktFY69wiAyU7/sHnrDm82LVCfxOTJhhnj+4Da/rUf17OEG1N1/wuz/KJBVGSyXAhsoTgbxJ8AF3uWdahpLp3BhGokRFQc211idihpxW7OaxSrpnph/6l5wlkLtQ2qKY9K8iJvfsMZKIurtwFeTE47DrE8JMP3HT9ZAglitvhkp2F0cAl6ZEwegn8d6amUYik/HxkbpOaqBeZObBd6gg7VeHsss1eraHVODG0oWTdUsxhGY9BGhd1usQ+b2qyuxAoIU5iFBBl+TxZstAkgDJ+Y7MGMP4XSBQVNX983aP+dj7uTz87/5TE+Hffj6Re/vwTVCceumLrtDFOsUCalZwt3cLYI/XLyyHROnRtX3pvuErQC3/d4GgH8nr1KD+zl9dvVdb7DuLm8YHF5GeNgLqdzL/fad7Vn1lGf6Z3PBVOWf7Qjb2Xapq2ezY9XqgLaqq6Q6WAXhy8TmSNIm/fmuTcVbBf8B4SBp9CGkb9hHuHfru1D+mKqa5Dt0Ih+gziSFo5LqUGlbwa8FOv/Sa+6k9rHf77yU0DhD0cOUQb0tDTqa2WXb65+P4aQJQd8Xl/j99QuUdQz9C8P9ulYz+PyHpnvgJSZRM00Ka/t7VZdXobr2PgJYVh2TTzAbLZg71EyXbTv6AC3puMl4LV/GHjgk5Do0RBnTFOa2GjlDyfcGOKHhVUxA+Tig6whsAv3JP6Bohs7Z6s89DHpsusXHDA8i4LcV8ANTsq6px2B5Eblk9xz2y+HisX55vVpganzsPwn5kYeVtZN5QyjmpZpfPoqa7LW3BJUkmT6V80OthYO3WE7HEfKqgWr95Fe54llBTMQqrVz7IxsGnWFpw+i8hhZoFEQbchNCRw2SYaYC8IIogJxfr4to3mfla+wwWH880igHD0093QdyplbgJyKlduH6qzTFI4Fnl34FLv/zlf4LAAD//wEAAP//6MamgqANAAA=";

  public static void main(String[] args) throws IOException {
//    new ZipExample().decompressToFile(Base64.getDecoder().decode(base64context));
    new ZipExample().decompressFromFileToFile();

  }

  private void decompressFromFileToFile() throws IOException {
    final File input = new File(getClass().getClassLoader().getResource("keyStore-compressed.p12").getPath());
//    final String stringInput = FileUtils.readFileToString(input);
//    final byte[] decodedBytes = Base64.getDecoder().decode(stringInput);
//    decompressToFile(decodedBytes);
    final byte[] bytes = FileUtils.readFileToByteArray(input);
    final byte[] decodedBytes = Base64.getDecoder().decode(bytes);
    decompressToFile(decodedBytes);
  }

  public static byte[] compress(final String stringToCompress) {
    if (Objects.isNull(stringToCompress) || stringToCompress.length() == 0) {
      return null;
    }

    try (final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final GZIPOutputStream gzipOutput = new GZIPOutputStream(baos)) {
      gzipOutput.write(stringToCompress.getBytes(UTF_8));
      gzipOutput.finish();
      return baos.toByteArray();
    } catch (IOException e) {
      throw new UncheckedIOException("Error while compression!", e);
    }
  }

  public static String decompressToString(final byte[] compressed) {
    if (Objects.isNull(compressed) || compressed.length == 0) {
      return null;
    }

    try (final GZIPInputStream gzipInput = new GZIPInputStream(new ByteArrayInputStream(compressed));
        final StringWriter stringWriter = new StringWriter()) {
      IOUtils.copy(gzipInput, stringWriter, UTF_8);
      return stringWriter.toString();
    } catch (IOException e) {
      throw new UncheckedIOException("Error while decompression!", e);
    }
  }

//    public static File base64ToBinary() throws IOException {
//    final InputStream originalStream = Files.asByteSource(new File("/etc/camel/resources/i-resource-000/keyStore.p12"))
//        .openStream();
//    final byte[] originalBytes = ByteStreams.toByteArray(originalStream);
//    final byte[] decodedOriginalBytes = Base64.getDecoder().decode(originalBytes);
//    File file = new File(UUID.randomUUID().toString());
//    Files.write(decodedOriginalBytes, file);
//    return file;
//  }

  public void decompressToFile(final byte[] compressed) {
    final File file = new File(getClass().getClassLoader().getResource("keyStore-decompressed.p12").getPath());
    try (final GZIPInputStream gzipInput = new GZIPInputStream(new ByteArrayInputStream(compressed));

        final FileWriter stringWriter = new FileWriter(file)) {
      IOUtils.copy(gzipInput, stringWriter, UTF_8);
    } catch (IOException e) {
      throw new UncheckedIOException("Error while decompression!", e);
    }
  }

}
