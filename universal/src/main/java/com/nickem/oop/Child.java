package com.nickem.oop;

public class Child extends Parent {

    static {
        System.out.println("Child static block");
    }

    {
        System.out.println("Child non-static block");
    }

    public Child() {
        System.out.println("Child constructor");
    }

}
