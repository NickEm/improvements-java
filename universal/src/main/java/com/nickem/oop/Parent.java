package com.nickem.oop;

public class Parent extends GrandParent {

    static {
        System.out.println("Parent static block");
    }

    {
        System.out.println("Parent non-static block");
    }

    public Parent() {
        System.out.println("Parent constructor");
    }
}
