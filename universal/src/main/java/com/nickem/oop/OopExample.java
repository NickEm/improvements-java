package com.nickem.oop;

import com.nickem.executor.IExecute;

public class OopExample implements IExecute {

    @Override
    public void execute() {
        new Child();
    }

    public static void main(final String[] args) {
        new Child();
    }
}
