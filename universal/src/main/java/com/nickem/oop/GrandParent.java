package com.nickem.oop;

public class GrandParent {

    static {
        System.out.println("GrandParent static block");
    }

    {
        System.out.println("GrandParent non-static block");
    }

    public GrandParent() {
        System.out.println("GrandParent constructor");
    }
}
