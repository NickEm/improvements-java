package com.nickem.sectiontask;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SectionCountExample {

    public static void main(String[] args) {

        int[] as = new int[]{0,  4, 11, 13, 16};
        int[] bs = new int[]{10, 15, 12, 14, 18};

        List<Section> sections = new ArrayList<>();
        for (int i = 0; i < as.length; i++) {
            sections.add(new Section(as[i], bs[i]));
        }
        System.out.println("Count of unbounded sections is: " + new SectionCountExample().returnUnboundedSectionCount(sections));
    }

    private int returnUnboundedSectionCount(List<Section> sections) {
        if (sections != null) {
            int result = 0;
            sections.sort(Comparator.comparingInt(Section::getStart));

            if (sections.size() < 2) {
                result = sections.size();
            } else {
                result = 1;
                int max = sections.get(0).getEnd();
                for (int i = 1; i < sections.size(); i++) {
                    if (max < sections.get(i-1).getEnd()) {
                        max = sections.get(i-1).getEnd();
                    }
                    if (max < sections.get(i).getStart()) {
                        result += 1;
                    }
                }
            }
            return result;
        } else {
            throw new RuntimeException("Invalid input section list.");
        }
    }
}
