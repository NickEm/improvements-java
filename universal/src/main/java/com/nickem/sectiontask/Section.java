package com.nickem.sectiontask;

public class Section {

    private final int start;
    private final int end;

    public Section(int start, int end) {
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }
}
