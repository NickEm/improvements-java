package com.nickem.collections;

import com.nickem.executor.IExecute;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Mykola Morozov
 */
public class CollectionsExecutor implements IExecute {

    @Override
    public void execute() {
        compareTwoArrays();
    }

    public void compareTwoArrays() {
        LinkedList<String> list1 = new LinkedList<>();
        list1.add("Sting_1");
        list1.add("Sting_2");
        list1.add("Sting_3");
        LinkedList<String> list2 = new LinkedList<>();
        list2.add("Sting_1");
        list2.add("Sting_2");
        list2.add("Sting_3");
        System.out.println("Array comparison : " + compareTwoLinkedList(list1, list2));
    }

    private boolean compareTwoList(List<String> list1, List<String> list2) {
        if (list1 == null || list2 == null || list1.size() != list2.size()) {
            return false;
        } else {
            for (int i = 0; i < list1.size(); i++) {
                if (!list1.get(i).equals(list2.get(i))) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean compareTwoLinkedList(LinkedList<String> list1, LinkedList<String> list2) {
        if (list1 == null || list2 == null || list1.size() != list2.size()) {
            return false;
        } else {
            Iterator iter1 = list1.iterator();
            Iterator iter2 = list2.iterator();
            while (iter1.hasNext()) {
                if (!iter1.next().equals(iter2.next())) {
                    return false;
                }
            }
            LinkedList<String> list3 = new LinkedList<>(list2);
            list1.removeLast();
            list2.removeLast();
            System.out.println(list1.size());
            System.out.println(list2.size());
            System.out.println(list3.size());
/*
            for (int i = 0; i < list1.size(); i++) {
                if (!list1.get(i).equals(list2.get(i))) {
                    return false;
                }
            }*/
        }

        return true;
    }
}
