package com.nickem.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.openjdk.jmh.annotations.Benchmark;

/**
 * Detailed description
 *
 * @author Mykola Morozov.
 */
@Slf4j
public class FileReadAndWrite {

    public static void main(String[] args) throws Exception {
//        File keyStoreFile = new File("littleproxy_keystore.jks");
        /*sqlReplaceEachIdToDefault();*/
//        playWithString();
//        cloneFileContent("/Users/mmorozov/vgs/fixtures/100MB-with-pan-and-headers.csv", 15, "1590MB-with-pan-and-headers.csv");
//        appendToFileContent("/Users/mmorozov/git-repos/own/file-management-service/fixtures/1060MB-with-pan-and-headers.csv", "15GB-with-pan-and-headers.csv");

        getFileSize("/Users/mmorozov/git-repos/own/improvements-java/universal/src/main/resources/file/smart-contract.md");
        getFileLength("/Users/mmorozov/git-repos/own/improvements-java/universal/src/main/resources/file/smart-contract.md");
//        getFileSize("/Users/mmorozov/vgs/fixtures/1590MB-with-pan-and-headers.csv");
//        copyFileLines("/Users/mmorozov/git-repos/own/improvements-java/converter/src/main/resources/csv/input/20K-with-pan-and-headers.csv",
//            9000, 500, "0.5K-with-pan-and-headers.csv");
//        byte[] strToBytes = "2720772757220859".repeat(62500).getBytes();
//        Files.write(Paths.get("/tmp", "1mb.json"), strToBytes);
    }

    private static void cloneFileContent(final String filePath, final int cloneMultiplier, final String outputName) throws Exception {
        Path resultPath = getOutputPath(filePath, outputName);

        File file = new File(filePath);
        String initialData = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        log.info("Length: {}", initialData.length());
        byte[] strToBytes = initialData.repeat(cloneMultiplier).getBytes();
        Files.write(resultPath, strToBytes);
    }

    private static void getFileLength(String filePath) throws Exception {
        File file = new File(filePath);
        String initialData = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        log.info("Length: {}", initialData.length());
    }

    private static Path getOutputPath(String filePath, String outputName) {
        final String pathToDirectory = FilenameUtils.getFullPath(filePath);
        return Paths.get(pathToDirectory + outputName);
    }

    private static void copyFileLines(String filePath, int skip, int limit, String outputName) throws Exception {
        Path resultPath = getOutputPath(filePath, outputName);
        final List<String> lines = Files.readAllLines(Path.of(filePath));
        final List<String> result = lines.stream().skip(skip).limit(limit).collect(Collectors.toList());
        Files.write(resultPath, result);
    }


    private static void appendToFileContent(final String filePath, final String fileName) throws Exception {
        final String pathToDirectory = FilenameUtils.getFullPath(filePath);
        File file = new File(filePath);
        String initialData = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        Path resultPath = Paths.get(pathToDirectory + fileName);

        Files.writeString(resultPath, String.valueOf(initialData), StandardOpenOption.APPEND);
    }

    private static void getFileSize(final String filePath) throws Exception {
        File imageFile = new File(filePath);
        long size = FileUtils.sizeOf(imageFile);
        int sizeFromStream = new FileInputStream(imageFile).available();
        System.out.println("FileUtils size is:       " + size);
        System.out.println("FileInputStream size is: " + sizeFromStream);
    }

    public static void createUniqueSetOfFileLines(File inputFile, File outputFile) {
        Set<String> resultSet = new TreeSet<>();
        try (BufferedReader in = new BufferedReader(new FileReader(inputFile))) {
            String line;
            while ((line = in.readLine()) != null) {
                resultSet.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (PrintWriter writer = new PrintWriter(new FileWriter(outputFile, true))) {
            resultSet.stream().forEach(writer::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
