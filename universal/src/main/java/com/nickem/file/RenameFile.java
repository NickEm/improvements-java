package com.nickem.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.commons.io.FilenameUtils;

/**
 * @author Mykola Morozov.
 */
public class RenameFile {

    private static final String UNDERSCORE = "_";
    private static final String DASH = "-";
    private static final String SHARP = "#";

    public static void main(String[] args) {
        final RenameFile instance = new RenameFile();
        instance.execute();
    }

    public void execute(){
        File file = new File("/Users/mmorozov/tmp/books");
//        File file = new File("/Users/mmorozov/tmp/solution-architect-associate");
        renameFile(file, false);
    }

    private void renameFile(File file, boolean justTraceChanges) {
        if (file != null && file.isDirectory()) {
            for (File f : file.listFiles()) {
                renameFile(f, justTraceChanges);
            }
        } else {
            try {
                String name = FilenameUtils.getBaseName(file.getName());
                String path = file.getParent() + File.separator;
                String extension = FilenameUtils.getExtension(file.getName());
                name = fixName(name);
                File newFile = new File(path + name + "." + extension);
                System.out.println("Old file " + file.getName());
                System.out.println("New file " + newFile.getName());

                if (!justTraceChanges) {
                    Files.move(file.toPath(), newFile.toPath());
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                //NOP
            }
        }
    }

    private String fixName(String name) {
        name = name.toLowerCase();
//        name = name.replaceAll("s04", "s04-");
        name = name.replaceAll(" ", DASH);
        name = name.replaceAll(":", DASH);
        name = name.replaceAll(",-", DASH);
        name = name.replaceAll("\\.-", DASH);
//        name = name.replaceAll("-e[d]", DASH);
//        name = name.replaceAll(" ", DASH);
//        name = name.replaceAll(UNDERSCORE, DASH);
        name = name.replaceAll("---", DASH);
        name = name.replaceAll("--", DASH);
//        name = name.replaceAll("\\(+.*\\)+", UNDERSCORE);
//        name = name.replaceAll("-", UNDERSCORE);
//        name = name.replaceAll("___", UNDERSCORE + "-" + UNDERSCORE);
//        if(name.substring(name.length() - 1).equals(UNDERSCORE)) {
//            name = name.substring(0, name.length() - 2);
//        }

        return name;
    }
}
