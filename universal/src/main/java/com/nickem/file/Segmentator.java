package com.nickem.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class Segmentator {

    public static void main(String[] args) throws IOException {
        final Segmentator instance = new Segmentator();
        final String resource = "file/smart-contract.md";
        final InputStream stream = instance.getClass().getClassLoader().getResourceAsStream(resource);
        final List<String> segments = instance.getSegments(stream, 3000);
        for (int i = 0; i < segments.size(); i++) {
            Path filePath = Paths.get("universal/src/main/resources/"
                + FilenameUtils.getPath(resource)
                + FilenameUtils.getBaseName(resource)
                + "-%s.".formatted(i)
                + FilenameUtils.getExtension(resource)
            );
            Files.writeString(filePath, segments.get(i));
        }

    }


    public List<String> getSegments(final InputStream stream, final int maxSegmentSize) throws IOException {
        List<String> result = new ArrayList<>();
        final String input = new String(stream.readAllBytes(), StandardCharsets.UTF_8);
        log.info("Processing input length: {}", input.length());
        final List<String> chapters = splitOnChapters(input);
        final List<String> optimalChapters = combineChaptersByLimit(chapters, maxSegmentSize);
        return optimalChapters;
    }

    public List<String> splitOnChapters(final String input) {
        final List<String> result = new ArrayList<>();
        final List<String> sentences = Arrays.asList(input.split(System.lineSeparator()));
        final Iterator<String> iterator = sentences.iterator();
        String currentSentence = "";
        while (iterator.hasNext()) {
            String current = iterator.next();
            if (StringUtils.isBlank(currentSentence)) {
                currentSentence = current + System.lineSeparator();
            } else {
                if (StringUtils.containsAny(current, "##", "###", "####", "#####")) {
                    result.add(currentSentence + System.lineSeparator());
                    currentSentence = current + System.lineSeparator();
                } else {
                    currentSentence += current + System.lineSeparator();
                }
            }
        }
        return result;
    }

    private List<String> combineChaptersByLimit(List<String> chapters, final int maxSegmentSize) {
        final List<String> optimalChapters = new ArrayList<>();

        StringBuilder currentOptimalChapter = new StringBuilder();
        for (final String current : chapters) {
            if (currentOptimalChapter.length() == 0 && current.length() <= maxSegmentSize) {
                currentOptimalChapter = new StringBuilder(current);
            } else if (currentOptimalChapter.length() + current.length() < maxSegmentSize) {
                currentOptimalChapter.append(current);
            } else if (current.length() > maxSegmentSize) {
                optimalChapters.add(currentOptimalChapter + System.lineSeparator());
                optimalChapters.addAll(splitBySentence(current, maxSegmentSize));
                currentOptimalChapter = new StringBuilder();
            } else if (currentOptimalChapter.length() + current.length() > maxSegmentSize) {
                optimalChapters.add(currentOptimalChapter + System.lineSeparator());
                currentOptimalChapter = new StringBuilder(current);
            }
        }

        final String optimalChapterString = currentOptimalChapter.toString();
        if  (optimalChapterString.length() > 0) {
            optimalChapters.add(optimalChapterString);
        }

        return optimalChapters;
    }

    private List<String> splitBySentence(String chapter, final int maxSegmentSize) {
        final List<String> result = new ArrayList<>();
        final String[] sentences = chapter.split(System.lineSeparator());

        StringBuilder currentChapter = new StringBuilder();
        for (String sentence : sentences) {
            /*In this if condition we assume for simplicity that sentence !> maxSegmentSize*/
            if (currentChapter.length() == 0) {
                currentChapter = new StringBuilder(sentence + System.lineSeparator());
            } else if (currentChapter.length() + sentence.length() >= maxSegmentSize) {
                result.add(currentChapter + System.lineSeparator());
                currentChapter = new StringBuilder(sentence);
            } else {
                currentChapter.append(sentence);
            }
        }
        result.add(currentChapter  + System.lineSeparator());

        return result;
    }

}
