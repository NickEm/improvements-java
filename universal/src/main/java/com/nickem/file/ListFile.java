package com.nickem.file;

import java.io.File;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ListFile {

    public static void main(String[] args) {
        File file = new File("/Users/mmorozov/Google Drive/My Drive/knowledge-base/architecture/a-cloud-guru-aws-associate/");
        final ListFile instance = new ListFile();
        final int countFiles = instance.countFiles(file, 0);
        log.info("Total number of files: [{}]", countFiles);
    }

    private int countFiles(final File file, int count) {
        if (file != null && file.isDirectory()) {
            log.info("Observing the file: [{}]", file.getName());
            for (File f : file.listFiles()) {
                if (f.isDirectory()) {
                    count += countFiles(f, count);
                } else {
                    count++;
                }
            }
        }
        return count;
    }

}
