package com.nickem.external;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class StreamGobbler implements Runnable {
    private InputStream inputStream;
    private CountDownLatch inputLatch;
    private CountDownLatch resultLatch;
    private String result;

    public StreamGobbler(InputStream inputStream, CountDownLatch inputLatch, CountDownLatch resultLatch) {
        this.inputStream = inputStream;
        this.inputLatch = inputLatch;
        this.resultLatch = resultLatch;
    }

    @Override
    public void run() {
        inputLatch.countDown();
        result = new BufferedReader(new InputStreamReader(inputStream)).lines()
            .collect(Collectors.joining("\n"));
        resultLatch.countDown();
    }

    public String getResult() {
        return result;
    }
}
