package com.nickem.external;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExecuteExternalProgram {

    public static final String SFTP_PASSWORD = "d122221b-08d9-4b90-9c31-7ec899151069";

    /**
     * In order to try interactive shell we may want to
     * https://stackoverflow.com/questions/21253614/execute-a-shell-command-using-processbuilder-and-interact-with-it
     * */
    public static void main(String[] args) throws Exception {
        final ExecuteExternalProgram instance = new ExecuteExternalProgram();
        final URL conf = Objects.requireNonNull(instance.getClass().getClassLoader().getResource("rclone-local-to-sftp.conf"));
        final URL resource = Objects.requireNonNull(instance.getClass().getClassLoader().getResource("rclone-local-to-sftp.conf"));

        final String[] strings = {
            "rclone",
            "--config",
            conf.getPath(),
            "copy",
            "-P",
            "--log-level=DEBUG",
            resource.getPath(),
            "fms-sftp:/upload/fms/local/6/"
        };

        instance.nativeCall(strings);
    }

    protected String nativeCall(final String... commands) throws Exception {
        log.info("Running '{}'", Arrays.asList(commands));
        final ProcessBuilder pb = new ProcessBuilder(commands);
        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        try {
            final Process process = pb.start();
            final InputStream is = process.getInputStream();
            final OutputStream output = process.getOutputStream();

            final CountDownLatch inputLatch = new CountDownLatch(1);
            final CountDownLatch resultLatch = new CountDownLatch(1);
            StreamGobbler streamGobbler = new StreamGobbler(is, inputLatch, resultLatch);
            executorService.submit(streamGobbler);

            inputLatch.await();
            output.write((SFTP_PASSWORD + "\n").getBytes(StandardCharsets.UTF_8));
            output.flush();
            output.close();

            resultLatch.await();
            String data = streamGobbler.getResult();
            log.info("Completed native call: '{}'\nResponse: '" + data + "'",
                Arrays.asList(commands));
            return data;
        } catch (final IOException e) {
            log.error("Error running commands: {}", Arrays.asList(commands), e);
            return "";
        } finally {
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        }
    }

}
