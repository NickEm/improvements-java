package com.nickem.pdfBox;

import com.nickem.executor.IExecute;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDTextField;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

/**
 * @author Mykola Morozov.
 */
public class PdfExecutor implements IExecute {

    private static final String FILE_EXTENSION = "png";

    @Override
    public void execute() {
        File inputFile = new File("resources/all_fields_test_1.pdf");
//        fillInPdfFormsOld(inputFile);
        fillInPdfFormsNew(inputFile);
    }

    public void convertToImageNewPdfBox(File pdf) {
        String rootFolder = StringUtils.substringBeforeLast(pdf.getAbsolutePath(), File.separator);
        PDDocument newDocument = new PDDocument();
        try (PDDocument document = PDDocument.load(pdf)) {
            for (int page = 0; page < document.getNumberOfPages(); page++) {
                newDocument.addPage(document.getPage(page));
                PDFRenderer renderer = new PDFRenderer(newDocument);
                BufferedImage bim = renderer.renderImageWithDPI(0, 300, ImageType.RGB);
                FileOutputStream output = new FileOutputStream(new File(rootFolder + File.separator + "file_page_" + page + "_mood1." + FILE_EXTENSION));
                ImageIO.write(bim, FILE_EXTENSION, output);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                newDocument.close();
            } catch (IOException e) {
                // Who cares
            }
        }
    }

    public void fillInPdfFormsNew(File inputFile) {

        File resultFile = new File("resources/filled_in_form.pdf");
        String comment = "template pattern provides an outline";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            PDDocument pdfDoc = PDDocument.load(inputFile);
            PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();

            if (acroForm != null) {
                for (Object placeholder : acroForm.getFields()) {
                    PDTextField field = (PDTextField) placeholder;
                    if (field.isReadOnly()) {
                        field.setReadOnly(false);
                    }
                    field.setValue(comment);
                    field.setReadOnly(true);
                }
            }

            protectResultFile(pdfDoc);
            pdfDoc.save(outputStream);
            pdfDoc.close();

            FileOutputStream writer = new FileOutputStream(resultFile);
            writer.write(outputStream.toByteArray());
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*    public void fillInPdfFormsOld(File inputFile) {

        File resultFile = new File("resources/filled_in_form.pdf");
        String comment = "template pattern provides an outline";

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            PDDocument pdfDoc = PDDocument.load(inputFile);
            PDDocumentCatalog docCatalog = pdfDoc.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();

            if (acroForm != null) {
                for (Object placeholder : acroForm.getFields()) {
                    PDTextField field = (PDTextField) placeholder;
                    if (field.isReadOnly()) {
                        field.setReadOnly(false);
                    }
                    field.setValue(comment);
                    field.setReadonly(true);
                }
            }

            protectResultFile(pdfDoc);
            pdfDoc.save(outputStream);
            pdfDoc.close();

            FileOutputStream writer = new FileOutputStream(resultFile);
            writer.write(outputStream.toByteArray());
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void protectResultFile(PDDocument pdfDoc) {
        AccessPermission permission = new AccessPermission();
        permission.setCanModify(false);
        permission.setReadOnly();
        StandardProtectionPolicy protectionPolicy = new StandardProtectionPolicy("password", StringUtils.EMPTY, permission);
        protectionPolicy.setEncryptionKeyLength(128);
        try {
            pdfDoc.protect(protectionPolicy);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
