package com.nickem.benchmark;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

@Slf4j
public class FileReadBenchmark {

    public static void main(String[] args) throws Exception {
        final FileReadBenchmark instance = new FileReadBenchmark();
        final long start = System.nanoTime();
        final long end = System.nanoTime();
        log.info("Time elapsed [{}]", TimeUnit.NANOSECONDS.toMicros(end - start));
    }


    /*
    * Benchmark
    * 2 users
    * 1_005_617_029 ns/op
    * 1_003_246_160 ns/op
    * 4 users
    * 1_004_522_320
    * 1_004_223_219
    * 1_004_070_169
    * 8 users
    * 1_006_084_722
    * 1_006_397_531
    * 16 users
    * 1_038_891_739
    * 1_033_697_359
    * 32 users
    * 2_879_997_008.814
    * */

//    @Benchmark
//    @OutputTimeUnit(TimeUnit.NANOSECONDS)
//    @BenchmarkMode(Mode.AverageTime)
    public void benchmark() throws InterruptedException {
        final int times = 4;
        final ExecutorService executorService = Executors.newFixedThreadPool(times);

        readFileInParallel("/Users/mmorozov/git-repos/own/file-management-service/fixtures/100MB-with-pan-and-headers.csv", times);
        if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
            executorService.shutdownNow();
        }

    }

    public void readFileInParallel(final String filePath, final int times, final ExecutorService executorService) {
        for (int i = 0; i < times; i++) {
            executorService.submit(() -> readFileToString(filePath));
        }
    }

    public void readFileInParallel(final String filePath, final int times) {
        final ExecutorService executorService = Executors.newFixedThreadPool(times);
        readFileInParallel(filePath, times, executorService);
    }

    private String readFileToString(final String filePath) throws Exception {
        File file = new File(filePath);
        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }


}
