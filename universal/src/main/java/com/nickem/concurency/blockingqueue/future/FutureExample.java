package com.nickem.concurency.blockingqueue.future;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

public class FutureExample {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();

        BlockingQueue<String> blockingQueue = new LinkedBlockingDeque<>(3);
        blockingQueue.put("1");
        blockingQueue.put("2");
        blockingQueue.put("3");
        System.out.println("Before put 4");
        blockingQueue.put("4");
        System.out.println("After put 4");
        Thread.sleep(1000);
        System.out.println("After wait put 4");
        blockingQueue.poll();


//        Future<String> future = calculateAsync(executorService);
//        System.out.println("1");
//        String str = future.get();
//        System.out.println("2");
//        System.out.println(str);
//        System.out.println("3");

//        CompletableFuture<String> completableFuture =
//                CompletableFuture.supplyAsync()
//                        .thenCompose(s -> CompletableFuture.supplyAsync(() -> s + " World"));

//        assertEquals("Hello World", completableFuture.get());

//        executorService.shutdown();
    }


    public static CompletableFuture<String> calculateAsync(ExecutorService executorService) {
        CompletableFuture<String> completableFuture = new CompletableFuture<>();
        completableFuture.join();

        executorService.submit(() -> {
            System.out.println("Something");
//            Thread.sleep(3000);
//            completableFuture.complete("Hello");
            return null;
        });

        return completableFuture;
    }

}
