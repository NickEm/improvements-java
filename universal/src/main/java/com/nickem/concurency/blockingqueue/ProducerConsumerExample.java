package com.nickem.concurency.blockingqueue;

import com.nickem.executor.IExecute;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ProducerConsumerExample implements IExecute {

    @Override
    public void execute() {
        BlockingQueue<Integer> sharedQueue = new LinkedBlockingQueue<>();

        Producer producer = new Producer(sharedQueue);
        Consumer consumer = new Consumer(sharedQueue);

        Thread producerThread = new Thread(producer, "ProducerThread");
        Thread consumerThread = new Thread(consumer, "ConsumerThread");
        producerThread.start();
        consumerThread.start();
    }
}
