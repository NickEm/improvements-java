package com.nickem.jbpm;

import com.nickem.executor.IExecute;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.KieServerInfo;
import org.kie.server.api.model.definition.ProcessDefinition;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.ProcessServicesClient;
import org.kie.server.client.UserTaskServicesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JbpmExample implements IExecute {

    private static final Logger log = LoggerFactory.getLogger(JbpmExample.class);

    private static final String HOST = "localhost";
    private static final int PORT = 8087;
    private static final String URL = String.format("http://%s:%s/kie-server/services/rest/server", HOST, PORT);
    private static final String USER = "admin";
    private static final String PASSWORD = "admin";

    private static final MarshallingFormat FORMAT = MarshallingFormat.JSON;

    private KieServicesConfiguration conf;
    private KieServicesClient client;

    @Override
    public void execute() {
        conf = KieServicesFactory.newRestConfiguration(URL, USER, PASSWORD);
        conf.setMarshallingFormat(FORMAT);
        client = KieServicesFactory.newKieServicesClient(conf);

        String containerId = "evaluation:evaluation:1.0.0-SNAPSHOT";
        log.info("Container Info: ", client.getContainerInfo(containerId).toString());
        KieServerInfo serverInfo = client.getServerInfo().getResult();
        log.info("Server capabilities:");
        for(String capability: serverInfo.getCapabilities()) {
            log.info("Capability is {}", capability);
        }

        String processId = "evaluation";
        ProcessServicesClient processServices = client.getServicesClient(ProcessServicesClient.class);
        ProcessDefinition evaluation = processServices.getProcessDefinition(containerId, processId);
        evaluation.getServiceTasks().forEach((k,v) -> log.info("{} : {}", k, v));

        UserTaskServicesClient taskServices = client.getServicesClient(UserTaskServicesClient.class);
    }
}
