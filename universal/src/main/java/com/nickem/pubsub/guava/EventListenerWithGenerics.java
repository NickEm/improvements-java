package com.nickem.pubsub.guava;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventListenerWithGenerics {

    @Subscribe
    public void stringEvent(Box<String> event) {
        log.info("Processing string {}", event.getValue());
    }

    @Subscribe
    public void intEvent(Box<Integer> event) {
        log.info("Processing int {}", event.getValue());
    }

}
