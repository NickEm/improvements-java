package com.nickem.pubsub.guava;

import com.google.common.eventbus.Subscribe;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventListener2 {

    @Subscribe
    public void stringEvent(String event) {
        log.info("Processing {}", event);
    }

    @Subscribe
    public void newtStringEvent(String event) {
        log.info("Processing {}", event);
    }

}
