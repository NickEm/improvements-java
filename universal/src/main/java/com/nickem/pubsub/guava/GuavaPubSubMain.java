package com.nickem.pubsub.guava;

import com.google.common.eventbus.EventBus;

public class GuavaPubSubMain {

    public static void main(String[] args) {
        EventBus eventBus = new EventBus();

        EventListenerWithGenerics listener1 = new EventListenerWithGenerics();
//        EventListener2 listener2 = new EventListener2();
        eventBus.register(listener1);
//        eventBus.register(listener2);

//        EventBus eventBus1 = new EventBus();
//        eventBus1.register(listener1);

        eventBus.post(new Box<>("dummy event"));
        eventBus.post(new Box<>(123));
//        eventBus1.post("dummy event 1");
    }

}
