package com.nickem.security;

import com.nickem.executor.IExecute;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mykola Morozov
 */
public class СypherExample implements IExecute {

    public static void main(String[] args) {
        new СypherExample().execute();
    }

    @Override
    public void execute() {
        String result = "";
        String key = readStringFromFile("F:\\cipher-key.txt").toLowerCase();
        String text = readStringFromFile("F:\\cipher-text.txt").toLowerCase();
        List<String> alphabet = initializeAlphabet();
        writeStatistic(text, alphabet, "Income");

        String[] keyArray = key.split("");
        String[] textArray = text.split("");
        int keyArrayCurrentIndex = 0;

        for(String s: textArray) {
            Integer indexOfCurrentSymbol = alphabet.indexOf(s);
            Integer indexOfNewSymbol = (indexOfCurrentSymbol + alphabet.indexOf(keyArray[keyArrayCurrentIndex])) % alphabet.size();
            result = result + alphabet.get(indexOfNewSymbol);
            if (keyArrayCurrentIndex >= keyArray.length - 1) {
                keyArrayCurrentIndex = 0;
            } else {
                keyArrayCurrentIndex++;
            }
        }

        System.out.println("Final result is: " + result);
        writeStatistic(result, alphabet, "Outcome");

    }

    private String readStringFromFile(String fileLocation) {
        String result = "";
        try (BufferedReader br = new BufferedReader(new FileReader(fileLocation))) {
            String line;
            while ((line = br.readLine()) != null) {
                result = result + line;
            }
        } catch (Exception e) {
            //NOP
        }

        return result;
    }

    private List<String> initializeAlphabet() {
        List<String> alphabet = new ArrayList<>();
        alphabet.add("a");
        alphabet.add("b");
        alphabet.add("c");
        alphabet.add("d");
        alphabet.add("e");
        alphabet.add("f");
        alphabet.add("g");
        alphabet.add("h");
        alphabet.add("i");
        alphabet.add("j");
        alphabet.add("k");
        alphabet.add("l");
        alphabet.add("m");
        alphabet.add("n");
        alphabet.add("o");
        alphabet.add("p");
        alphabet.add("q");
        alphabet.add("r");
        alphabet.add("s");
        alphabet.add("t");
        alphabet.add("u");
        alphabet.add("v");
        alphabet.add("w");
        alphabet.add("x");
        alphabet.add("y");
        alphabet.add("z");

        return alphabet;
    }

    private void writeStatistic(String text, List<String> alphabet, String textLabel) {
        String[] textArray = text.split("");
        for(String letter: alphabet) {
            int usageCounts = 0;
            for(String s: textArray) {
                if (s.equals(letter)) {
                    usageCounts++;
                }
            }
            System.out.println("Usage of letter " + letter  + " in " + textLabel + " is: " + usageCounts);
        }

    }
}
