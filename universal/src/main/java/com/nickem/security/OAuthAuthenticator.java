package com.nickem.security;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 * @author Mykola Morozov
 */
public class OAuthAuthenticator {

    private String host;
    private String path;
    private String consumerKey;
    private String consumerSecret;

    public OAuthAuthenticator( String host,
                               String path,
                               String consumerKey,
                               String consumerSecret )
    {
        this.host = host;
        this.path = path;
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
    }

    public String generateOauthHeader( String method,
                                       String callback,
                                       String[] additionalParameters ) throws UnsupportedEncodingException {
        long timestamp = new Date().getTime() / 1000;
        // XXX this should be different than the timestamp, but
        // good enough for demonstration purposes
        String nonce = Long.toString( timestamp );

        List<String> parameters = new ArrayList<>();
        parameters.add( "oauth_consumer_key=" + consumerKey );
        parameters.add( "oauth_nonce=" + nonce );
        parameters.add( "oauth_signature_method=HMAC-SHA1" );
        parameters.add( "oauth_timestamp=" + timestamp );
        // Note this is URL encoded twice
        parameters.add( "oauth_callback=" + URLEncoder.encode(callback, StandardCharsets.UTF_8.displayName()));
        parameters.add( "oauth_version=1.0" );

        for ( String additionalParameter : additionalParameters ) {
            parameters.add( additionalParameter );
        }

        Collections.sort( parameters );

        StringBuffer parametersList = new StringBuffer();

        for ( int i = 0; i < parameters.size(); i++ ) {
            parametersList.append( ( ( i > 0 ) ? "&" : "" ) + parameters.get( i ) );
        }

        String signatureString =
                method + "&" +
                        URLEncoder.encode( "https://" + host + path, StandardCharsets.UTF_8.displayName()) + "&" +
                        URLEncoder.encode(parametersList.toString(), StandardCharsets.UTF_8.displayName());

        String signature = null;

        try {
            SecretKeySpec signingKey = new SecretKeySpec((consumerSecret + "&").getBytes(), "HmacSHA1" );
            Mac mac = Mac.getInstance( "HmacSHA1" );
            mac.init(signingKey);
            byte[] rawHMAC = mac.doFinal( signatureString.getBytes() );
            signature = Base64.encodeBase64String(rawHMAC);
        }
        catch ( Exception e )
        {
            System.err.println( "Unable to append signature" );
            System.exit( 0 );
        }

        System.out.println( signature );

        String authorizationLine =
                "Authorization: OAuth " +
                        "oauth_consumer_key=\"" + consumerKey + "\", " +
                        "oauth_nonce=\"" + nonce + "\", " +
                        "oauth_timestamp=\"" + timestamp + "\", " +
                        "oauth_signature_method=\"HMAC-SHA1\", " +
                        "oauth_signature=\"" + URLEncoder.encode(signature, StandardCharsets.UTF_8.displayName()) + "\", " +
                        "oauth_version=\"1.0\"";
        authorizationLine += ", oauth_callback=\"" +
                URLEncoder.encode(signature, StandardCharsets.UTF_8.displayName()) + "\"";

        return authorizationLine;
    }
}
