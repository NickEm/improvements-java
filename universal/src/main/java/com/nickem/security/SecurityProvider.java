package com.nickem.security;

import com.nickem.executor.IExecute;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import net.oauth.OAuth;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Mykola Morozov.
 */
public class SecurityProvider implements IExecute {

    public static final String AUTH_HEADER_PREFIX_SERVICE_REQUEST_TOKEN = "Service-Request-Token";
    private static final String HMAC_SHA_1_ALGORITHM = "HmacSHA1";

    @Override
    public void execute() {
//        testServiceRequestToken();
        try {
            testLtiRequest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void testLtiRequest() throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
        String rawBody = "basiclti_submit=test+2&context_id=8602d323-e938-4194-b670-00644b345852&context_label=Demo+100+01+-+Knovio+LTI+Work&context_title=Demo+100+01+-+Knovio+LTI+Work&context_type=urn%3Alti%3Acontext-type%3Aims%2Flis%2FCourseSection&ext_jics_username=KnovioFaculty&ext_lms=eLearning%3A+2.0&launch_presentation_document_target=iframe&launch_presentation_locale=en-US&lis_course_offering_sourcedid=school.edu%3ASI182-F08&lis_course_section_sourcedid=9b18c6b0-1f77-4b1c-bf72-3d9d575e7863&lis_outcome_service_url=http%3A%2F%2Fast.jenzabar.net%2Fics%2Fportlets%2Fics%2Flearningtoolsportlet%2Fservices%2Foutcomescallback.aspx&lis_person_contact_email_primary=rob%40knowledgevision.com&lis_person_name_family=Knowledgevision&lis_person_name_full=Faculty+Knowledgevision&lis_person_name_given=Faculty&lis_person_sourcedid=ast.jenzabar.net%3Ac189be0e-2f51-4706-80f5-6da5d6c5b293&lis_result_sourcedid=365f4e6d-b1ce-4143-960d-6c66d50019d4%3A&lti_message_type=basic-lti-launch-request&lti_version=LTI-1p0&oauth_callback=about%3Ablank&oauth_consumer_key=db090c27-dd77-4e04-8551-cca478c41552&oauth_nonce=4633352&oauth_signature_method=HMAC-SHA1&oauth_timestamp=1471105262&oauth_version=1.0&resource_link_description=test+2&resource_link_id=365f4e6d-b1ce-4143-960d-6c66d50019d4&resource_link_title=test+2&roles=urn%3Alti%3Arole%3Aims%2Flis%2FInstructor&tool_consumer_info_product_family_code=eLearning%3A&tool_consumer_info_version=eLearning%3A+2.0&tool_consumer_instance_contact_email=rob%40knowledgevision.com&tool_consumer_instance_guid=ast.jenzabar.net&user_id=c189be0e-2f51-4706-80f5-6da5d6c5b293&oauth_signature=NR9215NaByYDfsZfI0ZXZd0TCRE%3D";
        String requestMethod = "POST";
        String requestUrl = "http://requestb.in/12mqffs1";
        String key = "db090c27-dd77-4e04-8551-cca478c41552";
        String secret = "06Cs1QDIJLx1TFM3D0VxyWjq8hK1WXdJgZJB21dinN8NS3JPNeXyXdp5Sarzam0L";
        String secretEncoded = encode(secret);

        Map<String, String> requestParams = new TreeMap<>();
        Map<String, String> rawParams = Arrays.stream(rawBody.split("&"))
            .collect(Collectors.toMap(e -> OAuth.decodePercent(e.split("=")[0]), e -> OAuth.decodePercent(e.split("=")[1]), (o, n) -> o));
        requestParams.putAll(rawParams);
//        requestParams.put("oauth_callback", "about:blank");
//        requestParams.put("oauth_consumer_key", key);
//        requestParams.put("oauth_nonce", "3836129");
//        requestParams.put("oauth_signature_method", "HMAC SHA1");
//        requestParams.put("oauth_timestamp", "1470931750");
//        requestParams.put("oauth_version", "1.0");

        String signatureBaseString = requestParams.entrySet().stream().map((e) -> encode(e.getKey()) + "=" + encode(e.getValue())).collect(Collectors.joining("&"));

//        for (Map.Entry<String, String> param : requestParams.entrySet()) {
//            String paramString = param.getKey() + "=" + encode(param.getValue()) + "&";
//            signatureBaseString = signatureBaseString.concat(paramString);
//        }
//        signatureBaseString = signatureBaseString.substring(0, signatureBaseString.length() - 1);
        System.out.println("Signature base string: " + signatureBaseString);
        signatureBaseString = requestMethod + "&" + encode(requestUrl) + "&" + encode(signatureBaseString);

        System.out.println("Signature base string: " + signatureBaseString);

        String signature;
        SecretKeySpec signingKey = new SecretKeySpec((secretEncoded + "&").getBytes(), HMAC_SHA_1_ALGORITHM);
        Mac mac = Mac.getInstance(HMAC_SHA_1_ALGORITHM);
        mac.init(signingKey);
        byte[] rawHMAC = mac.doFinal(signatureBaseString.getBytes());
        signature = Base64.encodeBase64String(rawHMAC);

        System.out.println("Signature after HMAC: " + signature);
    }

    private void testServiceRequestToken() {
        String serviceName = "knovio-web";
        String timestamp = Long.toString(new Date().getTime());
        String serviceKey = "c6e7925f-0d1d-4eb1-9b35-f07564f8b6fa";
        String requestUrl = "/analytic/events";
        String body = "{\"user_id\":16331,\"account_id\":892,\"event\":\"New security event\",\"application\":\"KnovioWeb\",\"properties\":{\"Project state\":\"Better\"}}";
        /*System.out.println(stageGenerateTokenForPresentations(serviceName, timestamp, body));*/
        System.out.println(localGenerateServiceToken(serviceName, timestamp, serviceKey, requestUrl, body));
    }

    public static String stageGenerateTokenForPresentations(String serviceName, String timestamp, String body){
        String serviceKey = "c068c3de-ec7c-4822-accf-4026e877e753";
        String requestUrl = "/accounts/16115/subaccounts/-1/presentations?user_id=16904";
        return generateRequestAuthorizationHeader(AUTH_HEADER_PREFIX_SERVICE_REQUEST_TOKEN, serviceName, serviceKey, timestamp, requestUrl, null);
    }

    public static String localGenerateServiceToken(String serviceName, String timestamp, String serviceKey, String requestUrl, String body){
        return generateRequestAuthorizationHeader(AUTH_HEADER_PREFIX_SERVICE_REQUEST_TOKEN, serviceName, serviceKey, timestamp, requestUrl, body);
    }

    public static String stageGenerateTokenForProvision(String serviceName, String timestamp, String body){
        String serviceKey = "c068c3de-ec7c-4822-accf-4026e877e753";
        String requestUrl = "/licenses/provision";
        return generateRequestAuthorizationHeader(AUTH_HEADER_PREFIX_SERVICE_REQUEST_TOKEN, serviceName, serviceKey, timestamp, requestUrl, body);
    }

    public static String localGenerateTokenForProvision(String serviceName, String timestamp, String body){
        String serviceKey = "bf73cfa0-6c5f-4edf-9f73-b911303aecd5";
        String requestUrl = "/admin-api/licenses/provision";
        return generateRequestAuthorizationHeader(AUTH_HEADER_PREFIX_SERVICE_REQUEST_TOKEN, serviceName, serviceKey, timestamp, requestUrl, body);
    }

    /*Base64(<service_name>:<timestamp>:HEX(SHA-256(<service_name>:<service_key>:<timestamp>:<request_url_without_domain>:<request_body>)))*/
    public static String generateRequestAuthorizationHeader(String prefix, String serviceName, String serviceKey, String timestamp, String requestUrl, String body) {
        String credentials = String.format("%s:%s:%s:%s:%s", serviceName, serviceKey, timestamp, requestUrl, body == null ? StringUtils.EMPTY : body);
        String secret = String.format("%s:%s:%s", serviceName, timestamp, DigestUtils.sha256Hex(credentials));
        return String.format("%s %s", prefix, new String(Base64.encodeBase64(secret.getBytes()), StandardCharsets.UTF_8));
    }

    private String encode(String value){
//        return URLEncoder.encode(value, StandardCharsets.UTF_8.displayName());
        return OAuth.percentEncode(value);
    }

}
