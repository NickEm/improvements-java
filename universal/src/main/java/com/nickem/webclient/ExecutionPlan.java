package com.nickem.webclient;

import java.net.http.HttpClient;
import lombok.Data;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@Data
@State(Scope.Benchmark)
public class ExecutionPlan {

    public HttpClient client;

    @Setup(Level.Trial)
    public void setUp() {
        client = HttpClient.newHttpClient();
    }

}
