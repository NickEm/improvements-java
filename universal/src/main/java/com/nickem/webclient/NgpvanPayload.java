package com.nickem.webclient;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
public class NgpvanPayload {

    @Data
    @RequiredArgsConstructor
    static class NgpvanAmount {
        final double amount;
        final String currency;
    }

    String paymentMethodType;
    String firstName;
    String lastName;
    String creditCardNumberOrLastFour;
    String expirationYear;
    String expirationMonth;
    String creditCardType;
    String merchantAccountGlobalId;
    String invoiceNumber;
    NgpvanAmount amount;

}
