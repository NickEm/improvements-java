package com.nickem.webclient;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;

/*
* https://www.baeldung.com/java-9-http-client
* */
@Slf4j
public class HttpClientExample {

    public static final int NUMBER_OF_REQUESTS = 16;

    /*
    * https://api-v2.play.holacash.mx/v2/account/whois?email=humberto@holacash.mx
    * https://live.api.play.holacash.mx/v2/account/whois?email=humberto@holacash.mx
    * */

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        final HttpClientExample instance = new HttpClientExample();
        final HttpClient client = HttpClient.newHttpClient();
        instance.distributeLoad(client, 16);

//        final ExecutionPlan executionPlan = new ExecutionPlan();
//        executionPlan.setClient(client);

//        final ExecutorService executorService = Executors.newFixedThreadPool(32);
//        final long startTime = System.nanoTime();
//        List<CompletableFuture<Void>> futures = new ArrayList<>();
//        for (int i = 0; i < NUMBER_OF_REQUESTS; i++) {
//            futures.add(CompletableFuture.runAsync(() -> instance.loadCall(executionPlan), executorService));
//        }
//        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
//        final long endTime = System.nanoTime();
//        final double average = ((double) (endTime - startTime) / 10) / 1_000_000_000;
//        BigDecimal bd = BigDecimal.valueOf(average);
//        bd = bd.setScale(2, RoundingMode.HALF_UP);
//        log.info("Average execution time: {}", bd.doubleValue());
    }


//    @Benchmark
//    @OutputTimeUnit(TimeUnit.NANOSECONDS)
//    @BenchmarkMode(Mode.AverageTime)
//    public void vgsCall(ExecutionPlan plan) throws URISyntaxException, IOException, InterruptedException {
//        final HttpRequest request = HttpRequest.newBuilder()
//            .uri(new URI("https://live.api.play.holacash.mx/v2/account/whois?email=humberto@holacash.mx"))
//            .GET()
//            .build();
//
//        HttpResponse<String> response = plan.getClient().send(request, BodyHandlers.ofString());
//        log.info("Request-id {}", response.headers().firstValue("vgs-request-id").get());
//        log.info("Body {}", response.body());
//    }

    public void distributeLoad(HttpClient client, int simultaneousRequests) throws URISyntaxException {

        final HttpRequest request = HttpRequest.newBuilder()
            .uri(new URI("https://hc-ping.com/de00acda-8091-41aa-a4cd-eef27a56b1fe"))
            .GET()
            .version(HttpClient.Version.HTTP_1_1)
            .build();

        final ExecutorService executorService = Executors.newFixedThreadPool(simultaneousRequests);
        List<CompletableFuture<Void>> futures = new ArrayList<>();
        for (int i = 0; i < simultaneousRequests; i++) {
            futures.add(CompletableFuture.runAsync(() -> {
                try {
                    final String uuid = UUID.randomUUID().toString();
                    HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
                    log.info("Response body {}:{}", uuid, response.body());
                    log.warn("Response hdrs {}:{}", uuid, response.headers());
                    log.info("Response code {}:{}", uuid, response.statusCode());

                } catch (Exception e) {
                    log.error("Unexpected error", e);
                }
            }, executorService));
        }
        CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    public void directCall(ExecutionPlan plan) throws URISyntaxException, IOException, InterruptedException {
        final HttpRequest request = HttpRequest.newBuilder()
            .uri(new URI("https://api-v2.play.holacash.mx/v2/account/whois?email=humberto@holacash.mx"))
            .GET()
            .build();

        HttpResponse<String> response = plan.getClient().send(request, BodyHandlers.ofString());
        log.info("Body {}", response.body());
    }

    @SneakyThrows
    public void loadCall(ExecutionPlan plan) {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final HttpRequest request = HttpRequest.newBuilder()
            .uri(new URI("https://tntgdyt5rt0.sandbox.verygoodproxy.io/post/reverse-proxy"))
            .header("Content-type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofInputStream(() -> classLoader.getResourceAsStream(
                "csv/0.5K-with-pan-and-headers.json")))
            .build();
        try {
            plan.getClient().send(request, BodyHandlers.ofString());
        } catch (Exception e) {
            log.warn("Issue with sending request", e);
        }
    }


}
