package com.nickem.webclient.forwardproxy;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

// https://verygoodsecurity.atlassian.net/browse/CSL3-2209
public class ForwardProxyIssue {

    public static void main(String[] args)
        throws IOException, CertificateException, InterruptedException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        System.setProperty("java.util.logging.ConsoleHandler.level", "ALL");
        System.setProperty("java.util.logging.ConsoleHandler.formatter", "java.util.logging.SimpleFormatter");
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
        System.setProperty("java.net", "TRACE");

        ForwardProxyIssue instance = new ForwardProxyIssue();

//        int proxyPort = 8443;
        int proxyPort = 8080;
        String dataEnv = "sandbox";

        String tenant = "tnth35betez";
        String proxyUser = "<replace-me>";
        String proxyPassword = "<replace-me>";
        String targetUrl = "https://echo.apps.verygood.systems/post";

        instance.tlsProxy(tenant, dataEnv, proxyPort, proxyUser, proxyPassword, targetUrl);
    }

    private void tlsProxy(String tenant, String dataEnv, int proxyPort, String proxyUser, String proxyPassword, String targetUrl)
        throws IOException, CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        String proxyHost = "%s.%s.verygoodproxy.com".formatted(tenant, dataEnv);

        // Setting up the proxy authenticator
        Authenticator proxyAuthenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(proxyUser, proxyPassword.toCharArray());
            }
        };

        final SSLContext sslContext = createSslContext();

        HttpClient httpClient = HttpClient.newBuilder()
            .version(Version.HTTP_1_1)
            .proxy(ProxySelector.of(new InetSocketAddress(proxyHost, proxyPort)))
            .sslContext(sslContext)
            .authenticator(proxyAuthenticator)
            .build();

        // Creating the HTTP request
        HttpRequest request = createRequest(targetUrl, proxyUser, proxyPassword);

        // Sending the request and getting the response
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println("Status code: " + response.statusCode());
            System.out.println("Response: " + response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            System.err.println("Failed to send HTTP request: " + e.getMessage());
        }
    }

    private HttpRequest createRequest(String targetUrl, String proxyUser, String proxyPassword) {
        String ccNumber = "tok_sandbox_gaT8cwCrZecnA49nLbLGGJ";
        String ccCvv = "tok_sandbox_qv8DYsZMjnwo1PSojzVSya";

        String requestBody = """
			{
			    "number": "%s",
			    "cvv": "%s"
			}
			""".formatted(ccNumber, ccCvv);

        return HttpRequest.newBuilder()
            .uri(URI.create(targetUrl))
            .header("Content-Type", "application/json")
            .POST(BodyPublishers.ofString(requestBody, StandardCharsets.UTF_8))
//            .version(Version.HTTP_1_1)
            .build();
    }

    private SSLContext createSslContext()
        throws CertificateException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        // Loading the keystore
        KeyStore keyStore = loadKeystore();

        // Building the SSL context
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(keyStore);

        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
        return sslContext;
    }

    private KeyStore loadKeystore() throws KeyStoreException, CertificateException, IOException, NoSuchAlgorithmException {
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, "vgs".toCharArray());

        final ClassLoader classLoader = this.getClass().getClassLoader();
        try (InputStream fis = classLoader.getResourceAsStream("vgs-certificate/sandbox.pem");
            BufferedInputStream bis = new BufferedInputStream(fis)) {
            X509Certificate vgsSelfSigned = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(bis);
            ks.setCertificateEntry("VGS Self Signed Certificate", vgsSelfSigned);
        }

        URL destinationURL = new URL("https://tntctl97lil.SANDBOX.verygoodproxy.com");
        HttpsURLConnection conn = (HttpsURLConnection) destinationURL.openConnection();
        conn.connect();
        for (Certificate certificate : conn.getServerCertificates()) {
            ks.setCertificateEntry(String.valueOf(certificate.hashCode()), certificate);
        }

        return ks;
    }

}
