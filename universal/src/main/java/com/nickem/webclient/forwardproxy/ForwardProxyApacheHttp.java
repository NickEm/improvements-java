package com.nickem.webclient.forwardproxy;
import java.nio.charset.Charset;
import org.apache.hc.client5.http.auth.AuthScope;
import org.apache.hc.client5.http.auth.UsernamePasswordCredentials;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.auth.BasicCredentialsProvider;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.ssl.SSLContextBuilder;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.TrustAllStrategy;
import org.apache.hc.core5.util.Timeout;
import org.apache.hc.core5.http.io.entity.StringEntity;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.GeneralSecurityException;

public class ForwardProxyApacheHttp {

    public static void main(String[] args) throws IOException, GeneralSecurityException {
        System.setProperty("java.util.logging.ConsoleHandler.level", "ALL");
        System.setProperty("java.util.logging.ConsoleHandler.formatter", "java.util.logging.SimpleFormatter");
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
        System.setProperty("java.net", "TRACE");

        ForwardProxyApacheHttp instance = new ForwardProxyApacheHttp();

        int proxyPort = 8443;
//        int proxyPort = 8080;
        String dataEnv = "sandbox";

        String tenant = "tnth35betez";
        String proxyUser = "<replace-me>";
        String proxyPassword = "<replace-me>";
        String targetUrl = "https://echo.apps.verygood.systems/post";

        instance.tlsProxy(tenant, dataEnv, proxyPort, proxyUser, proxyPassword, targetUrl);
    }

    private void tlsProxy(String tenant, String dataEnv, int proxyPort, String proxyUser, String proxyPassword, String targetUrl)
        throws IOException, GeneralSecurityException {
        String proxyHost = String.format("%s.%s.verygoodproxy.com", tenant, dataEnv);

        // Create SSL context that trusts all certificates
        SSLContext sslContext = SSLContextBuilder.create()
            .loadTrustMaterial(TrustAllStrategy.INSTANCE)
            .build();

        // Create connection manager with the custom SSL context
        PoolingHttpClientConnectionManagerBuilder connectionManagerBuilder = PoolingHttpClientConnectionManagerBuilder.create();
        connectionManagerBuilder.setSSLSocketFactory(new SSLConnectionSocketFactory(sslContext));
        connectionManagerBuilder.setConnectionTimeToLive(Timeout.ofMinutes(1));

        // Create credentials provider for proxy authentication
        BasicCredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
            new AuthScope(proxyHost, proxyPort),
            new UsernamePasswordCredentials(proxyUser, proxyPassword.toCharArray())
        );

        // Build the HttpClient with the proxy and custom SSL context
        CloseableHttpClient httpClient = HttpClients.custom()
            .setConnectionManager(connectionManagerBuilder.build())
            .setDefaultCredentialsProvider(credsProvider)
            .setProxy(new HttpHost("https", proxyHost, proxyPort))
            .build();

        // Create the HTTP request
        HttpPost request = createRequest(targetUrl);

        // Execute the request and get the response
        try (CloseableHttpResponse response = httpClient.execute(request)) {
            System.out.println("Status code: " + response.getCode());
            System.out.println("Response: " + new String(response.getEntity().getContent().readAllBytes(), Charset.defaultCharset()));

        }
    }

    private HttpPost createRequest(String targetUrl) {
        String ccNumber = "tok_sandbox_gaT8cwCrZecnA49nLbLGGJ";
        String ccCvv = "tok_sandbox_qv8DYsZMjnwo1PSojzVSya";

        String requestBody = String.format("""
            {
                "number": "%s",
                "cvv": "%s"
            }
            """, ccNumber, ccCvv);

        HttpPost request = new HttpPost(targetUrl);
        request.setEntity(new StringEntity(requestBody, Charset.defaultCharset()));
        request.setHeader("Content-Type", "application/json");
        return request;
    }
}
