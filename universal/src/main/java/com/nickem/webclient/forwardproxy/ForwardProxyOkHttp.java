package com.nickem.webclient.forwardproxy;
import okhttp3.*;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class ForwardProxyOkHttp {

    public static void main(String[] args) throws IOException, CertificateException, InterruptedException, NoSuchAlgorithmException, KeyManagementException {
        System.setProperty("java.util.logging.ConsoleHandler.level", "ALL");
        System.setProperty("java.util.logging.ConsoleHandler.formatter", "java.util.logging.SimpleFormatter");
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
        System.setProperty("java.net", "TRACE");

        ForwardProxyOkHttp instance = new ForwardProxyOkHttp();

//        int proxyPort = 8443;
        int proxyPort = 8080;
        String dataEnv = "sandbox";

        String tenant = "tnth35betez";
        String proxyUser = "<replace-me>";
        String proxyPassword = "<replace-me>";
        String targetUrl = "https://echo.apps.verygood.systems/post";

        instance.tlsProxy(tenant, dataEnv, proxyPort, proxyUser, proxyPassword, targetUrl);
    }

    private void tlsProxy(String tenant, String dataEnv, int proxyPort, String proxyUser, String proxyPassword, String targetUrl)
        throws IOException, CertificateException, NoSuchAlgorithmException, KeyManagementException {
        String proxyHost = String.format("%s.%s.verygoodproxy.com", tenant, dataEnv);

        final SSLContext sslContext = createTrustAllSslContext();

        OkHttpClient client = new OkHttpClient.Builder()
            .proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort)))
            .proxyAuthenticator((route, response) -> {
                String credential = Credentials.basic(proxyUser, proxyPassword);
                return response.request().newBuilder()
                    .header("Proxy-Authorization", credential)
                    .build();
            })
            .sslSocketFactory(sslContext.getSocketFactory(), new TrustAllCertificates())
            .hostnameVerifier((hostname, session) -> true)
            .build();

        Request request = createRequest(targetUrl, proxyUser, proxyPassword);

        try (Response response = client.newCall(request).execute()) {
            System.out.println("Status code: " + response.code());
            System.out.println("Response: " + response.body().string());
        }
    }

    private SSLContext createTrustAllSslContext() throws NoSuchAlgorithmException, KeyManagementException {
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
        };

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, trustAllCerts, new SecureRandom());
        return sslContext;
    }

    private Request createRequest(String targetUrl, String proxyUser, String proxyPassword) {
        String ccNumber = "tok_sandbox_gaT8cwCrZecnA49nLbLGGJ";
        String ccCvv = "tok_sandbox_qv8DYsZMjnwo1PSojzVSya";

        String requestBody = String.format("""
            {
                "number": "%s",
                "cvv": "%s"
            }
            """, ccNumber, ccCvv);

        return new Request.Builder()
            .url(targetUrl)
            .header("Content-Type", "application/json")
            .post(RequestBody.create(requestBody, MediaType.parse("application/json; charset=utf-8")))
            .build();
    }

    static class TrustAllCertificates implements X509TrustManager {
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }

        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    }
}
