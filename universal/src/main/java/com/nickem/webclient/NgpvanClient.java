package com.nickem.webclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Set;
import java.util.TreeSet;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class NgpvanClient {

    Set<String> requestContexts = new TreeSet<>();
    public static void main(String[] args) throws Exception {
        final NgpvanClient instance  = new NgpvanClient();
        final HttpClient client = HttpClient.newHttpClient();
        final ObjectMapper objectMapper = new ObjectMapper();
        final NgpvanPayload payloadObj = instance.preparePayload();
        final String payload = objectMapper.writeValueAsString(payloadObj);
        for (int i = 0; i < 36000; i++) {
            instance.loadCall(client, payload);
        }
        log.info("{}", instance.requestContexts);
    }

    @SneakyThrows
    public void loadCall(final HttpClient client, final String body) {
        final long startTime = System.nanoTime();
        final HttpRequest request = HttpRequest.newBuilder()
            .uri(new URI("https://pay-staging.ngpvan.com/api/payments/completePayment"))
            .header("Content-type", "application/json")
            .header("Authorization", "Basic VmdzU3RhZ2luZ1VzZXI6MDhBNDQ5REQtMTA2Ri00ODIwLUE4RkEtQjYyMjBCQzhBQkM5")
            .POST(HttpRequest.BodyPublishers.ofString(body))
            .version(HttpClient.Version.HTTP_1_1)
            .build();
        try {
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            if (response.statusCode() != 200) {
//                log.warn("Response code is {}", response.statusCode());
//                log.warn("headers {}", response.headers());
                log.warn("body {}", response.body());
            }
            final String context = response.headers().firstValue("request-context").get();
            requestContexts.add(StringUtils.substringAfter(context, ":"));
            log.debug("Body {}", response.body());
        } catch (Exception e) {
            log.warn("Issue with sending request", e);
        } finally {
            log.info("Execution of single call: {}", findTimeLap(startTime, System.nanoTime()));

        }
    }

    private NgpvanPayload preparePayload() {
        return NgpvanPayload.builder()
            .paymentMethodType("CreditCard")
            .firstName("First Name")
            .lastName("Last Name")
            .creditCardNumberOrLastFour("4111111111111111")
            .expirationYear("2025")
            .expirationMonth("12")
            .creditCardType("visa")
            .merchantAccountGlobalId("AAAAAAAA-AAAA-AAAA-AAAA-000000000004")
            .invoiceNumber("VGS Fake Invoice Number")
            .amount( new NgpvanPayload.NgpvanAmount(501d, "USD"))
            .build();
    }

    private NgpvanPayload preparePayload1() {
        return NgpvanPayload.builder()
            .paymentMethodType("CreditCard")
            .firstName("First Name")
            .lastName("Last Name")
            .creditCardNumberOrLastFour("4111111111111111")
            .expirationYear("2025")
            .expirationMonth("12")
            .creditCardType("visa")
            .merchantAccountGlobalId("AAAAAAAA-AAAA-AAAA-AAAA-000000000004")
            .invoiceNumber("VGS Fake Invoice Number")
            .amount( new NgpvanPayload.NgpvanAmount(501d, "USD"))
            .build();
    }

    public double findTimeLap(final long startTime, final long endTime) {
        final double average = ((double) (endTime - startTime)) / 1_000_000_000;
        BigDecimal bd = BigDecimal.valueOf(average);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
