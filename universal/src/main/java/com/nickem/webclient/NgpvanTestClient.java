package com.nickem.webclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NgpvanTestClient {

    public static void main(String[] args) throws Exception {
        final NgpvanTestClient instance = new NgpvanTestClient();
        final HttpClient client = HttpClient.newHttpClient();
        final ObjectMapper objectMapper = new ObjectMapper();
        final NgpvanTestPayload payloadObj = instance.preparePayload();
        final String payload = objectMapper.writeValueAsString(payloadObj);
        for (int i = 0; i < 36000; i++) {
            instance.loadCall(client, payload);
        }
    }

    public void loadCall(final HttpClient client, final String body) throws URISyntaxException {
        final long startTime = System.nanoTime();
        final HttpRequest request = HttpRequest.newBuilder()
            .uri(new URI("https://pay-staging.ngpvan.com/api/payments/completePayment"))
            .header("Content-type", "application/json")
            .header("Authorization", "Basic VmdzU3RhZ2luZ1VzZXI6MDhBNDQ5REQtMTA2Ri00ODIwLUE4RkEtQjYyMjBCQzhBQkM5")
            .POST(HttpRequest.BodyPublishers.ofString(body))
            .version(HttpClient.Version.HTTP_1_1)
            .build();
        try {
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                log.warn("body {}", response.body());
            }
        } catch (Exception e) {
            log.warn("Issue with sending request", e);
        } finally {
            log.info("Execution of single call: {}", findTimeLap(startTime, System.nanoTime()));

        }
    }

    private NgpvanTestPayload preparePayload() {
        return NgpvanTestPayload.builder()
            .paymentMethodType("CreditCard")
            .firstName("First Name")
            .lastName("Last Name")
            .creditCardNumberOrLastFour("4111111111111111")
            .expirationYear("2025")
            .expirationMonth("12")
            .creditCardType("visa")
            .merchantAccountGlobalId("AAAAAAAA-AAAA-AAAA-AAAA-000000000004")
            .invoiceNumber("VGS Fake Invoice Number")
            .amount(new NgpvanTestPayload.NgpvanAmount(501d, "USD"))
            .build();
    }

    static class NgpvanTestPayload {

        record NgpvanAmount(double amount, String currency) {}
        String paymentMethodType;
        String firstName;
        String lastName;
        String creditCardNumberOrLastFour;
        String expirationYear;
        String expirationMonth;
        String creditCardType;
        String merchantAccountGlobalId;
        String invoiceNumber;
        NgpvanAmount amount;

        public static Builder builder() {
            return new Builder();
        }

        public static class Builder {
            private String paymentMethodType;
            private String firstName;
            private String lastName;
            private String creditCardNumberOrLastFour;
            private String expirationYear;
            private String expirationMonth;
            private String creditCardType;
            private String merchantAccountGlobalId;
            private String invoiceNumber;
            private NgpvanAmount amount;

            public Builder paymentMethodType(String paymentMethodType) {
                this.paymentMethodType = paymentMethodType;
                return this;
            }

            public Builder firstName(String firstName) {
                this.firstName = firstName;
                return this;
            }

            public Builder lastName(String lastName) {
                this.lastName = lastName;
                return this;
            }

            public Builder creditCardNumberOrLastFour(String creditCardNumberOrLastFour) {
                this.creditCardNumberOrLastFour = creditCardNumberOrLastFour;
                return this;
            }

            public Builder expirationYear(String expirationYear) {
                this.expirationYear = expirationYear;
                return this;
            }

            public Builder expirationMonth(String expirationMonth) {
                this.expirationMonth = expirationMonth;
                return this;
            }

            public Builder creditCardType(String creditCardType) {
                this.creditCardType = creditCardType;
                return this;
            }

            public Builder merchantAccountGlobalId(String merchantAccountGlobalId) {
                this.merchantAccountGlobalId = merchantAccountGlobalId;
                return this;
            }

            public Builder invoiceNumber(String invoiceNumber) {
                this.invoiceNumber = invoiceNumber;
                return this;
            }

            public Builder amount(NgpvanAmount amount) {
                this.amount = amount;
                return this;
            }

            public NgpvanTestPayload build() {
                NgpvanTestPayload payload = new NgpvanTestPayload();
                payload.paymentMethodType = this.paymentMethodType;
                payload.firstName = this.firstName;
                payload.lastName = this.lastName;
                payload.creditCardNumberOrLastFour = this.creditCardNumberOrLastFour;
                payload.expirationYear = this.expirationYear;
                payload.expirationMonth = this.expirationMonth;
                payload.creditCardType = this.creditCardType;
                payload.merchantAccountGlobalId = this.merchantAccountGlobalId;
                payload.invoiceNumber = this.invoiceNumber;
                payload.amount = this.amount;
                return payload;
            }
        }
    }

    public double findTimeLap(final long startTime, final long endTime) {
        final double average = ((double) (endTime - startTime)) / 1_000_000_000;
        BigDecimal bd = BigDecimal.valueOf(average);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
