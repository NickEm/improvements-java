package com.nickem.util;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.EntityUtils;

@Slf4j
public class HttpRequestUtil {

    public static String executeHttpRequest(HttpClient client, HttpRequestBase request, String service, List<Integer> acceptableStatusCodes) throws IOException {
        try {
            return client.execute(request, getStringResponseHandler(acceptableStatusCodes));
        } catch (IOException e) {
            String message = String.format("Error while performing request to service %s.", service);
            log.error(message, e);
            throw new IOException(message);
        }
    }

    public static URIBuilder prepareUriBuilder(String uri, Map<String, String> requestParams) {
        URIBuilder uriBuilder = prepareUriBuilder(uri);
        requestParams.forEach(uriBuilder::setParameter);

        return uriBuilder;
    }

    public static URIBuilder prepareUriBuilder(String uri) {
        URIBuilder uriBuilder;
        try {
            uriBuilder = new URIBuilder(uri);
        } catch (URISyntaxException e) {
            throw new RuntimeException(String.format("Could not prepare request to service. URI: %s is incorrect", uri));
        }

        return uriBuilder;
    }

    public static ResponseHandler<String> getStringResponseHandler(final List<Integer> acceptableStatusCodes) {
        return response -> {
            int status = response.getStatusLine().getStatusCode();
            if (acceptableStatusCodes.contains(status)) {
                HttpEntity entity = response.getEntity();
                return entity != null ? EntityUtils.toString(entity) : null;
            } else {
                String message = String.format("Unexpected response status code: %s.", status);
                log.error(message);
                throw new IOException(message);
            }
        };
    }
}