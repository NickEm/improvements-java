package com.nickem.util;

import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class PermutationUtil {

    public <T> Set<List<T>> permutations(List<T> input) {
        Set<List<T>> result = new HashSet<>();
        if (input.size() == 1) {
            result.add(input);
        } else if (input.size() > 1) {
            int lastIndex = input.size() - 1;
            // Find out the last element
            T last = input.get(lastIndex);
            // Rest of the elements without last
            List<T> withoutLast = input.stream().limit(lastIndex).collect(Collectors.toList());
            // Perform permutation on the rest of the elements and merge with the last
            result = merge(permutations(withoutLast), last);
        }
        return result;
    }

    private <T> Set<List<T>> merge(Set<List<T>> input, T element) {
        Set<List<T>> result = new HashSet<>();
        for (List<T> current : input) {
            // For each list of elements insert the last character to all possible positions
            for (int i = 0; i <= current.size(); ++i) {
                List<T> currentModification = Lists.newArrayList(current);
                currentModification.add(i, element);
                log.debug("{}", currentModification);
                result.add(currentModification);
            }
        }
        return result;
    }

    public List<String> permutations(String input) {
        List<String> result = new ArrayList<>();
        if (input.length() == 1) {
            result.add(input);
        } else if (input.length() > 1) {
            int lastIndex = input.length() - 1;
            // Find out the last element
            String last = input.substring(lastIndex);
            // Rest of the elements without last
            String withoutLast = input.substring(0, lastIndex);
            // Perform permutation on the rest of the elements and merge with the last
            result = merge(permutations(withoutLast), last);
        }
        return result;
    }

    private List<String> merge(List<String> input, String element) {
        List<String> result = new ArrayList<>();
        for (String current : input) {
            // For each list of elements insert the last character to all possible positions
            for (int i = 0; i <= current.length(); ++i) {
                String currentModification = new StringBuffer(current).insert(i, element).toString();
                log.debug("{}", currentModification);
                result.add(currentModification);
            }
        }
        return result;
    }

    public <T> Set<Set<T>> uniquePermutations(Set<T> input, int depth) {
        Set<Set<T>> resultHolder = new HashSet<>();
        permutations(input, new HashSet<>(), depth, resultHolder);
        return resultHolder;
    }

    private <T> void permutations(Set<T> input, Set<T> prefix, int depth, Set<Set<T>> resultHolder) {
        if (depth == 0) {
            resultHolder.add(prefix);
        } else {
            for (T instance : input) {
                Set<T> newPrefix = new HashSet<>(prefix);
                newPrefix.add(instance);

                permutations(input, newPrefix, depth - 1, resultHolder);
            }
        }
    }

    public void permutationsStringArray(String[] input, String[] inFlight, int depth, Set<String[]> resultHolder) {
        if (depth == 0) {
            resultHolder.add(inFlight);
        } else {
            for (String i : input) {
                String[] newPrefix = new String[inFlight.length + 1];
                System.arraycopy(inFlight, 0, newPrefix, 0, inFlight.length);
                newPrefix[inFlight.length] = i;

                permutationsStringArray(input, newPrefix, depth - 1, resultHolder);
            }
        }
    }

    public void permutationsIntArray(int[] input, int[] inFlight, int depth, Set<int[]> resultHolder) {
        if (depth == 0) {
            resultHolder.add(inFlight);
        } else {
            for (int i : input) {
                int[] newPrefix = new int[inFlight.length + 1];
                System.arraycopy(inFlight, 0, newPrefix, 0, inFlight.length);
                newPrefix[inFlight.length] = i;

                permutationsIntArray(input, newPrefix, depth - 1, resultHolder);
            }
        }
    }


}
