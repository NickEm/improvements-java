package com.nickem.multithreading;

import io.vavr.control.Try;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MultipleThreadInSequence {
    

    public static void main(String[] args) {
        final var foo = new Foo();
        mode1(foo);
//        mode2(foo);
    }

    @SneakyThrows
    public static void mode1(final Foo foo) {
        log.debug("Running with CompletableFuture");
        final var executorService = Executors.newFixedThreadPool(3);

        final var first = new Thread(foo::first);
        final var second = new Thread(foo::second);
        final var third = new Thread(foo::third);

        CompletableFuture.runAsync(first)
                         .thenRunAsync(second, executorService)
                         .thenRunAsync(third, executorService);

        if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
            executorService.shutdownNow();
        }
    }

    public static void mode2(final Foo foo) {
        log.debug("Running with CountDownLatch");
        CountDownLatch latchSecond = new CountDownLatch(1);
        CountDownLatch latchThird = new CountDownLatch(1);

        final var first = new Thread(() -> {
            foo.first();
            latchSecond.countDown();
        });

        final var second = new Thread(() -> {
            Try.run(() -> latchSecond.await());
            foo.second();
            latchThird.countDown();
        });

        final var third = new Thread(() -> {
            Try.run(() -> latchThird.await());
            foo.third();
        });

        third.start();
        second.start();
        first.start();
    }
}

@Slf4j
class Foo {

    public void first() { log.info("first"); }

    public void second() {log.info("second"); }

    public void third() { log.info("third"); }

}
