package com.nickem.multithreading;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ConcurrentCollectionExample {

    public static void main(String[] args) throws InterruptedException {
//        final List<String> arrayList = new CopyOnWriteArrayList<>();
        concurrentList();
    }

    private static void concurrentList() throws InterruptedException {
        final List<String> objects = new CopyOnWriteArrayList<>();
        final ExecutorService executorService = Executors.newFixedThreadPool(2);

        final CountDownLatch latch = new CountDownLatch(2);
        executorService.execute(() -> {
            for (int i = 0; i < 20; i++) {
                try {
                    Thread.sleep(50);
                    objects.add("2");
                    if (i == 19) {
                        latch.countDown();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        executorService.execute(() -> {
            for (int i = 0; i < 20; i++) {
                try {
                    Thread.sleep(50);
                    objects.add(0, "1");
                    if (i == 19) {
                        latch.countDown();
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });
        latch.await();
        log.info("Size: {}", objects.size());
        log.info("{}", objects);
        if (!executorService.awaitTermination(1L, TimeUnit.SECONDS)) {
            executorService.shutdownNow();
        }
    }

    private static void concurrentMapExample() throws InterruptedException {
        final ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();
        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            try {
                Thread.sleep(2000);
                map.put("yes", "yes");
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        waitUntilAvailable(map, "yes");
        if (!executorService.awaitTermination(1L, TimeUnit.SECONDS)) {
            executorService.shutdownNow();
        }
    }

    private static void waitUntilAvailable(Map<String, String> list, String element) throws InterruptedException {
        while (true) {
            if (list.containsKey(element)) {
                log.info("Contains");
                return;
            } else {
                log.info("Doesn't contain");
                Thread.sleep(250);
            }
        }
    }

}
