package com.nickem.multithreading;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExecutorsGuide {

    public static void main(String[] args) {
        final ExecutorsGuide instance = new ExecutorsGuide();
        ScheduledExecutorService executor =  Executors.newScheduledThreadPool(1);
        final Runnable smth = () -> {
            try {
                instance.smth();
            } catch (Exception e) {
                log.error("Exception happened", e);
            }
        };
        executor.scheduleAtFixedRate(smth, 0, 5, TimeUnit.SECONDS);
    }

    private void smth() {
        log.info("smth is executed");
        throw new IllegalArgumentException();
    }




}
