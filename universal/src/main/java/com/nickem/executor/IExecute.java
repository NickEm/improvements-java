package com.nickem.executor;

/**
 * @author Mykola Morozov.
 */
public interface IExecute {

    void execute();
}
