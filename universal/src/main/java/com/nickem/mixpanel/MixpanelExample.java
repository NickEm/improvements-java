package com.nickem.mixpanel;

import com.mixpanel.mixpanelapi.ClientDelivery;
import com.mixpanel.mixpanelapi.MessageBuilder;
import com.mixpanel.mixpanelapi.MixpanelAPI;
import com.nickem.executor.IExecute;
import java.io.IOException;
import java.time.LocalDateTime;
import org.apache.commons.lang3.tuple.Pair;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Mykola Morozov.
 */
public class MixpanelExample implements IExecute {

    private static final String TEST_TOKEN = "de29d71d04ace842c664e1a932e5ac04";

    private static final String SIGN_IN = "Sign in";
    private static final String LOG_IN = "Log in";

    private static final String PUBLISHED_PRESENTATION = "Published Presentation";
    private static final String ENABLED_GUESTBOOK = "Enabled Guestbook";
    private static final String ENHANCED_PRESENTATION = "Enhanced Presentation";
    private static final String ADDED_NOTIFICATION = "Added Notification";
    private static final String EDITED_PRESENTATION = "Edited Presentation";
    private static final String DELETED_PRESENTATION = "Deleted Presentation";

    private static final String SELECT_SLIDES = "Select Slides";
    private static final String SELECT_MEDIA = "Select Media";
    private static final String SYNCHRONIZE = "Synchronize";

    private static final String PROPERTY_TOOL = "Tool";
    private static final String PROPERTY_TEMPLATE = "Template";

    private static final String TOOL_KVSTUDIO = "KVStudio";
    private static final String TOOL_KNOVIO_WEB = "Knovio Web";
    private static final String TOOL_KNOVIO_MOBILE = "Knovio Mobile";
    private static final String TOOL_MATCH = "Match";

    private static final String TEMPLATE_LEGACY = "Legacy";
    private static final String TEMPLATE_ENHANCED = "Enhanced";

    private static final String REPLACED_TEMPLATE = "Replaced Template";

    private static final String PROFILE_LABEL_FIRST_NAME = "first_name";
    private static final String PROFILE_LABEL_LAST_NAME = "last_name";
    private static final String PROFILE_LABEL_CREATED = "created";
    private static final String PROFILE_LABEL_EMAIL = "email";
    private static final String PROFILE_LABEL_ACCOUNT_ID = "account_id";
    private static final String PROFILE_LABEL_PARENT_ID = "parent_id";
    private static final String PROFILE_LABEL_ORGANIZATION = "organization";
    private static final String PROFILE_LABEL_ACCOUNT_PLAN_ID = "account_plan_id";
    private static final String PROFILE_LABEL_ACCOUNT_PLAN_NAME = "account_plan_name";
    private static final String PROFILE_LABEL_USER_ID = "user_id";
    private static final String PROFILE_LABEL_USER_TYPE_ID = "user_type_id";
    private static final String PROFILE_LABEL_USER_TYPE = "account_plan_name";
    private static final String PROFILE_LABEL_USER_ROLE_ID = "account_plan_name";
    private static final String PROFILE_LABEL_USER_ROLE = "account_plan_name";

    private static final MixpanelAPI mixpanelClient = new MixpanelAPI();

    @Override
    public void execute() {
        try {
            updateProfile(1L, 2L);
//            sendEvents(1L, 1L);
//            sendPresentationPrepareEvents(892L, 16331L);
//            sendEvents(892L, 16331L);
        } catch (Exception e) {
            System.out.println("An exception occurred during profile creation");
        }
    }

    private void updateProfile(Long accountId, Long userId) throws JSONException, IOException {
        MessageBuilder messageBuilder = createMessageBuilder();

        JSONObject props = new JSONObject();
        props.put(PROFILE_LABEL_FIRST_NAME, "Jack");
        props.put(PROFILE_LABEL_LAST_NAME, "Nicholson");
        props.put(PROFILE_LABEL_CREATED, LocalDateTime.now());
        props.put(PROFILE_LABEL_EMAIL, "will.smith@gmail.com");
        props.put(PROFILE_LABEL_ACCOUNT_ID, accountId);
        props.put(PROFILE_LABEL_USER_ID, userId);
        props.put(PROFILE_LABEL_ORGANIZATION, "Pirates Bay");
        JSONObject update = messageBuilder.set(buildMixpanelId(accountId, userId), props);

        mixpanelClient.sendMessage(update);
    }

    private void sendEvents(Long accountId, Long userId) throws JSONException, IOException {
        MessageBuilder messageBuilder = createMessageBuilder();

        JSONObject publishEventProp = buildStringEventProperties(
                Pair.of(PROPERTY_TOOL, TOOL_KNOVIO_WEB),
                Pair.of(PROPERTY_TEMPLATE, TEMPLATE_ENHANCED));

        JSONObject editEventProp = buildStringEventProperties(Pair.of(PROPERTY_TOOL, TOOL_KNOVIO_WEB));

        JSONObject eventFirst = messageBuilder.event(buildMixpanelId(accountId, userId), PUBLISHED_PRESENTATION, publishEventProp);

//        JSONObject eventFirst = messageBuilder.event(buildMixpanelId(accountId, userId), PUBLISHED_PRESENTATION, null);
        JSONObject eventSecond = messageBuilder.event(buildMixpanelId(accountId, userId), ENABLED_GUESTBOOK, null);
        JSONObject eventThird = messageBuilder.event(buildMixpanelId(accountId, userId), ENHANCED_PRESENTATION, null);
        JSONObject eventFourth = messageBuilder.event(buildMixpanelId(accountId, userId), ADDED_NOTIFICATION, null);
        JSONObject eventFifth = messageBuilder.event(buildMixpanelId(accountId, userId), EDITED_PRESENTATION, editEventProp);
        JSONObject eventSixth = messageBuilder.event(buildMixpanelId(accountId, userId), DELETED_PRESENTATION, null);

        sendEvents(eventFirst);
//        sendEvents(eventFirst, eventSecond);
//        sendEvents(eventFirst, eventSecond, eventThird);
//        sendEvents(eventFirst, eventSecond, eventThird, eventFourth);
//        sendEvents(eventFirst, eventSecond, eventThird, eventFourth, eventFifth);
//        sendEvents(eventFirst, eventSecond, eventThird, eventFourth, eventFifth, eventSixth);
    }

    private void sendPresentationPrepareEvents(Long accountId, Long userId) throws JSONException, IOException {
        MessageBuilder messageBuilder = createMessageBuilder();

        JSONObject event_1 = messageBuilder.event(buildMixpanelId(accountId, userId), SELECT_SLIDES, null);
        JSONObject event_2 = messageBuilder.event(buildMixpanelId(accountId, userId), SELECT_MEDIA, null);
        JSONObject event_3 = messageBuilder.event(buildMixpanelId(accountId, userId), SYNCHRONIZE, null);
        JSONObject event_4 = messageBuilder.event(buildMixpanelId(accountId, userId), PUBLISHED_PRESENTATION, null);

//        sendEvents(event_1);
        sendEvents(event_1, event_2);
//        sendEvents(event_1, event_2, event_3);
//        sendEvents(event_1, event_2, event_3, event_4);
    }

    private void sendEvents(JSONObject... props) throws IOException {
        ClientDelivery delivery = new ClientDelivery();
        for (JSONObject prop : props) {
            delivery.addMessage(prop);
        }

        mixpanelClient.deliver(delivery);
    }

    private MessageBuilder createMessageBuilder() {
        return new MessageBuilder(TEST_TOKEN);
    }

    private String buildMixpanelId(Long accountId, Long userId) {
        return String.format("%s_%s", accountId, userId);
    }

    private JSONObject buildStringEventProperties(Pair<String, String> ... pairs) throws JSONException {
        JSONObject eventProperties = new JSONObject();
        for(Pair<String, String> pair: pairs) {
            eventProperties.put(pair.getKey(), pair.getValue());
        }

        return  eventProperties;
    }


}
