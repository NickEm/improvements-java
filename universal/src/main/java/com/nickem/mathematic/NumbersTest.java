package com.nickem.mathematic;

import com.google.common.base.Functions;
import com.google.common.primitives.Chars;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

public class NumbersTest {

    @Test
    public void test() {
        for (int i = 0; i < 100000; i++) {
            final Integer sum =
                    Chars.asList(String.valueOf(i).toCharArray()).stream().map(Functions.toStringFunction())
                        .collect(Collectors.toList())
                         .stream()
                         .map(Integer::valueOf)
                         .reduce(0, (a, b) -> a + b);
            if (sum  == i / 12 && i % 12 == 0) {
                System.out.println("Yuhu: " + i);
            }
        }
    }

    @Test
    public void test1(){
        System.out.println(LocalDate.parse("January 7, 2018", DateTimeFormatter.ofPattern("MMMM d, yyyy")));
//        System.out.println(LocalDate.parse("June 17, 2007", DateTimeFormatter.ofPattern("MMMM d, yyyy")));
    }
}

