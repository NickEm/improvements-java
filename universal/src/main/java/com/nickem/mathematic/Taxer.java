package com.nickem.mathematic;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Taxer {

    public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);

    public BigDecimal sum(BigDecimal ...numbers) {
        BigDecimal result = new BigDecimal(0);
        for (BigDecimal number : numbers) {
            result = result.add(number);
        }
        return result;
    }

    public BigDecimal tax(BigDecimal number) {
        return tax(number, new BigDecimal(5));
    }

    public BigDecimal tax(BigDecimal number, BigDecimal percentage) {
        return percentage(number, percentage);
    }

    private BigDecimal percentage(BigDecimal base, BigDecimal percentage){
        return base.multiply(percentage).divide(ONE_HUNDRED, RoundingMode.HALF_UP);
    }
}
