package com.nickem.mathematic;

public class EllipseCalculation {

    public static void main(String[] args) {
        double ceilingLength = 4.3;
        double ceilingWeight = 3.1;

        double lengthPadding = 0.4;
        double weightPadding = 0.3;

        double a = (ceilingLength - lengthPadding)/2;
        double b = (ceilingWeight - weightPadding)/2;

        double focus = Math.sqrt(a*a - b*b);

        double ropeLength = 2 * a;

        System.out.println("Focus length is " + focus);
        System.out.println("Rope length is " + ropeLength);
    }
}
