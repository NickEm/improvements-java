package com.nickem.java8;

/**
 * @author Mykola Morozov.
 */
/*@FunctionalInterface*/
interface Converter<F, T> {
    T convert(F from);
}
