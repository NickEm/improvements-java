package com.nickem.java8;

public class WinnerClass {

    public static Class getClassObjectByWinnerRate(Integer n) {
        switch (n) {
            case 2: return WinnerClass2.class;
            case 3: return WinnerClass3.class;
            default: return null;
        }
    }
}
