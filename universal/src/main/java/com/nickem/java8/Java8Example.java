package com.nickem.java8;

import com.nickem.executor.IExecute;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Mykola Morozov.
 */
@Slf4j
public class Java8Example implements IExecute {

    public static void main(String[] args) {
        new Java8Example().myTest();
    }

    private void myTest() {
        wordsInFileCounter();
    }

    @Override
    public void execute() {
        try {
            log.info("Words count is: " + wordsInFileCounter("/home/mykola/git-repos/own/improvements-java/src/main/resources/file-with-words.txt"));
        } catch (Exception e) {
            log.error("", e);
        }
//        groupNumberByWinnerClass(new Integer[]{1,2,3,2,4,3,3,5,3});
    }

    public void start(){
        System.out.println("Start java8 package");
        Converter<String, Integer> converter = (from) -> Integer.valueOf(from);
        Integer converted = converter.convert("123");
        System.out.println(converted);    // 123

        workWithStreams();
    }

    private void workWithStreams() {
        List<String> stringCollection = new ArrayList<>();
        stringCollection.add("ddd2");
        stringCollection.add("aaa2");
        stringCollection.add("bbb1");
        stringCollection.add("aaa1");
        stringCollection.add("bbb3");
        stringCollection.add("ccc");
        stringCollection.add("bbb2");
        stringCollection.add("ddd1");

        stringCollection
                .stream()
                .filter(s -> s.startsWith("a"))
                .forEach(System.out::println);
    }

    class User {
        String name;

        public User(final String name) {
            this.name = name;
        }
    }

    private void orderUsers() {
        final TreeSet<User> set = new TreeSet<>();
        set.add(new User("Mykola"));
        set.add(new User("Oleksii"));
        System.out.println(set);
    }


    private static void orderJoinStreams() {
        Stream<String> names1 = Stream.of("Mykola", "Dmytro", "Vova");
        Stream<String> names2 = Stream.of("Oleksii", "Andrii");

        Stream.of(names1, names2).flatMap(Function.identity())
              .sorted()
              .forEach(System.out::println);
    }

    public void groupNumberByWinnerClass(Integer[] rightNumberCount){
        Map<Class<? extends WinnerClass>, Integer> groupedWinners = new HashMap<>();
        for (Integer number : rightNumberCount) {
            if (number > 1) {
                groupedWinners.merge(WinnerClass.getClassObjectByWinnerRate(number), 1, (oldValue, newValue) -> oldValue + newValue);
            }
        }

        groupedWinners.forEach((k, v) -> log.info("Class {} has {} winners", k, v));
    }


    private long wordsInFileCounter(String filePath) throws IOException {
        return Files.lines(Paths.get(filePath))
                .flatMap(line -> Arrays.stream(line.trim().split(" ")))
                .map(String::trim)
                .filter(word -> !word.isEmpty())
                .count();
    }

    private void wordsInFileCounter(String ...args) {
        String text = "text {0} and again {1} {2}";
        for (int i = 0; i < args.length; i++) {
            final String placeholder = String.format("{%s}", i);
            if (text.contains(placeholder)) {
                text = text.replace(placeholder, args[i]);
            }
        }
        System.out.println(text);
    }

}
