package com.nickem.javabasic;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TryCatch {

    public static void main(String[] args) {
        try {
            log.info("In try");
            throw new RuntimeException();
        } catch (Exception e) {
            log.info("In catch");
            throw new RuntimeException();
        } finally {
            log.info("In finally");
        }
    }
}
