package com.nickem.codewars.algorithms;

import java.util.List;

public class PaginationHelper<I> {

    private final List<I> collection;
    private final int pageSize;

    /**
     * The constructor takes in an array of items and a integer indicating how many
     * items fit within a single page
     */
    public PaginationHelper(List<I> collection, int itemsPerPage) {
        this.collection = collection;
        this.pageSize = itemsPerPage;
    }

    /**
     * returns the number of items within the entire collection
     */
    public int itemCount() {
        return collection.size();
    }

    /**
     * returns the number of pages
     */
    public int pageCount() {
        return collection.size() % pageSize > 0 ? collection.size() / pageSize + 1 : collection.size() / pageSize;
    }

    /**
     * returns the number of items on the current page. page_index is zero based.
     * this method should return -1 for pageIndex values that are out of range
     */
    public int pageItemCount(int pageIndex) {
        final int pageCount = pageCount();

        if (pageIndex + 1 > pageCount || pageIndex < 0) {
            return -1;
        } else {
            return pageCount - 1 == pageIndex ? collection.size() % pageSize : pageSize;
        }
    }

    /**
     * determines what page an item is on. Zero based indexes
     * this method should return -1 for itemIndex values that are out of range
     */
    public int pageIndex(int itemIndex) {
        if (itemIndex >= collection.size() || itemIndex < 0) {
            return -1;
        } else {
            final int itemPerPage = itemIndex / pageSize;
            final boolean firstPage = itemPerPage <= 0;
            if (firstPage) {
                return 0;
            } else {
                return itemIndex % pageSize > 0 ? itemPerPage : itemPerPage - 1;
            }
        }
    }
}
