package com.nickem.codewars.algorithms;

import java.util.Arrays;

/*
* https://www.codewars.com/kata/5a092d9e46d843b9db000064
*/
public class ArrayElementParity {
    public static int solve(final int [] arr){
        return Arrays.stream(arr).distinct().sum();
    }
}
