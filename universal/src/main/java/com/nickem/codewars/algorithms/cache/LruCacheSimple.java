package com.nickem.codewars.algorithms.cache;

import com.google.common.collect.ImmutableMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LruCacheSimple<K, V> implements LruCache<K, V> {

    private final int capacity;

    private final Map<K, V> elements;

    public LruCacheSimple(int capacity) {
        this.capacity = capacity;
        this.elements = new LinkedHashMap<>(16, 0.75f, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                return this.size() > capacity;
            }
        };
    }

    public V get(K key) {
        return elements.get(key);
    }

    public V put(K key, V value) {
        return elements.put(key, value);
    }

    public void delete(K key) {
        elements.remove(key);
    }

    public int size() {
        return elements.size();
    }

    public int getCapacity() {
        return capacity;
    }

    public Map<K, V> getElements() {
        return ImmutableMap.copyOf(elements);
    }
}
