package com.nickem.codewars.algorithms.cache;

import java.util.Map;

public interface LruCache<K, V> {

    V get(K key);

    V put(K key, V value);

    void delete(K key);

    int size();

    int getCapacity();

    Map<K, V> getElements();

}
