package com.nickem.codewars.algorithms.cache;

public record CacheElement<K, V>(K key, V value) {

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }
}
