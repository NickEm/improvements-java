package com.nickem.codewars.algorithms;

import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;

/*
* https://www.codewars.com/kata/5f120a13e63b6a0016f1c9d5
*/
@Slf4j
public class ConsecutiveSum {

    /*This doesn't pass performance test*/
    public static int consecutiveSum(final int num){
        int result = 1;
        for (int i = 1; i <= num / 2 + 1; i++) {
            int intermediate = i;
            for (int j = i + 1; j <= num / 2 + 1; j++) {
                intermediate += j;
                if (intermediate > num) {
                    break;
                } else if (intermediate == num) {
                    log.info("i: {}, j: {}", i, j);
                    result++;
                    break;
                }
            }
        }
        return result;
    }
}
