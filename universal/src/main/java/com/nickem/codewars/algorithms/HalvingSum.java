package com.nickem.codewars.algorithms;

/*
* https://www.codewars.com/kata/5a58d46cfd56cb4e8600009d
*/
public class HalvingSum {
    public static int halvingSum(final int n) {
        int sum = n;
        int i = 2;

        while (n / i != 0) {
            sum += n / i;
            i *= 2;
        }
        return sum;
    }
}
