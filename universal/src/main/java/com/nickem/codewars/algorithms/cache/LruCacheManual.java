package com.nickem.codewars.algorithms.cache;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

public class LruCacheManual<K, V> implements LruCache<K, V> {

    private final int capacity;
    private final Map<K, CacheElement<K,V>> linkedListElementMap;
    private final LinkedList<CacheElement<K,V>> elements;
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    public LruCacheManual(int capacity) {
        this.capacity = capacity;
        linkedListElementMap = new ConcurrentHashMap<>(capacity);
        elements = new LinkedList<>();
    }


    @Override
    public V get(K key) {
        this.lock.readLock().lock();
        try {
            if (linkedListElementMap.containsKey(key)) {
                final CacheElement<K,V> element = linkedListElementMap.get(key);
                /*Remove has linear complexity for LinkedList*/
                elements.remove(element);
                elements.addLast(element);
                return element.getValue();
            } else {
                return null;
            }
        } finally {
            this.lock.readLock().unlock();
        }
    }

    @Override
    public V put(K key, V value) {
        this.lock.writeLock().lock();
        try {
            if (linkedListElementMap.containsKey(key)) {
                final CacheElement<K, V> old = linkedListElementMap.get(key);
                final CacheElement<K, V> element = new CacheElement<>(key, value);
                linkedListElementMap.put(key, element);
                /*Remove has linear complexity for LinkedList*/
                elements.remove(old);
                elements.addLast(old);
                return old.getValue();
            } else {
                if (linkedListElementMap.size() == this.capacity) {
                    final CacheElement<K, V> old = elements.getFirst();
                    linkedListElementMap.remove(old.getKey());
                    elements.remove(old);
                }
                final CacheElement<K, V> element = new CacheElement<>(key, value);
                linkedListElementMap.put(key, element);
                elements.addLast(element);
                return null;
            }
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    @Override
    public void delete(K key) {
        this.lock.writeLock().lock();
        try {
            if (linkedListElementMap.containsKey(key)) {
                final CacheElement<K, V> old = linkedListElementMap.get(key);
                linkedListElementMap.remove(key);
                /*Remove has linear complexity for LinkedList*/
                elements.remove(old);
            }
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    @Override
    public int size() {
        return linkedListElementMap.size();
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public Map<K, V> getElements() {
        this.lock.readLock().lock();
        try {
            return elements.stream().collect(Collectors.toMap(
                CacheElement::getKey, CacheElement::getValue,
                (o, n) -> n,
                LinkedHashMap::new));
        } finally {
            this.lock.readLock().unlock();
        }
    }
}
