package com.nickem.codewars.security;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class ShaCracker {

    public static String crackSha256(String hash, String chars) {
        String results = null;
        List<String> possibleWords = new ArrayList<>();
        generatePossibleWordsWithoutLetterDuplication(chars.toCharArray(), "", chars.length(), possibleWords);

//        for (String possibleWord : possibleWords) {
//            System.out.println(possibleWord);
//        }
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-256");
            for (String word : possibleWords) {

                byte[] encodedHash = digest.digest(
                        word.getBytes(StandardCharsets.UTF_8));
                String sha256hex = bytesToHex(encodedHash);
                if (hash.equals(sha256hex)) {
                    results = word;
                    break;
                }
        }

        } catch (NoSuchAlgorithmException e) {
            /*NOP*/
        }


        return results;
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte aHash : hash) {
            String hex = Integer.toHexString(0xff & aHash);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static void generatePossibleWordsWithoutLetterDuplication(char set[], String prefix, int wordLength, List<String> resultHolder) {
        // Means that we have reached word length limit
        if (wordLength == 0) {
            resultHolder.add(prefix);
        } else {
            for (int i = 0; i < set.length; ++i) {
                String newPrefix = prefix + set[i];
                generatePossibleWordsWithoutLetterDuplication(removeElement(set, i), newPrefix, wordLength - 1, resultHolder);
            }
        }
    }

    public static char[] removeElement(char[] source, int removedIdx) {
        char[] result = new char[source.length - 1];
        int skipElementIndex = 0;
        for (int i = 0; i < source.length; i++) {
            if (i != removedIdx) {
                result[i-skipElementIndex] = source[i];
            } else {
                skipElementIndex++;
            }
        }
        return result;
    }

    private static void generatePossibleWords(char set[], String prefix, int wordLength, List<String> resultHolder) {
        // Means that we have reached word length limit
        if (wordLength == 0) {
            resultHolder.add(prefix);
        } else {
            for (char aSet : set) {
                String newPrefix = prefix + aSet;
                // wordLength is decreased, because we have added a new character
                generatePossibleWords(set, newPrefix, wordLength - 1, resultHolder);
            }
        }
    }
}
