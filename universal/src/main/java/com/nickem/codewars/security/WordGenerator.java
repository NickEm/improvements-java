package com.nickem.codewars.security;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class WordGenerator {

    public static void main(String[] args) {
        System.out.println("Let's start this");
        WordGenerator instance = new WordGenerator();
        List<String> results = new ArrayList<>();
        char set2[] = {'a', 'b', 'c', 'd'};
        int k = 4;
        instance.generatePossibleWords(set2, StringUtils.EMPTY, k, results);
        log.info("{}", results);
    }

    /**
     * Recursive method to print all possible strings of length wordLength
     *
     * @param set - source set of characters for a word
     * @param prefix - that is actual result of the word
     * @param wordLength - that is length of word that should be generated
     */
    public void generatePossibleWords(char set[], String prefix, int wordLength, List<String> resultHolder) {
        // Means that we have reached word length limit
        if (wordLength == 0) {
            resultHolder.add(prefix);
        } else {
            for (char aSet : set) {
                String newPrefix = prefix + aSet;
                // wordLength is decreased, because we have added a new character
                generatePossibleWords(set, newPrefix, wordLength - 1, resultHolder);
            }
        }
    }

    public static void generatePossibleWordsWithoutLetterDuplication(char set[], String prefix, int wordLength, List<String> resultHolder) {
        // Means that we have reached word length limit
        if (wordLength == 0) {
            resultHolder.add(prefix);
        } else {
            for (int i = 0; i < set.length; ++i) {
                String newPrefix = prefix + set[i];
                generatePossibleWordsWithoutLetterDuplication(ArrayUtils.removeElement(set, set[i]), newPrefix, wordLength - 1, resultHolder);
            }
        }
    }

}
