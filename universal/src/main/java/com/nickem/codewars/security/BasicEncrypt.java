package com.nickem.codewars.security;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class BasicEncrypt {

    private String encrypt(String text, int rule) {
        StringBuilder result = new StringBuilder();
        for (char c : text.toCharArray()) {
            int ascii = ((int) c + rule) % 256;
            result.append(Character.toString((char) ascii));
        }
        return result.toString();
    }

    @Test
    public void testDecrypt() throws Exception {
        BasicEncrypt enc = new BasicEncrypt();
        assertEquals("text = '', rule = 1", "",
                enc.encrypt("", 1));
        assertEquals("text = 'a', rule = 1", "b",
                enc.encrypt("a", 1));
        assertEquals("text = 'bztmubic', rule = 200", "*B<5=*1+",
                enc.encrypt("bztmubic", 200));
        assertEquals("text = 'please encrypt me', rule = 2", "rngcug\"gpet{rv\"og",
                enc.encrypt("please encrypt me", 2));
    }

}
