package com.nickem.mathematic;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class TaxerTest {

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            /*1-quarter*/
            Arguments.of(ImmutableList.of(
                new BigDecimal("28373"), new BigDecimal("41857"),
                new BigDecimal("172132.16"), new BigDecimal("156752.73"), new BigDecimal("156996.13")),
                new BigDecimal("556111.02"),
                new BigDecimal("27805.55")
            ),
            /*2-quarter*/
            Arguments.of(ImmutableList.of(
                new BigDecimal("165851.00"), new BigDecimal("161755.32"), new BigDecimal("164563.79")),
                new BigDecimal("492170.11"),
                new BigDecimal("24608.51")
            ),
            /*3-quarter*/
            Arguments.of(ImmutableList.of(
                new BigDecimal("261105.25"), new BigDecimal("199341.66"), new BigDecimal("223316.03")),
                new BigDecimal("683762.94"),
                new BigDecimal("34188.15")
            ),
            /*1-to-3-quarters*/
            Arguments.of(ImmutableList.of(
                new BigDecimal("556111.02"), new BigDecimal("492170.11"), new BigDecimal("683762.94")),
                new BigDecimal("1732044.07"),
                new BigDecimal("86602.20")
            ),
            /*4-quarter*/
            Arguments.of(ImmutableList.of(
                new BigDecimal("213420.57"), new BigDecimal("212177.23"), new BigDecimal("203803.02"), new BigDecimal("219990.12")),
                new BigDecimal("849390.94"),
                new BigDecimal("42469.55")
            ),
            /*year*/
            Arguments.of(ImmutableList.of(
                    new BigDecimal("556111.02"), new BigDecimal("492170.11"), new BigDecimal("683762.94"), new BigDecimal("849390.94")),
                new BigDecimal("2581435.01"),
                new BigDecimal("129071.75")
            )
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void testSum(final List<BigDecimal> input, BigDecimal income, BigDecimal tax) {
        Taxer instance = new Taxer();
        final BigDecimal sum = instance.sum(input.toArray(new BigDecimal[0]));
        assertThat(sum).as("total income").isEqualTo(income);
        assertThat(instance.tax(sum)).as("calculated tax").isEqualTo(tax);
    }

}