package com.nickem.mathematic;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Test;

@Slf4j
public class RandomDice {

    @Test
    public void testExample() {
        final Map<Integer, Integer> scores = new HashMap<>();
        final int runs = 100_000;
        for (int i = 0; i < runs; i++) {
            int score = getRandom();
            scores.compute(score, (key, value) -> value == null ? 1 : value + 1);
        }

        scores.entrySet().stream().sorted(Entry.comparingByKey()).forEach((entry) -> {
            log.info("For score [{}] there were [{}%] occurrences", entry.getKey(), Precision.round((double) entry.getValue() / runs, 2) * 100);
        });
    }

    public int getRandom() {
        final Random random = new Random();
        return random.nextInt(1, 7);
    }


}
