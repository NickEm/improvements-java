package com.nickem.mathematic;

import java.util.Random;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@Slf4j
public class MontyHallProblem {

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(true),
            Arguments.of(false)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void testExample(final boolean selectTheSameDoor) {
        int winCount = 0;
        final int runs = 1_000_000;
        for (int i = 0; i < runs; i++) {
            final int doorWithCar = chooseRandomDoor();
            final int firstSelectedDoor = chooseRandomDoor();
            final int openedDoor = chooseThirdDoor(doorWithCar, firstSelectedDoor);
            if (selectTheSameDoor) {
                if (doorWithCar == firstSelectedDoor) {
                    winCount++;
                }
            } else {
                /*change the door*/
                final int chooseThirdDoor = chooseThirdDoor(firstSelectedDoor, openedDoor);
                if (doorWithCar == chooseThirdDoor) {
                    winCount++;
                }
            }
        }
        log.info("For selectingTheSameDoor={} there were [{}%] of success", selectTheSameDoor, Precision.round((double) winCount / runs, 2) * 100);
    }

    public int chooseRandomDoor() {
        final Random random = new Random();
        return random.nextInt(1, 4);
    }

    public int chooseThirdDoor(final int firstSelectedDoor, final int openedDoor) {
        for (int i = 1; true; i++) {
            if (i != firstSelectedDoor && i != openedDoor) {
                return i;
            }
        }
    }

}
