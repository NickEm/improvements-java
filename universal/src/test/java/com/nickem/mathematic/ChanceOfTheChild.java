package com.nickem.mathematic;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@Slf4j
public class ChanceOfTheChild {
    AtomicInteger male = new AtomicInteger(0);
    AtomicInteger female = new AtomicInteger(0);

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(100_000_000),
            Arguments.of(200_000_000),
            Arguments.of(300_000_000),
            Arguments.of(400_000_000),
            Arguments.of(500_000_000),
            Arguments.of(600_000_000),
            Arguments.of(700_000_000),
            Arguments.of(800_000_000),
            Arguments.of(900_000_000)
        );
    }
    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void testExample(int familyCount) {
        int maxAttemptToGetFirstMan = 0;
        for (int family = 0; family < familyCount; family++) {
            int sex = born();
            increaseSex(sex);
            for (int attempt = 1; attempt < Integer.MAX_VALUE; attempt++) {
                sex = born();
                increaseSex(sex);
                if (sex != 0) {
                    maxAttemptToGetFirstMan = Math.max(maxAttemptToGetFirstMan, attempt);
                } else {
                    break;
                }
            }
        }
        log.info("For [{}] families: Male: [{}], Female: [{}], Max attempt: [{}]", familyCount, male.get(), female.get(), maxAttemptToGetFirstMan);
    }

    public void increaseSex(int sex) {
        if (sex == 0) {
            male.getAndIncrement();
        } else {
            female.getAndIncrement();
        }
    }

    public int born() {
        final Random random = new Random();
        return random.nextInt(0, 2);
    }

}
