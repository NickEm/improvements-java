package com.nickem.util;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@Slf4j
class PermutationUtilTest {

    private static Stream<Arguments> assertionPermutationProvider() {
        return Stream.of(
            Arguments.of(
                ImmutableList.of("A", "A"),
                ImmutableSet.of(ImmutableList.of("A", "A"))
            ), Arguments.of(
                ImmutableList.of("A", "B"),
                ImmutableSet.of(ImmutableList.of("A", "B"), ImmutableList.of("B", "A"))
            ), Arguments.of(
                ImmutableList.of("A", "B", "C"),
                ImmutableSet.of(
                    ImmutableList.of("A", "B", "C"), ImmutableList.of("A", "C", "B"),
                    ImmutableList.of("B", "A", "C"), ImmutableList.of("B", "C", "A"),
                    ImmutableList.of("C", "A", "B"), ImmutableList.of("C", "B", "A")
                )
            ), Arguments.of(
                ImmutableList.of("A", "A", "A", "B", "B", "B"),
                ImmutableSet.of(
                    ImmutableList.of("A", "A", "A", "B", "B", "B"),
                    ImmutableList.of("A", "A", "B", "A", "B", "B"),
                    ImmutableList.of("B", "B", "A", "B", "A", "A"),
                    ImmutableList.of("B", "B", "A", "A", "A", "B"),
                    ImmutableList.of("A", "A", "B", "B", "B", "A"),
                    ImmutableList.of("B", "B", "A", "A", "B", "A"),
                    ImmutableList.of("A", "A", "B", "B", "A", "B"),
                    ImmutableList.of("B", "A", "B", "A", "B", "A"),
                    ImmutableList.of("B", "B", "B", "A", "A", "A"),
                    ImmutableList.of("A", "B", "B", "B", "A", "A"),
                    ImmutableList.of("A", "B", "B", "A", "A", "B"),
                    ImmutableList.of("A", "B", "B", "A", "B", "A"),
                    ImmutableList.of("A", "B", "A", "B", "B", "A"),
                    ImmutableList.of("A", "B", "A", "A", "B", "B"),
                    ImmutableList.of("A", "B", "A", "B", "A", "B"),
                    ImmutableList.of("B", "A", "A", "B", "B", "A"),
                    ImmutableList.of("B", "A", "A", "A", "B", "B"),
                    ImmutableList.of("B", "A", "B", "B", "A", "A"),
                    ImmutableList.of("B", "A", "B", "A", "A", "B"),
                    ImmutableList.of("B", "A", "A", "B", "A", "B")
                )
            )
        );
    }

    @ParameterizedTest
    @MethodSource("assertionPermutationProvider")
    public void test(final List<String> input, Set<List<String>> expected) {
        assertThat(PermutationUtil.permutations(input)).isEqualTo(expected);
    }

    private static Stream<Arguments> uniquePermutationsProvider() {
        return Stream.of(
            Arguments.of(ImmutableSet.of("A", "B", "C", "D"), 4,
                ImmutableSet.of(
                ImmutableSet.of("A"),
                ImmutableSet.of("B"),
                ImmutableSet.of("C"),
                ImmutableSet.of("D"),
                ImmutableSet.of("A", "B"),
                ImmutableSet.of("A", "C"),
                ImmutableSet.of("A", "D"),
                ImmutableSet.of("B", "C"),
                ImmutableSet.of("B", "D"),
                ImmutableSet.of("C", "D"),
                ImmutableSet.of("A", "B", "C"),
                ImmutableSet.of("A", "B", "D"),
                ImmutableSet.of("A", "C", "D"),
                ImmutableSet.of("B", "C", "D"),
                ImmutableSet.of("A", "B", "C", "D")
                )
            ),
            Arguments.of(ImmutableSet.of("A", "B"), 2,
                ImmutableSet.of(
                    ImmutableSet.of("A"),
                    ImmutableSet.of("B"),
                    ImmutableSet.of("A", "B")
                )
            ),
            Arguments.of(ImmutableSet.of("A", "B", "C"), 2,
                ImmutableSet.of(
                    ImmutableSet.of("A"),
                    ImmutableSet.of("B"),
                    ImmutableSet.of("C"),
                    ImmutableSet.of("A", "B"),
                    ImmutableSet.of("A", "C"),
                    ImmutableSet.of("B", "C")
                )
            )
        );
    }

    @ParameterizedTest
    @MethodSource("uniquePermutationsProvider")
    public void testUniquePermutations(Set<String> provided, int depth, Set<Set<String>> expected) {
        final Set<Set<String>> result = PermutationUtil.uniquePermutations(provided, depth);
        assertThat(result).containsExactlyInAnyOrderElementsOf(expected);
    }


    @Test
    public void test() {
        final String[] strings = {"A", "B", "C"};
        final Set<String[]> result = new HashSet<>();
        PermutationUtil.permutationsStringArray(strings, new String[0], 3, result);
        final List<List<String>> collect = result.stream().map(Arrays::asList).collect(Collectors.toList());
        log.info("{}", collect);
    }

    private <T extends String> String prepareLine(Collection<T> input) {
        final String collect = String.join("\",\"", input);
        return String.format("ImmutableSet.of(\"%s\")", collect);
    }

}