package com.nickem.java8;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;

@Slf4j
public class CompletableFutureTest {

    @Test
    public void supplyAsyncTest() throws ExecutionException, InterruptedException {
        final double price = 1.00;
        final CompletableFuture<Double> doubleCompletableFuture =
                CompletableFuture.supplyAsync(() -> returnValueWithDealy(price, 2000));
        assertThat(doubleCompletableFuture.get()).isEqualTo(price);
    }

    @Test
    public void supplyAsyncWithApplyTest() throws ExecutionException, InterruptedException {
        final double price = 2.00;
        final double tax = 0.25;

        final CompletableFuture<Double> doubleCompletableFuture =
                CompletableFuture.supplyAsync(() -> returnValueWithDealy(price, 1000)).thenApply(p -> p + tax);
        assertThat(doubleCompletableFuture.get()).isEqualTo(price + tax);
    }

    @Test
    public void supplyAsyncWithComposeTest() throws ExecutionException, InterruptedException {
        final double price = 2.00;
        final double tax = 0.25;

        final CompletableFuture<Double> doubleCompletableFuture =
                CompletableFuture.supplyAsync(() -> returnValueWithDealy(price, 1500))
                                 .thenCompose(
                                         s -> CompletableFuture.supplyAsync(() -> s + returnValueWithDealy(tax, 1000)));

        assertThat(doubleCompletableFuture.get()).isEqualTo(price + tax);
    }

    @Test
    public void completeAsync() throws ExecutionException, InterruptedException {
        final double price = 0.5;
        final double tax = 0.25;

        ExecutorService taskExecutor = Executors.newWorkStealingPool();

        final CompletableFuture<Double> doubleCompletableFuture =
                CompletableFuture.supplyAsync(() -> returnValueWithDealy(price, 1500))
                                 .thenComposeAsync(v -> {
                                     log.info("Async compose section 1");
                                     returnValueWithDealy(1, 1000);
                                     return CompletableFuture.completedFuture(v);
                                 }, taskExecutor)
                                 .thenCompose(v -> {
                                     log.info("Async compose section 2");
                                     returnValueWithDealy(2, 1000);
                                     return CompletableFuture.completedFuture(v);
                                 })
                                 .thenComposeAsync(v -> {
                                     log.info("Async compose section 3");
                                     returnValueWithDealy(3, 1000);
                                     return CompletableFuture.completedFuture(v);
                                 }, taskExecutor)
                                 .whenComplete((v, ex) -> log.info("On complete section"))
                                 .thenApply(v -> {
                                     log.info("Last apply section");
                                     return v;
                                 });
        assertThat(doubleCompletableFuture.get()).isEqualTo(price);
    }

    @Test
    public void some() {
        final StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        log.info("Start calculations: {}", stopwatch);
        returnValueWithDealy(1.0, 1000);
        stopwatch.split();
        log.info("Finish calculations: {}", stopwatch.getTime());
        stopwatch.split();
        returnValueWithDealy(1.0, 1500);
        stopwatch.suspend();
        log.info("Finish calculations: {}", stopwatch.getTime());
    }

    @Test
    public void runWithExecutorServiceInParallel() {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        final List<CompletableFuture<Double>> futures = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            final Double value = Double.valueOf(i);
            futures.add(CompletableFuture.supplyAsync(() -> returnValueWithDealy(value, 1000), pool));
        }
//        completeAllFutureAndThenHandle(futures);
        completeAllFutureFailOnFirst(futures);
    }

    private void completeAllFutureAndThenHandle(final List<CompletableFuture<Double>> futures) {
        final Boolean failed = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                                                .handle((r, e) -> e != null ? Boolean.TRUE : Boolean.FALSE)
                                                .join();
        if (failed) {
            log.error("PROBLEM!");
        } else {
            log.info("NP! All good :)");
        }
    }

    private void completeAllFutureFailOnFirst(final List<CompletableFuture<Double>> futures) {
        final CompletableFuture<Void> all = allOfOrException(futures);
        all.join();
        if (all.isCompletedExceptionally()) {
            log.error("PROBLEM!");
        } else {
            log.info("NP! All good :)");
        }
    }

    public static <T> CompletableFuture<Void> allOfOrException(List<CompletableFuture<T>> futures) {
        CompletableFuture<List<T>> result = allOf(futures);

        for (CompletableFuture<?> f : futures) {
            f.handle((__, ex) -> ex == null || result.completeExceptionally(ex));
        }

        return result.thenAccept(result::complete);
    }

    public static <T> CompletableFuture<List<T>> allOf(List<CompletableFuture<T>> futures) {
        return CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                                .thenApply(__ -> futures.stream().map(CompletableFuture::join).collect(toList()));
    }

    private double returnValueWithDealy(final double value, final int sleepTime) {
        final String uuid = UUID.randomUUID().toString();
        log.info("[{}]: Starter return value in {}", uuid, LocalTime.now());
        if (value == 4.0) {
            throw new RuntimeException();
        }
        try {
            Thread.sleep(sleepTime);
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
        log.info("[{}]: Finished return value in {}", uuid, LocalTime.now());
        log.warn("Return for value [{}].", value);
        return value;
    }
}
