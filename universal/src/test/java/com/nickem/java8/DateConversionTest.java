package com.nickem.java8;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.google.common.collect.ImmutableSet;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@Slf4j
public class DateConversionTest {

    private static final Set<DateTimeFormatter> SUPPORTED_FORMATS = ImmutableSet.of(
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"),
        DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"),
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"),
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS"),
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
        DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")
    );

    public Date convertDate(String date) {
        Date result = null;
        for (DateTimeFormatter format : SUPPORTED_FORMATS) {
            try {
                result = Date.from(LocalDateTime.parse(date, format).toInstant(ZoneOffset.UTC));
                break;
            } catch (DateTimeParseException ex) {
                log.warn("Unable to parse [{}] with format [{}]", date, format);
            }
        }

        if (result == null) {
            throw new IllegalArgumentException(String.format("Exception while parsing date from [%s]", date));
        }
        return result;
    }


    private static Stream<Arguments> transactionInputProvider() {
        return Stream.of(
            Arguments.of("2014-01-12 14:36:39", 2014, 1, 12, 14, 36, 39),
            Arguments.of("2014-01-12 23:26:08", 2014, 1, 12, 23, 26, 8),
            Arguments.of("2014-01-13 01:38:56", 2014, 1, 13, 1, 38, 56)
        );
    }

    @ParameterizedTest
    @MethodSource("transactionInputProvider")
    void convertDate(String dateStr, int year, int month, int day, int hour, int minute, int second) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Date date = convertDate(dateStr);
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(year);
        /*Weird that month counts from 0 instead of 1*/
        assertThat(calendar.get(Calendar.MONTH)).isEqualTo(month - 1);
        assertThat(calendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(day);

        assertThat(calendar.get(Calendar.HOUR_OF_DAY)).isEqualTo(hour);
        assertThat(calendar.get(Calendar.MINUTE)).isEqualTo(minute);
        assertThat(calendar.get(Calendar.SECOND)).isEqualTo(second);
    }


    @Test
    public void offsetToEpochAndBack() {
        final OffsetDateTime previousOdt = Instant.now().atOffset(ZoneOffset.ofHours(3));
        log.debug("{}", previousOdt);
        final OffsetDateTime odtInUtc = previousOdt.withOffsetSameInstant(ZoneOffset.UTC);
        final long epochMilli = odtInUtc.toInstant().toEpochMilli();
        log.debug("{}", odtInUtc);

        final OffsetDateTime newOdt = Instant.ofEpochMilli(epochMilli).atOffset(ZoneOffset.UTC);
        log.debug("{}", newOdt);
    }

    @Test
    public void dateToLocalDateTime() {
        final Date date = new Date();
        log.debug("Date: {}", date);
        final Instant instant = Instant.ofEpochMilli(date.getTime());
        final LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        log.debug("LocalDateTime: {}", ldt);
    }

    @Test
    public void localDateTimeToDate() {
        final LocalDateTime ldt = LocalDateTime.now();
        log.debug("LocalDateTime: {}", ldt);
        final Instant instant = ldt.toInstant(ZoneOffset.UTC);

        final Date date = Date.from(instant);
        log.debug("Date: {}", date);
    }

    @Test
    public void today() {
        final var now = LocalDate.now();
        final var fromDate = LocalDate.ofInstant(new Date().toInstant(), ZoneId.systemDefault());
        log.info("{} from now is", now);
        log.info("{} from date is", fromDate);
        assertEquals(now, fromDate);
    }

    @Test
    public void getAvailableTimezones() {
        final SimpleDateFormat sdf = new SimpleDateFormat("zzz");
        final Date now = new Date();

        for (final String id : TimeZone.getAvailableIDs()) {
            sdf.setTimeZone(TimeZone.getTimeZone(id));
            log.info("{}={}}", id, sdf.format(now));
        }
    }

    @Test
    public void formatTest(){
        final String format = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss").withZone(ZoneOffset.UTC).format(Instant.now());
        log.info("{}", format);
    }

    @Test
    public void formatFromStringNanosecondAndCompareToMilliseconds(){
        var strDateNanos = "2023-05-30T11:30:08.804441";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
        LocalDateTime ldtNanos = LocalDateTime.parse(strDateNanos, formatter);
        final Date expected = Date.from(ldtNanos.atZone(ZoneOffset.UTC).toInstant());

        var strDateMillis = "2023-05-30-11-30-08-804";
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SSS")
            .withZone(ZoneOffset.UTC);

        LocalDateTime ldtMillis = LocalDateTime.parse(strDateMillis, DATE_TIME_FORMATTER);
        final Date actual = Date.from(ldtMillis.atZone(ZoneOffset.UTC).toInstant());

        assertThat(actual).isEqualTo(expected);
    }

    /*TODO Investigate if instance is better than LocalDateTime*/
    //        final Instant date = LocalDateTime.parse(fields[mapper.getIndexByFieldName(IncomeRecordMapper.date)], FORMATTER)
//            .atZone(ZoneId.of("UTC")).toInstant();

    @Test
    public void durationParse(){
        log.info("{}", Duration.parse("PT10S").getSeconds());
        log.info("{}", Duration.parse("PT5M").getSeconds());
        log.info("{}", Duration.parse("PT1H").getSeconds());
    }

    @Test
    public void timestampToDate(){
        long epoch = Long.parseLong("1694386800");
        Instant instant = Instant.ofEpochSecond(epoch);
        log.info("{}", Date.from(instant));
    }

    @Test
    public void localDateToInstant() {
        /*TODO*/
        final LocalDate begin = LocalDate.of(2023, 7, 1);
        final Instant beginInstant = begin.atStartOfDay().toInstant(ZoneOffset.UTC);
        final LocalDate end = LocalDate.of(2023, 8, 1);
        final Instant endInstant = end.atStartOfDay().toInstant(ZoneOffset.UTC);
        log.info("Begin {}", beginInstant.getEpochSecond());
        log.info("End {}", endInstant.getEpochSecond());
    }

    @Test
    public void dateToLocalDate() {
        final Date date = Date.from(Instant.now());
        LocalDate localDate = date.toInstant().atZone(ZoneOffset.UTC).toLocalDate();
        log.info("Now {}", localDate);
    }

}
