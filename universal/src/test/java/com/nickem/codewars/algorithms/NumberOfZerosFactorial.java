package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class NumberOfZerosFactorial {

    public static int zeros(int n) {
        int factorial = 1;
        int result = 0;
        for (int i = 1; i <= n; i++) {
            factorial = i * factorial;
            if (factorial > 10 && factorial % 10 == 0) {
                result++;
                factorial = factorial / 10;
            }
        }
        return result;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(0, 0),
            Arguments.of(6, 1),
            Arguments.of(12, 2),
            Arguments.of(14, 2)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final int number, final int expected) {
        assertThat(zeros(number)).isEqualTo(expected);
    }

}
