package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* https://www.codewars.com/kata/54c9fcad28ec4c6e680011aa/train/java
* */
public class MergedStringChecker {

    public static boolean isMerge(final String input, String left, String right) {
        boolean result = false;

        if (input != null && left != null && right != null && sizeIsEqual(input, left, right)) {
            for (String ch : input.split("")) {
                if (left.length() != 0 && left.startsWith(ch)) {
                    left = left.substring(1);
                } else if (right.length() != 0 && right.startsWith(ch)) {
                    right = right.substring(1);
                } else {
                    break;
                }
            }

            if (left.length() == 0 && right.length() == 0) {
                result = true;
            }
        }

        return result;
    }

    private static boolean sizeIsEqual(final String input, String left, String right) {
        return input.length() == left.length() + right.length();
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of("codewars", "code", "wars"),
            Arguments.of("codewars", "cdw", "oears"),
            Arguments.of("codewars", "cdwr", "oeas"),
            Arguments.of("Going bananas!", "Going ", "bananas!"),
            Arguments.of("#qv\"8qB#Qq/@,HM4r", "qBQq/,M", "#qv\"8#@H4r")
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final String actual, final String left, final String right) {
        assertThat(isMerge(actual, left, right)).isTrue();
    }

}
