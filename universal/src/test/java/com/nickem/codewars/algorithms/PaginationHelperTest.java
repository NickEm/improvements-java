package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/515bb423de843ea99400000a
* */
class PaginationHelperTest {

    @Test
    public void test() {
        PaginationHelper<Character> helper = new PaginationHelper(Arrays.asList('a', 'b', 'c', 'd', 'e', 'f'), 4);
        assertThat(helper.pageCount()).isEqualTo(2); //should == 2
        assertThat(helper.itemCount()).isEqualTo(6); //should == 6
        assertThat(helper.pageItemCount(0)).isEqualTo(4); //should == 4
        assertThat(helper.pageItemCount(1)).isEqualTo(2); // last page - should == 2
        assertThat(helper.pageItemCount(2)).isEqualTo(-1); // should == -1 since the page is invalid

        // pageIndex takes an item index and returns the page that it belongs on
        assertThat(helper.pageIndex(5)).isEqualTo(1); //should == 1 (zero based index)
        assertThat(helper.pageIndex(2)).isEqualTo(0); //should == 0
        assertThat(helper.pageIndex(20)).isEqualTo(-1); //should == -1
        assertThat(helper.pageIndex(-10)).isEqualTo(-1); //should == -1
    }
}