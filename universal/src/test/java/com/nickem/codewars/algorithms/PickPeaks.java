package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PickPeaks {

    public static Map<String, List<Integer>> getPeaks(int[] arr) {
        Map<String, List<Integer>> result = new HashMap<>() {{
            put("pos", new ArrayList<>());
            put("peaks", new ArrayList<>());
        }};
        for (int i = 1; i < arr.length - 1; i++) {
            if (arr[i] > arr[i - 1] && arr[i] >= arr[i + 1]) {
                if (arr[i] == arr[i + 1] && doesntContainPlateau(arr, i + 1)) {
                    continue;
                }
                final int pos = i;
                final int peak = arr[pos];
                result.computeIfPresent("pos", (key, value) -> {
                    value.add(pos);
                    return value;
                });
                result.computeIfPresent("peaks", (key, value) -> {
                    value.add(peak);
                    return value;
                });
            }
        }
        return result;
    }

    private static boolean doesntContainPlateau(int[] arr, int i) {
        boolean result = true;
        for (int j = i; j < arr.length - 1; j++) {
            if (arr[i] < arr[j + 1]) {
                break;
            } else if (arr[i] > arr[j + 1]) {
                result = false;
            }
        }
        return result;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(new int[]{1, 2, 3, 6, 4, 1, 2, 3, 2, 1}, new int[]{3, 7}, new int[]{6, 3}),
            Arguments.of(new int[]{3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 3}, new int[]{3, 7}, new int[]{6, 3}),
            Arguments.of(new int[]{3, 2, 3, 6, 4, 1, 2, 3, 2, 1, 2, 2, 2, 1}, new int[]{3, 7, 10}, new int[]{6, 3, 2}),
            Arguments.of(new int[]{2, 1, 3, 1, 2, 2, 2, 2}, new int[]{2}, new int[]{3}),
            Arguments.of(new int[]{2, 1, 3, 1, 2, 2, 2, 2, 1}, new int[]{2, 4}, new int[]{3, 2})
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void sampleTests(int[] input, int[] positions, int[] peaks) {
        Map<String, List<Integer>> expected = new HashMap<>() {{
            put("pos", Arrays.stream(positions).boxed().collect(Collectors.toList()));
            put("peaks", Arrays.stream(peaks).boxed().collect(Collectors.toList()));
        }};

        Map<String, List<Integer>> actual = getPeaks(input);
        assertThat(actual).isEqualTo(expected);
    }

}
