package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class FindOutlierTest {

    private static int findOutlier(int[] integers) {
        int result = 0;
        int odd = 0;
        int even = 0;
        for (int i = 0; i < 3; i++) {
            if (Math.abs(integers[i] % 2) == 1) {
                odd++;
            } else {
                even++;
            }
        }
        boolean lookingForOdd = odd < even;
        for (int number : integers) {
            if (lookingForOdd && Math.abs(number % 2) == 1 || !lookingForOdd && number % 2 == 0) {
                result = number;
                break;
            }
        }

        return result;
    }

    @Test
    public void testExample() {
        assertThat(findOutlier(new int[]{2, 6, 8, -10, 3})).isEqualTo(3);
        assertThat(findOutlier(new int[]{206847684, 1056521, 7, 17, 1901, 21104421, 7, 1, 35521, 1, 7781})).isEqualTo(
                206847684);
        assertThat(findOutlier(new int[]{Integer.MAX_VALUE, 0, 1})).isEqualTo(0);
    }

}
