package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/55c6126177c9441a570000cc
* */
public class WeightForWeight {

//        return Arrays.stream(strng.split(" ")).sorted((s1,s2)->{
//            int sum1 = s1.chars().map(c -> Character.getNumericValue(c)).sum();
//            int sum2 = s2.chars().map(c -> Character.getNumericValue(c)).sum();
//            return sum1 == sum2 ? s1.compareTo(s2) : sum1-sum2;
//        }).collect(Collectors.joining(" "));
//  }

    public static String orderWeight(String weightResult) {
        final String split = "[\\[\\]|;# .]";
        final String delimiter = " ";
        return  Arrays.stream(weightResult.trim().split(split))
            .filter(e -> {
                try {
                    Long.valueOf(e);
                    return true;
                } catch (NumberFormatException ex) {
                    return false;
                }
            })
            .map(Long::valueOf).sorted(
                (o1, o2) -> {
                    Long left = findStrangeWeight(o1);
                    Long right = findStrangeWeight(o2);
                    if (left.equals(right)) {
                        return String.valueOf(o1).compareTo(String.valueOf(o2));
                    } else {
                        return left.compareTo(right);
                    }
                })
            .map(String::valueOf).collect(Collectors.joining(delimiter));
    }

    public static Long findStrangeWeight(Long weight) {
        long result = 0;
        while ((weight % 10) > 0 || (weight / 10) > 0) {
            result += weight % 10;
            weight = weight / 10;
        }
        return result;
    }

    @Test
    public void BasicTests() {
        assertThat(findStrangeWeight(200L)).isEqualTo(2);
        assertThat(findStrangeWeight(310L)).isEqualTo(4);
        assertThat(findStrangeWeight(99L)).isEqualTo(18);
        assertEquals("2000 103 123 4444 99", orderWeight("103 123 4444 99 2000"));
        assertEquals("2000 103 123 4444 99", orderWeight("103 123 99 4444 2000"));
        assertEquals("11 11 2000 10003 22 123 1234000 44444444 9999", orderWeight("2000 10003 1234000 44444444 9999 11 11 22 123"));
        assertEquals("1234 1131268 49455 347464 59544965313 496636983114762 85246814996697", orderWeight("...1131268 49455 347464[ 1234 59544965313 496636983114762 85246814996697]"));
    }
}
