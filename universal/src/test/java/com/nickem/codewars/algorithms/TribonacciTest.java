package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/556deca17c58da83c00002db
*/
public class TribonacciTest {

    public double[] tribonacciRecursive(double[] src, int n) {
        if (n == 0) {
            return new double[]{};
        } else if (n > 0 && n < 4) {
            final double[] dest = new double[n];
            System.arraycopy(src, 0, dest, 0, src.length);
            return dest;
        }

        if (src.length == n) {
            return src;
        } else {
            final double[] dest = new double[src.length + 1];
            System.arraycopy(src, 0, dest, 0, src.length);
            dest[src.length] = src[src.length - 1] + src[src.length - 2] + src[src.length - 3];
            return tribonacci(dest, n);
        }
    }

    public double[] tribonacci(double[] src, int n) {
        double[] tritab = Arrays.copyOf(src, n);
        for (int i = 3; i < n; i++) {
            tritab[i] = tritab[i - 1] + tritab[i - 2] + tritab[i - 3];
        }
        return tritab;
    }

    @Test
    public void sampleTests() {
        assertThat(tribonacci(new double[]{1, 1, 1}, 10)).isEqualTo(new double[]{1, 1, 1, 3, 5, 9, 17, 31, 57, 105});
        assertThat(tribonacci(new double[]{0, 0, 1}, 10)).isEqualTo(new double[]{0, 0, 1, 1, 2, 4, 7, 13, 24, 44});
        assertThat(tribonacci(new double[]{0, 1, 1}, 10)).isEqualTo(new double[]{0, 1, 1, 2, 4, 7, 13, 24, 44, 81});
        assertThat(tribonacci(new double[]{2.0, 9.0, 19.0}, 3)).isEqualTo(new double[]{2.0, 9.0, 19.0});
    }

}
