package com.nickem.codewars.algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


/*
* You are given an array of integers where the items correspond to stock prices and the indexes of the array
* correspond to sequential days. Write a function getMaxProfit that calculates the maximum amount you can earn by
* buying once and selling once. The worst you can do is a profit of $0 (you buy/sell even or don’t buy/sell at all).
* You can only sell a stock on a day after you bought it.
* You can’t sell before you buy.
* You can’t sell on the same day that you buy.
* */
@Slf4j
public class BestTimeToSellStock {

    @Test
    public void test() {
        assertEquals(5, maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
        assertEquals(5, maxProfit(new int[]{6, 2, 3, 4, 5, 7, 1, 5}));
        assertEquals(0, maxProfit(new int[]{7, 6, 4, 3, 1}));
    }

    public int maxProfit(int[] prices) {
        int max = 0;
        for (int i = 0; i < prices.length - 1; i++) {
            int currentMax = findMaxProfitForRange(i, prices);
            if (currentMax > max) {
                max = currentMax;
            }
        }
        return max;
    }

    private int findMaxProfitForRange(final int index, final int[] prices) {
        log.info("Input {}", prices);
        int maxProfit = 0;
        for (int i = index + 1; i < prices.length; i++) {
            final var currentProfit = prices[i] - prices[index];
            if (currentProfit > maxProfit) {
                maxProfit = currentProfit;
            }
        }

        return maxProfit;
    }

}
