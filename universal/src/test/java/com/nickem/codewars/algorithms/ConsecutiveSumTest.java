package com.nickem.codewars.algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ConsecutiveSumTest {

    @Test
    public void consecutiveSum() {
        assertEquals(1, ConsecutiveSum.consecutiveSum(1));
        assertEquals(4, ConsecutiveSum.consecutiveSum(15));
        assertEquals(2, ConsecutiveSum.consecutiveSum(48));
        assertEquals(2, ConsecutiveSum.consecutiveSum(97));
    }
}