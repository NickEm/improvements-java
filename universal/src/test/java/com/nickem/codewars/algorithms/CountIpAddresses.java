package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* https://www.codewars.com/kata/526989a41034285187000de4
* */
public class CountIpAddresses {

    public static long ipsBetween(String start, String end) {
        return convertToLong(end) - convertToLong(start);
    }

    private static long convertToLong(String ip) {
        long res = 0;
        for (String s : ip.split("\\.")) {
            res = res * 256 + Long.parseLong(s);
        }
        return res;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(50, "10.0.0.0", "10.0.0.50"),
            Arguments.of(246, "20.0.0.10", "20.0.1.0"),
            Arguments.of(256, "10.0.0.0", "10.0.1.0")
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final long number, String start, String end) {
        assertThat(number).isEqualTo(ipsBetween(start, end));
    }

}
