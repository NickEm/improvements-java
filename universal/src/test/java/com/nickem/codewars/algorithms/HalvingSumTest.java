package com.nickem.codewars.algorithms;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class HalvingSumTest {

    @Test
    public void halvingSum() {
        assertEquals(47, HalvingSum.halvingSum(25));
        assertEquals(247, HalvingSum.halvingSum(127));
    }
}