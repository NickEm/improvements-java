package com.nickem.codewars.algorithms.cache;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public abstract class AbstractLruCacheTest {

    @Test
    public void testDefaultInsertionOrder() {
        final Map<String, Integer> map = new LinkedHashMap<>();
        map.put("one", 1);
        map.put("three", 3);
        map.put("two", 2);

        assertThat(map).containsExactlyEntriesOf(ImmutableMap.of(
            "one", 1,
            "three", 3,
            "two", 2
        ));
    }

    @Test
    public void testAccessOrder() {
        final Map<String, Integer> map = new LinkedHashMap<>(16, 0.75f, true);
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        map.get("two");

        assertThat(map).containsExactlyEntriesOf(ImmutableMap.of(
            "one", 1,
            "three", 3,
            "two", 2
        ));
    }

    @Test
    public void testWithCapacityDefaultInsertionOrder() {
        final Map<String, Integer> map = newSizeAwareLinkedHashMap(3);
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        map.put("four", 4);

        assertThat(map).containsExactlyEntriesOf(ImmutableMap.of(
            "two", 2,
            "three", 3,
            "four", 4
        ));
    }

    @Test
    public void testWithCapacityAccessOrder() {
        final LruCache<String, Integer> cache = getCache(3);
        cache.put("one", 1);
        cache.put("two", 2);
        cache.put("three", 3);
        cache.getElements();
        cache.get("one");
        cache.put("four", 4);

        assertThat(cache.getElements()).containsExactlyEntriesOf(ImmutableMap.of(
            "three", 3,
            "one", 1,
            "four", 4
        ));
    }

    private Map<String, Integer> newSizeAwareLinkedHashMap(int capacity) {
        return newSizeAwareLinkedHashMap(capacity, false);
    }

    private Map<String, Integer> newSizeAwareLinkedHashMap(int capacity, boolean accessOrder) {
        return new LinkedHashMap<>(16, 0.75f, accessOrder) {

            @Override
            protected boolean removeEldestEntry(Map.Entry<String, Integer> eldest) {
                return this.size() > capacity;
            }
        };
    }

    @Test
    public void comprehensive() {
        LruCache<String, Integer> store = getCache(3);
        store.put("a", 1);
        assertThat(store.size()).isEqualTo(1);

        assertThat(store.getCapacity()).isEqualTo(3);
        assertThat(store.get("a")).isEqualTo(1);
        assertThat(store.put("b", 2)).isEqualTo(null);
        assertThat(store.put("a", 5)).isEqualTo(1);
        assertThat(store.get("a")).isEqualTo(5);
        assertThat(store.size()).isEqualTo(2);

        store.put("c", 3);
        store.put("d", 4);
        assertThat(store.get("b")).isEqualTo(null);
        assertThat(store.get("c")).isEqualTo(3);
        assertThat(store.get("d")).isEqualTo(4);
        assertThat(store.get("a")).isEqualTo(5);
        assertThat(store.size()).isEqualTo(3);
        store.delete("delete");
        assertThat(store.size()).isEqualTo(3);
        store.delete("d");
        assertThat(store.size()).isEqualTo(2);

        store.put("c", 4);
        assertThat(store.get("c")).isEqualTo(4);

        assertThat(store.getElements()).containsExactlyEntriesOf(ImmutableMap.of(
            "a", 5,
            "c", 4
        ));
    }

    @Test
    public void runMultiThreadTask_WhenPutDataInConcurrentToCache_ThenNoDataLost() throws Exception {
        final int size = 50;
        final ExecutorService executorService = Executors.newFixedThreadPool(5);
        LruCache<String, Integer> cache = getCache(size);
        CountDownLatch countDownLatch = new CountDownLatch(size);
        try {
            IntStream.range(0, size).<Runnable>mapToObj(key -> () -> {
                cache.put("value-" + key, key);
                countDownLatch.countDown();
            }).forEach(executorService::submit);
            countDownLatch.await();
        } finally {
            executorService.shutdown();
        }
        assertThat(cache.size()).isEqualTo(size);
        IntStream.range(0, size).forEach(i -> assertThat(i).isEqualTo(cache.get("value-" + i)));
    }

    protected abstract LruCache<String, Integer> getCache(int size);

}