package com.nickem.codewars.algorithms;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/541c8630095125aba6000c00/java
*
* Digital root is the recursive sum of all the digits in a number.
* Given n, take the sum of the digits of n. If that value has more than one digit,
* continue reducing in this way until a single-digit number is produced.
* The input will be a non-negative integer.
*
*/
public class DigitalRootTest {

    public static int digitalRoot(int n) {
        var numbers = String.valueOf(n).split("");
        final var sum = Stream.of(numbers).mapToInt(Integer::valueOf).sum();
        return sum / 9 == 0 ? sum : digitalRoot(sum);
    }

    @Test
    public void test1() {
        assertThat(digitalRoot(16)).isEqualTo(7);
        assertThat(digitalRoot(942)).isEqualTo(6);
        assertThat(digitalRoot(456)).isEqualTo(6);
        assertThat(digitalRoot(132189)).isEqualTo(6);
        assertThat(digitalRoot(493193)).isEqualTo(2);
    }
}
