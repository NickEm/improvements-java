package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* In our family known as dices
* */
@Slf4j
public class Dices {

    public int getRandom() {
        final Random random = new Random();
        return random.nextInt(1, 6);
    }

    private int[] roll(final int size) {
        int[] dice = new int[size];
        for (int j = 0; j < size; j++) {
            dice[j] = getRandom();

        }
        return dice;
    }

    public int score(int[] dice) {
        return score(dice, 0, true);
    }

    public int score(int[] dice, int score, boolean singleThrow) {
        int currentScore = 0;
        final Map<Integer, Integer> scores = new HashMap<>();
        for (int item : dice) {
            scores.compute(item, (key, value) -> value == null ? 1 : value + 1);
        }
        for (int die = 1; die <= 6; die++) {
            final Integer sameDiceCount = scores.get(die);
            if (sameDiceCount != null) {
                final int triplets = sameDiceCount / 3;
                if (triplets >= 1) {
                    currentScore += scoreForTriple(die);
                    currentScore = handleTripleRest(currentScore, die, sameDiceCount - 3);
                    scores.remove(die);
                } else {
                    currentScore = handleLessThanTriplet(currentScore, die, sameDiceCount, scores);
                }
            }
        }

        if (singleThrow) {
            return currentScore;
        } else if (currentScore != 0) {
            int diceCountToRoll = scores.size() > 0 ? scores.size() : 6;
            return score(roll(diceCountToRoll), score + currentScore, false);
        } else {
            return score;
        }
    }

    private int handleTripleRest(int score, int die, Integer sameDiceCount) {
        for (int j = 1; j <= sameDiceCount; j++) {
            score += scoreForTriple(die);
        }
        return score;
    }

    private int handleLessThanTriplet(int score, int die, Integer sameDiceCount, final Map<Integer, Integer> scores) {
        if (sameDiceCount >= 1) {
            if (die == 5) {
                score += sameDiceCount * 5;
                scores.remove(die);
            } else if (die == 1) {
                score += sameDiceCount * 10;
                scores.remove(die);
            }
        }
        return score;
    }

    public int scoreForTriple(Integer value) {
        switch (value) {
            case 1 -> {
                return 100;
            }
            case 6 -> {
                return 60;
            }
            case 5 -> {
                return 50;
            }
            case 4 -> {
                return 40;
            }
            case 3 -> {
                return 30;
            }
            case 2 -> {
                return 20;
            }
            default -> {
                return 0;
            }
        }
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(new int[]{1,2,2,3,3,4}, 10),
            Arguments.of(new int[]{1,1,2,2,3,4}, 20),
            Arguments.of(new int[]{1,1,1,2,3,4}, 100),
            Arguments.of(new int[]{1,1,1,1,2,3}, 200),
            Arguments.of(new int[]{1,1,1,1,1,3}, 300),
            Arguments.of(new int[]{1,1,1,1,1,1}, 400),

            Arguments.of(new int[]{2,2,2,3,3,4}, 20),
            Arguments.of(new int[]{2,2,2,2,3,4}, 40),
            Arguments.of(new int[]{2,2,2,2,2,4}, 60),
            Arguments.of(new int[]{2,2,2,2,2,2}, 80),

            Arguments.of(new int[]{3,3,3,1,1,1}, 130),
            Arguments.of(new int[]{3,3,3,2,2,2}, 50),

            Arguments.of(new int[]{3,3,3,2,2,4}, 30),
            Arguments.of(new int[]{3,3,3,3,2,2}, 60),
            Arguments.of(new int[]{3,3,3,3,3,2}, 90),
            Arguments.of(new int[]{3,3,3,3,3,3}, 120),

            Arguments.of(new int[]{4,4,4,1,1,1}, 140),
            Arguments.of(new int[]{4,4,4,2,2,2}, 60),
            Arguments.of(new int[]{4,4,4,3,3,3}, 70),

            Arguments.of(new int[]{4,4,4,2,2,3}, 40),
            Arguments.of(new int[]{4,4,4,4,2,2}, 80),
            Arguments.of(new int[]{4,4,4,4,4,2}, 120),
            Arguments.of(new int[]{4,4,4,4,4,4}, 160),

            Arguments.of(new int[]{5,5,5,1,1,1}, 150),
            Arguments.of(new int[]{5,5,5,1,2,2}, 60),
            Arguments.of(new int[]{5,5,5,1,1,2}, 70),
            Arguments.of(new int[]{5,5,5,2,2,2}, 70),
            Arguments.of(new int[]{5,5,5,3,3,3}, 80),
            Arguments.of(new int[]{5,5,5,4,4,4}, 90),
            Arguments.of(new int[]{5,5,5,6,6,6}, 110),

            Arguments.of(new int[]{5,5,5,2,2,3}, 50),
            Arguments.of(new int[]{5,5,5,5,2,2}, 100),
            Arguments.of(new int[]{5,5,5,5,5,2}, 150),
            Arguments.of(new int[]{5,5,5,5,5,5}, 200),

            Arguments.of(new int[]{6,6,6,2,2,3}, 60),
            Arguments.of(new int[]{6,6,6,2,2,2}, 80),
            Arguments.of(new int[]{6,6,6,6,2,2}, 120),
            Arguments.of(new int[]{6,6,6,6,6,2}, 180),
            Arguments.of(new int[]{6,6,6,6,6,6}, 240),

            Arguments.of(new int[]{1,1,1,2,3,4}, 100),
            Arguments.of(new int[]{1,1,1,2,3,4}, 100),
            Arguments.of(new int[]{1,1,1,2,3,4}, 100),
            Arguments.of(new int[]{5,1,3,4,1,5}, 30),
            Arguments.of(new int[]{2,4,4,5,4,6}, 45),
            Arguments.of(new int[]{3,3,4,4,5,5}, 10),
            Arguments.of(new int[]{3,3,4,4,6,6}, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void testExample(int[] input, int expected) {
        final String message = String.format("Score for %s must be %s",
            Arrays.stream(input).boxed().collect(Collectors.toList()), expected);
        assertThat(score(input)).as(message).isEqualTo(expected);
    }

    /*
    * Execution time
    *
    * 100m - 46 sec
    *
    * 0 -> 1.0%
    * 5 -> 3.0%
    * 10 -> 9.0%
    * 15 -> 10.0%
    * 20 -> 15.0%
    * 25 -> 11.0%
    * 30 -> 8.0%
    * 35 -> 3.0%
    * 40 -> 5.0%
    * 45 -> 4.0%
    * 50 -> 7.0%
    * 55 -> 2.0%
    * 60 -> 5.0%
    * */
    @Test
    public void testPossibility() {
        final Map<Integer, Integer> scores = new HashMap<>();
        final int runs = 100_000_000;
        for (int i = 0; i < runs; i++) {
            int[] dice = roll(6);
            int score = score(dice);
            scores.compute(score, (key, value) -> value == null ? 1 : value + 1);
        }

        scores.entrySet().stream().sorted(Entry.comparingByKey()).forEach((entry) -> {
            log.info("For score [{}] there were [{}%] occurrences", entry.getKey(), Precision.round((double) entry.getValue() / runs, 2) * 100);
        });
    }

    /*
     * Execution time:
     * 10m - 13 sec
     * 100m - 2 min
     *
     * 0 -> 1.0%
     * 5 -> 1.0%
     * 10 -> 2.0%
     * 15 -> 4.0%
     * 20 -> 7.0%
     * 25 -> 8.0%
     * 30 -> 9.0%
     * 35 -> 5.0%
     * 40 -> 4.0%
     * 45 -> 3.0%
     * 50 -> 4.0%
     *
     * */
    @Test
    public void testPossibilityMultipleRolls() {
        final Map<Integer, Integer> scores = new HashMap<>();
        final int runs = 100_000_000;
        for (int i = 0; i < runs; i++) {
            int[] dice = roll(6);
            int score = score(dice, 0, false);
            scores.compute(score, (key, value) -> value == null ? 1 : value + 1);
        }

        scores.entrySet().stream().sorted(Entry.comparingByKey()).forEach((entry) -> {
            log.info("For score [{}] there were [{}%] occurrences", entry.getKey(), Precision.round((double) entry.getValue() / runs, 2) * 100);
        });
    }

}
