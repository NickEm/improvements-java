package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

/*
 * https://www.codewars.com/kata/55c04b4cc56a697bb0000048
 * */
public class Scramblies {

    public static boolean scramble(String characters, String word) {
        boolean result = true;
        final List<CharSequence> charSequences = new ArrayList<>(Arrays.asList(characters.split("")));

        for (CharSequence s : word.split("")) {
            if (charSequences.contains(s)) {
                charSequences.remove(s);
            } else {
                result = false;
            }
        }

        return result;
    }

    @Test
    public void test() {
        assertThat(Scramblies.scramble("scriptjavx", "javascript")).isEqualTo(false);
        assertThat(Scramblies.scramble("scriptsjava", "javascripts")).isEqualTo(true);
        assertThat(Scramblies.scramble("rkqodlw","world")).isEqualTo(true);
        assertThat(Scramblies.scramble("cedewaraaossoqqyt","codewars")).isEqualTo(true);
        assertThat(Scramblies.scramble("katas","steak")).isEqualTo(false);
        assertThat(Scramblies.scramble("scriptingjava","javascript")).isEqualTo(true);

        assertThat(Scramblies.scramble("javscripts","javascript")).isEqualTo(false);
        assertThat(Scramblies.scramble("aabbcamaomsccdd","commas")).isEqualTo(true);
        assertThat(Scramblies.scramble("commas","commas")).isEqualTo(true);
        assertThat(Scramblies.scramble("sammoc","commas")).isEqualTo(true);
    }
}
