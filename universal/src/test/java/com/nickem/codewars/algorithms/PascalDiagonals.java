package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* https://www.codewars.com/kata/576b072359b1161a7b000a17
* */
public class PascalDiagonals {

    public static long[] generateRow(int n, int l) {
        Map<Integer, List<Long>> state = new HashMap<>();
        final long[] row = generateRow(n, state);
        final long[] result = new long[l];
        System.arraycopy(row, 0, result, 0, l);
        return result;
    }

    public static long[] generateDiagonal(int n, int l) {
        Map<Integer, List<Long>> state = new HashMap<>();
        generateRow(n + l, state);
        final long[] result = new long[l];
        for (int i = 0; i < l; i++) {
            result[i] = state.get(n + i).get(i);
        }
        return result;
    }

    private static long[] generateRow(int n, Map<Integer, List<Long>> state) {
        if (n == 0) {
            final long[] result = {1};
            state.put(n, Arrays.stream(result).boxed().collect(Collectors.toList()));
            return result;
        } else {
            final long[] previous = generateRow(n - 1, state);
            final long[] current = new long[previous.length + 1];
            current[0] = 1;
            for (int i = 1; i < previous.length; i++) {
                current[i] = previous[i] + previous[i - 1];
            }
            current[previous.length] = 1;
            state.put(n, Arrays.stream(current).boxed().collect(Collectors.toList()));
            return current;
        }
    }

    private static Stream<Arguments> assertionProviderRows() {
        return Stream.of(
            Arguments.of(0, 0, Collections.emptyList()),
            Arguments.of(1, 0, Collections.emptyList()),
            Arguments.of(0, 1, ImmutableList.of(1)),
            Arguments.of(1, 2, ImmutableList.of(1, 1)),
            Arguments.of(4, 5, ImmutableList.of(1, 4, 6, 4, 1)),
            Arguments.of(6, 4, ImmutableList.of(1, 6, 15, 20))
        );
    }


    @ParameterizedTest
    @MethodSource("assertionProviderRows")
    public void testRows(int n, int l, List<Integer> expected) {
        assertThat(generateRow(n, l)).isEqualTo(expected.stream().mapToLong(Long::valueOf).toArray());
    }

    private static Stream<Arguments> assertionProviderDiagonals() {
        return Stream.of(
            Arguments.of(0, 0, Collections.emptyList()),
            Arguments.of(1, 0, Collections.emptyList()),
            Arguments.of(0, 1, ImmutableList.of(1)),
            Arguments.of(1, 2, ImmutableList.of(1, 2)),
            Arguments.of(2, 6, ImmutableList.of(1, 3, 6, 10, 15, 21)),
            Arguments.of(6, 3, ImmutableList.of(1, 7, 28))
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProviderDiagonals")
    public void testDiagonals(int n, int l, List<Integer> expected) {
        assertThat(generateDiagonal(n, l)).isEqualTo(expected.stream().mapToLong(Long::valueOf).toArray());
    }

}
