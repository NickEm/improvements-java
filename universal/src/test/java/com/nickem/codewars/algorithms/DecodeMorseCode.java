package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/54b724efac3d5402db00065
*/
public class DecodeMorseCode {

    private static final Map<String, String> CHARACTER_TO_CODE = new HashMap<>();
    private static final Map<String, String> CODE_TO_CHARACTER = new HashMap<>();

    static {
        try (InputStream props = DecodeMorseCode.class.getClassLoader().getResourceAsStream("morse-code.properties")) {
            Properties properties = new Properties();
            properties.load(props);
            for (String key : properties.stringPropertyNames()) {
                CHARACTER_TO_CODE.put(key, properties.getProperty(key));
                CODE_TO_CHARACTER.put(properties.getProperty(key), key);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String decode(final String morseCode) {
        final var words = morseCode.trim().split("   ");
        final var builder = new StringBuilder();
        for (String word : words) {
            final var letters = word.split(" ");
            for (String letter : letters) {
                builder.append(CODE_TO_CHARACTER.get(letter).toUpperCase());
            }
            builder.append(" ");
        }
        return builder.toString().trim();
    }

    @Test
    public void test() {
        assertThat(decode(".... . -.--   .--- ..- -.. .")).isEqualTo("HEY JUDE");
    }
}
