package com.nickem.codewars.algorithms;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/57591ef494aba64d14000526
* */
public class JohnAndAnnSignUp {

    public static long annTasksForDay(long n, long[] annValues, long[] johnValues) {
        if (n <= 0) {
            annValues[0] = 1;
            return 1;
        } else {
            final long result = n - johnTasksForDay(annTasksForDay(n - 1, annValues, johnValues), annValues, johnValues);
            annValues[(int) n] = result;
            return result;
        }
    }

    public static long johnTasksForDay(long n, long[] annValues, long[] johnValues) {
        if (n <= 0) {
            johnValues[0] = 0;
            return 0;
        } else {
            final long result = n - annTasksForDay(johnTasksForDay(n-1, annValues, johnValues), annValues, johnValues);
            johnValues[(int) n] = result;
            return result;
        }
    }

    public static List<Long> ann(long n) {
        final long[] annValues = new long[(int) n + 1];
        final long[] johnValues = new long[(int) n + 1];

        annTasksForDay(n, annValues, johnValues);

        return Arrays.stream(annValues).boxed().limit(n).collect(Collectors.toList());
    }

    public static List<Long> john(long n) {
        final long[] annValues = new long[(int) n + 1];
        final long[] johnValues = new long[(int) n + 1];

        johnTasksForDay(n, annValues, johnValues);

        return Arrays.stream(johnValues).boxed().limit(n).collect(Collectors.toList());
    }

    public static long sumAnn(long n) {
        return ann(n).stream().mapToLong(Long::longValue).sum();
    }

    public static long sumJohn(long n) {
        return john(n).stream().mapToLong(Long::longValue).sum();
    }


    @Test
    public void test2() {
        testAnn(6, "[1, 1, 2, 2, 3, 3]");
        testAnn(3, "[1, 1, 2]");
    }

    private static void testJohn(long n, String res) {
        assertEquals(res, Arrays.toString(john(n).toArray()));
    }
    @Test
    public void test1() {
        testJohn(11, "[0, 0, 1, 2, 2, 3, 4, 4, 5, 6, 6]");
    }

    private static void testAnn(long n, String res) {
        assertEquals(res, Arrays.toString(ann(n).toArray()));
    }

    private static void testSumAnn(long n, long res) {
        assertEquals(res, sumAnn(n));
    }
    @Test
    public void test3() {
        testSumAnn(115, 4070);
    }

    private static void testSumJohn(long n, long res) {
        assertEquals(res, sumJohn(n));
    }
    @Test
    public void test4() {
        testSumJohn(75, 1720);
    }
}
