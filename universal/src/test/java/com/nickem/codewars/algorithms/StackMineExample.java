package com.nickem.codewars.algorithms;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;

/*
* TODO
* https://www.codewars.com/kata/5a62173ee626c5f0e40000e9
*/
public class StackMineExample {

    private static Map<String, String> OPEN_TO_CLOSED_BRACES = ImmutableMap.of("(", ")", "[", "]", "<", ">", "{", "}");
    private static List<String> POSSIBLE_BRACES = ImmutableList.of("(", ")", "[", "]", "<", ">", "{", "}");

    public Triple<Boolean, Integer, Integer> isBalanced(final String braces) {
        final Stack<String> stack = new Stack<>();
        int balancedBraces = 0;
        for (final String letter : braces.split("")) {
            if (POSSIBLE_BRACES.contains(letter)) {
                if (stack.empty()) {
                    stack.push(letter);
                } else {
                    if (letter.equals(OPEN_TO_CLOSED_BRACES.get(stack.peek()))) {
                        stack.pop();
                        balancedBraces++;
                    } else {
                        stack.push(letter);
                    }
                }
            }
        }

        return Triple.of(stack.empty(), balancedBraces, stack.size());
    }
    
    @Test
    public void testExample() {
        assertThat(isBalanced("(string[5])")).isEqualTo(Triple.of(true, 2, 0));
        assertThat(isBalanced("(string[)5]")).isEqualTo(Triple.of(false, 0, 4));
        assertThat(isBalanced("")).isEqualTo(Triple.of(true, 0, 0));
        assertThat(isBalanced("(([()]))")).isEqualTo(Triple.of(true, 4, 0));
        assertThat(isBalanced("({)}")).isEqualTo(Triple.of(false, 0, 4));
    }


}
