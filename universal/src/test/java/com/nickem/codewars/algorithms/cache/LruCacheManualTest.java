package com.nickem.codewars.algorithms.cache;

public class LruCacheManualTest extends AbstractLruCacheTest {

    @Override
    protected LruCache<String, Integer> getCache(int size) {
        return new LruCacheManual<>(size);
    }
}
