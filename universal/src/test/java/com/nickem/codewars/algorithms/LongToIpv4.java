package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* https://www.codewars.com/kata/52e88b39ffb6ac53a400022e
* */
public class LongToIpv4 {

    public static String longToIP(long ip) {
        long part4 = ip % 256;
        long part3 = (ip / 256) % 256;
        long part2 = (ip / 256) / 256 % 256;
        long part1 = (ip / 256) / 256 / 256 % 256;
        return String.join(".", String.valueOf(part1), String.valueOf(part2), String.valueOf(part3), String.valueOf(part4));
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(2154959208L, "128.114.17.104"),
            Arguments.of(0, "0.0.0.0"),
            Arguments.of(2149583361L, "128.32.10.1")
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final long ipAsLong, String expectedIp) {
        assertThat(longToIP(ipAsLong)).isEqualTo(expectedIp);
    }
}
