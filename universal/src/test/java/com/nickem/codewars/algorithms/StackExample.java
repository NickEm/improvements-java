package com.nickem.codewars.algorithms;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/5277c8a221e209d3f6000b56
* Дано рядок в якому є круглі, квадратні і кутові дужки (відкриваючі та закриваючі).
* Перевірити що кожній круглій, квадратній, кутовій відкриваючій дужці відповідає аналогічна кругла, квадратна, кутова
* закриваюча дужка.
* Приклад: ( []) - правильно; ( ( < [] () > )) - правильно; [(] ) - не правильно.
*/
public class StackExample {

    private static Map<String, String> OPEN_TO_CLOSED_BRACES = new HashMap<>();
    private static List<String> POSSIBLE_BRACES = new ArrayList<>();

    private static final String OPEN_BRACKETS = "(";
    private static final String CLOSE_BRACKETS = ")";
    private static final String SQUARE_OPEN_BRACKETS = "[";
    private static final String SQUARE_CLOSE_BRACKETS = "]";
    private static final String CORNER_OPEN_BRACKETS = "<";
    private static final String CORNER_CLOSE_BRACKETS = ">";

    static {
        POSSIBLE_BRACES.add(OPEN_BRACKETS);
        POSSIBLE_BRACES.add(CLOSE_BRACKETS);
        POSSIBLE_BRACES.add(SQUARE_OPEN_BRACKETS);
        POSSIBLE_BRACES.add(SQUARE_CLOSE_BRACKETS);
        POSSIBLE_BRACES.add(CORNER_OPEN_BRACKETS);
        POSSIBLE_BRACES.add(CORNER_CLOSE_BRACKETS);
        OPEN_TO_CLOSED_BRACES.put(OPEN_BRACKETS, CLOSE_BRACKETS);
        OPEN_TO_CLOSED_BRACES.put(SQUARE_OPEN_BRACKETS, SQUARE_CLOSE_BRACKETS);
        OPEN_TO_CLOSED_BRACES.put(CORNER_OPEN_BRACKETS, CORNER_CLOSE_BRACKETS);
    }

    public boolean isValid(String braces) {
        final Stack<String> stack = new Stack<>();
        for (final String letter : braces.split("")) {
            if (POSSIBLE_BRACES.contains(letter)) {
                if (stack.empty()) {
                    stack.push(letter);
                } else {
                    if (letter.equals(OPEN_TO_CLOSED_BRACES.get(stack.peek()))) {
                        stack.pop();
                    } else {
                        stack.push(letter);
                    }
                }
            }
        }

        return stack.empty();
    }
    
    @Test
    public void testExample() {
        StackExample instance = new StackExample();
        assertThat(instance.isValid("(){}[]")).isTrue();
        assertThat(instance.isValid("( [])")).isTrue();
        assertThat(instance.isValid("( ( < [] () > ))")).isTrue();
        assertThat(instance.isValid("[(] )")).isFalse();
        assertThat(instance.isValid("[(<>] )")).isFalse();
        assertThat(instance.isValid("(}")).isFalse();
        assertThat(instance.isValid("[(])")).isFalse();
        assertThat(instance.isValid("[({})](]")).isFalse();
    }


}
