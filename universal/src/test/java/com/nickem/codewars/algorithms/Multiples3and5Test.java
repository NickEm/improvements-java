package com.nickem.codewars.algorithms;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/514b92a657cdc65150000006
*/
public class Multiples3and5Test {

    public int solution(int number) {
        var result = 0;
        for (int i = 3; i < number; i++) {
            if(i % 3 == 0 || i % 5 == 0) {
                result +=i;
            }
        }

        return result;
    }

    @Test
    public void test1() {
        assertThat(solution(10)).isEqualTo(23);
        assertThat(solution(14)).isEqualTo(45);
        assertThat(solution(15)).isEqualTo(60);
    }
}
