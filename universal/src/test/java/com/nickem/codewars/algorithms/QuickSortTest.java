package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/*
* https://stackabuse.com/quicksort-in-java/
* */
public class QuickSortTest {

    public void quickSort(int[] input, int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(input, begin, end);

            quickSort(input, begin, partitionIndex - 1);
            quickSort(input, partitionIndex + 1, end);
        }
    }

    public void swap(int[] arr, int left, int right){
        int tmp = arr[left];
        arr[left] = arr[right];
        arr[right] = tmp;
    }

    public int partition(int[] arr, int low, int high) {
        /*As a partition we always take first element*/
        int pivot = low;
        for (int j = low + 1; j <= high; j++) {
            /*As a partition we always take first element*/
            if (arr[j] < arr[low]) {
                pivot = pivot + 1;
                swap(arr, pivot, j);
            }
        }

        /*
        * When the for loop is done executing, j has value of high+1, meaning the elements on arr[pivot + 1, high]
        * are higher or equal than the pivot. Because of this it's required we do one more swap of the elements
        * on the position low and p, bringing the pivot at it's correct position in the array (that is, position p)
        * */

        swap(arr, pivot, low);
        return pivot;
    }

    @Test
    public void testSort() {
        int[] actual = { 5, 1, 6, 2, 3, 4, 7};
        int[] expected = { 1, 2, 3, 4, 5, 6, 7 };
        quickSort(actual, 0, actual.length - 1);
        assertThat(actual).isEqualTo(expected);
    }

}
