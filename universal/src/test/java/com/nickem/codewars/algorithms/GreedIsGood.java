package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

/*
* Greed is a die game played with five six-sided dice
* https://www.codewars.com/kata/5270d0d18625160ada0000e4
* */
public class GreedIsGood {

    public static int score(int[] dice){
        int score = 0;
        final Map<Integer, Integer> scores = new HashMap<>();
        for (int die : dice) {
            scores.compute(die, (key, value) -> value == null ? 1 : value + 1);
        }
        for (int i = 1; i <= 6; i++) {
            final Integer sameDiceCount = scores.get(i);
            if (sameDiceCount != null) {
                final int triplets = sameDiceCount / 3;
                if (triplets == 1) {
                    score += scoreForTriple(i);
                    if (sameDiceCount % 3 > 0) {
                        score = handleLessThanTriplet(score, i, sameDiceCount - 3);
                    }
                } else {
                    score = handleLessThanTriplet(score, i, sameDiceCount);
                }
            }
        }

        return score;
    }

    private static int handleLessThanTriplet(int score, int i, Integer sameDiceCount) {
        if (sameDiceCount >= 1) {
            if (i == 5) {
                score += sameDiceCount * 50;
            } else if (i == 1) {
                score += sameDiceCount * 100;
            }
        }
        return score;
    }

    public static int scoreForTriple(Integer value) {
        switch (value) {
            case 1 -> {
                return 1000;
            }
            case 6 -> {
                return 600;
            }
            case 5 -> {
                return 500;
            }
            case 4 -> {
                return 400;
            }
            case 3 -> {
                return 300;
            }
            case 2 -> {
                return 200;
            }
            default -> {
                return 0;
            }
        }
    }

    @Test
    public void testExample() {
        assertThat(score(new int[]{5,1,3,4,1})).as("Score for [5,1,3,4,1] must be 250:").isEqualTo(250);
        assertThat(score(new int[]{1,1,1,3,1})).as("Score for [1,1,1,3,1] must be 1100:").isEqualTo(1100);
        assertThat(score(new int[]{2,4,4,5,4})).as("Score for [2,4,4,5,4] must be 450:").isEqualTo(450);
    }

}
