package com.nickem.codewars.algorithms.cache;

public class LruCacheSimpleTest extends AbstractLruCacheTest {

    @Override
    protected LruCache<String, Integer> getCache(int size) {
        return new LruCacheSimple<>(size);
    }
}
