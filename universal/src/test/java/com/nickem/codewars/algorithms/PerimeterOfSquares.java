package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* https://www.codewars.com/kata/559a28007caad2ac4e000083/train/java
* */
public class PerimeterOfSquares {

    public static BigInteger perimeter(BigInteger n) {
        return BigInteger.valueOf(4L * fibonacciSum(n.longValue() + 1));
    }

    public static long fibonacciSum(long n) {
        long sum = 0;
        for (long i = 1; i <= n; i++) {
            sum = sum + fibonacci(i);
        }

        return sum;
    }

    public static long fibonacci(long n) {
        if (n <= 2) {
            return 1;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(BigInteger.valueOf(5), BigInteger.valueOf(80)),
            Arguments.of(BigInteger.valueOf(7), BigInteger.valueOf(216))
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final BigInteger input, final BigInteger output) {
        assertThat(perimeter(input)).isEqualTo(output);
    }

}
