package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* https://www.codewars.com/kata/55aa075506463dac6600010d
* */
public class IntegersRecreationOne {

    public static String listSquared(long m, long n) {
        Map<Long, Long> result = new LinkedHashMap<>();
        for (long i = m; i <= n; i++) {
            List<Long> divisors = new ArrayList<>();
            for (long j = 1; j <= i; j++) {
                if (i % j == 0) {
                    divisors.add(j);
                }
            }
            final long sum = sumOfElementSquares(divisors);
            final double sqrt = Math.sqrt(sum);
            if (sqrt == (long) sqrt) {
                result.put(i, sum);
            }
        }
        return prepareResult(result);
    }

    private static String prepareResult(Map<Long, Long> result) {
        return "[" +
            result.entrySet()
                .stream()
                .map((entry) -> String.format("[%s, %s]", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining(", "))
            + "]";
    }

    private static long sumOfElementSquares(List<Long> divisors) {
        return divisors.stream().mapToLong(e -> e*e).sum();
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(1, 250, "[[1, 1], [42, 2500], [246, 84100]]"),
            Arguments.of(42, 250, "[[42, 2500], [246, 84100]]"),
            Arguments.of(250, 500, "[[287, 84100]]")
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final long m, final long n, final String expected) {
        assertThat(listSquared(m, n)).isEqualTo(expected);
    }

}
