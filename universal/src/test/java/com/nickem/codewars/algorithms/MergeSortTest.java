package com.nickem.codewars.algorithms;

import static org.assertj.core.api.Assertions.assertThat;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class MergeSortTest {

    /*
    * Takes in the input both the sub-arrays and the start and end indices of both the sub arrays.
    * The merge function compares the elements of both sub-arrays one by one and places the smaller element into
    * the input array.
    * */
    public static void merge(int[] initial, int[] left, int[] right) {
        int leftSize = left.length;
        int rightSize = right.length;
        int leftIndex = 0, rightIndex = 0, initialIndex = 0;
        while (leftIndex < leftSize && rightIndex < rightSize) {
            if (left[leftIndex] <= right[rightIndex]) {
                initial[initialIndex++] = left[leftIndex++];
            } else {
                initial[initialIndex++] = right[rightIndex++];
            }
        }

        /*
        * When we reach the end of one of the sub-arrays, the rest of the elements from the other array are copied
        * into the input array, thereby giving us the final sorted array
        * */
        while (leftIndex < leftSize) {
            initial[initialIndex++] = left[leftIndex++];
        }
        while (rightIndex < rightSize) {
            initial[initialIndex++] = right[rightIndex++];
        }
    }

    /*
     * Splits array on two halves and does merging after-all
     * */
    public static void mergeSort(int[] initial) {
        if (initial.length < 2) {
            return;
        }
        int mid = initial.length / 2;
        int[] left = new int[mid];
        int[] right = new int[initial.length - mid];

        System.arraycopy(initial, 0, left, 0, left.length);
        System.arraycopy(initial, left.length, right, 0, right.length);

        mergeSort(left);
        mergeSort(right);

        merge(initial, left, right);
    }

    @Test
    public void testMerge() {
        int[] initial = new int[7];
        int[] left = { 1, 2, 3, 4 };
        int[] right = { 5, 6, 7 };
        int[] expected = { 1, 2, 3, 4, 5, 6, 7 };
        merge(initial, left, right);
        assertThat(initial).isEqualTo(expected);
    }

    @Test
    public void testMergeSort() {
        int[] actual = { 5, 1, 6, 2, 3, 4, 7};
        int[] expected = { 1, 2, 3, 4, 5, 6, 7 };
        mergeSort(actual);
        assertThat(actual).isEqualTo(expected);
    }

}
