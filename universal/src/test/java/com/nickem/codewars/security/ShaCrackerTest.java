package com.nickem.codewars.security;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class ShaCrackerTest {

    @Test
    public void testExamples() {
        assertEquals("GoOutside", ShaCracker.crackSha256("b8c49d81cb795985c007d78379e98613a4dfc824381be472238dbd2f974e37ae", "deGioOstu"));
        assertEquals("Shot", ShaCracker.crackSha256("068ce7be64dc19be0057c33ed037735f330ba4453bdae0459ec15f464932a590", "thoS"));
        assertEquals(null, ShaCracker.crackSha256("f58262c8005bb64b8f99ec6083faf050c502d099d9929ae37ffed2fe1bb954fb", "abc"));
    }
}
