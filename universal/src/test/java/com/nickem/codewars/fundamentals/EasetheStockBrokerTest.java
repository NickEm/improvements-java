package com.nickem.codewars.fundamentals;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
* https://www.codewars.com/kata/54de3257f565801d96001200
*/
public class EasetheStockBrokerTest {

    static class OrdersSummary {

        public static String balanceStatements(final String list) {
            if (list.isEmpty()) {
                return "Buy is 0 and Sell is 0";
            }
            final var orders = list.split(",");
            var badlyFormedCount = 0;
            var totalBuy = 0d;
            var totalSell = 0d;
            StringBuilder badlyFormed = new StringBuilder("Badly formed %s: ");

            for (String order : orders) {
                if (!order.trim().isEmpty()) {
                    try {
                        final var values = order.trim().split(" ");
                        var count = Integer.valueOf(values[1]);
                        final var priceStr = values[2];
                        if (!priceStr.contains(".")) {
                            throw new IllegalArgumentException("Incorrect price");
                        }
                        var price = Double.valueOf(priceStr);
                        var action = values[3];
                        var totalPrice = count * price;
                        if (action.equals("B")) {
                            totalBuy += totalPrice;
                        } else if (action.equals("S")) {
                            totalSell += totalPrice;
                        } else {
                            throw new IllegalArgumentException("Incorrect action");
                        }

                    } catch (Exception e) {
                        badlyFormedCount++;
                        badlyFormed.append(order.trim() + " ;");
                    }
                }
            }

            final var base = String.format("Buy: %s Sell: %s", (int) Math.round(totalBuy), (int) Math.round(totalSell));
            return badlyFormedCount == 0 ? base : base + "; " + String.format(badlyFormed.toString(), badlyFormedCount);
        }
    }

    @Test
    public void test1() {
        String l = "ZNGA 1300 2.66 B, CLH15.NYM 50 56.32 B, OWW 1000 11.623 B, OGG 20 580.1 B";
        assertEquals("Buy: 29499 Sell: 0", OrdersSummary.balanceStatements(l));
    }

    @Test
    public void test2() {
        String l = "GOOG 90 160.45 B, JPMC 67 12.8 S, MYSPACE 24.0 210 B, CITI 50 450 B, CSCO 100 55.5 S";
        assertEquals("Buy: 14440 Sell: 6408; Badly formed 2: MYSPACE 24.0 210 B ;CITI 50 450 B ;",
                     OrdersSummary.balanceStatements(l));
    }
}
