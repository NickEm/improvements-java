package com.nickem.codewars.fundamentals;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;

public class WhoLikesItTest {
    
    public static String whoLikesIt(String... names) {
        switch (names.length) {
            case 0: return "no one likes this";
            case 1: return String.format("%s likes this", names[0]);
            case 2: return String.format("%s and %s like this", names[0], names[1]);
            case 3: return String.format("%s, %s and %s like this", names[0], names[1], names[2]);
            default: return String.format("%s, %s and %d others like this", names[0], names[1], names.length - 2);
        }
    }

    @Test
    public void staticTests() {
        assertThat("no one likes this").isEqualTo(whoLikesIt());
        assertThat("Peter likes this").isEqualTo(whoLikesIt("Peter"));
        assertThat("Jacob and Alex like this").isEqualTo(whoLikesIt("Jacob", "Alex"));
        assertThat("Max, John and Mark like this").isEqualTo(whoLikesIt("Max", "John", "Mark"));
        assertThat("Alex, Jacob and 2 others like this").isEqualTo(whoLikesIt("Alex", "Jacob", "Mark", "Max"));
    }
}
