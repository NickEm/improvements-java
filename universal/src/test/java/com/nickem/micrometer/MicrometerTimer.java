package com.nickem.micrometer;

import static org.assertj.core.api.Assertions.assertThat;

import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.distribution.HistogramSnapshot;
import io.micrometer.core.instrument.distribution.ValueAtPercentile;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class MicrometerTimer {

    @Test
    public void testSimpleSummary() {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
        DistributionSummary distributionSummary = DistributionSummary
            .builder("request.size")
            .baseUnit("bytes")
            .register(registry);

        distributionSummary.record(3);
        distributionSummary.record(5);
        distributionSummary.record(12);

        assertThat(distributionSummary.count()).isEqualTo(3);
        assertThat(distributionSummary.totalAmount()).isEqualTo(20);
        log.info("Max [{}]", distributionSummary.max());
        log.info("Mean [{}]", distributionSummary.mean());
    }

    @Test
    public void testTimerSimplePercentiles() {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
        Timer timer = Timer
            .builder("test.timer")
            .publishPercentiles(0.2, 0.25, 0.3, 0.5, 0.8, 0.90)
            .publishPercentileHistogram()
            .register(registry);


        timer.record(2, TimeUnit.SECONDS);
        timer.record(2, TimeUnit.SECONDS);
        timer.record(3, TimeUnit.SECONDS);
        timer.record(4, TimeUnit.SECONDS);
        timer.record(5, TimeUnit.SECONDS);

        timer.record(8, TimeUnit.SECONDS);
        timer.record(10, TimeUnit.SECONDS);
        timer.record(12, TimeUnit.SECONDS);
        timer.record(14, TimeUnit.SECONDS);
        timer.record(20, TimeUnit.SECONDS);

        log.info("Count [{}]", timer.count());
        log.info("Max [{}]", timer.max(TimeUnit.SECONDS));
        log.info("Mean [{}]", timer.mean(TimeUnit.SECONDS));
        log.info("Measure [{}]", timer.measure());

        Map<Double, Double> actualMicrometer = new TreeMap<>();
        ValueAtPercentile[] percentiles = timer.takeSnapshot().percentileValues();
        for (ValueAtPercentile percentile : percentiles) {
            actualMicrometer.put(percentile.percentile(), percentile.value(TimeUnit.MILLISECONDS));
        }

        Map<Double, Double> expectedMicrometer = new TreeMap<>();
        expectedMicrometer.put(0.2, 1946.157056);
        expectedMicrometer.put(0.25, 3019.89888);
        expectedMicrometer.put(0.3, 3019.89888);
        expectedMicrometer.put(0.5, 5033.1648);
        expectedMicrometer.put(0.8, 12280.922112);
        expectedMicrometer.put(0.9, 14428.40576);

        assertThat(actualMicrometer).isEqualTo(expectedMicrometer);
    }

    @Test
    public void testServiceLevelObjective() {
        SimpleMeterRegistry registry = new SimpleMeterRegistry();
        DistributionSummary hist = DistributionSummary
            .builder("summary")
            .serviceLevelObjectives(1, 5, 9, 10)
            .register(registry);

        hist.record(3);
        hist.record(6);
        hist.record(9);
        hist.record(11);

        Map<Integer, Double> actualMicrometer = new TreeMap<>();
        HistogramSnapshot snapshot = hist.takeSnapshot();
        Arrays.stream(snapshot.histogramCounts()).forEach(p -> {
            actualMicrometer.put(((int) p.bucket()), p.count());
        });

        Map<Integer, Double> expectedMicrometer = new TreeMap<>();
        expectedMicrometer.put(1, 0D);
        expectedMicrometer.put(5, 1D);
        expectedMicrometer.put(9, 3D);
        expectedMicrometer.put(10, 3D);

        assertThat(actualMicrometer).isEqualTo(expectedMicrometer);
    }
}
