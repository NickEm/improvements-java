package com.nickem.micrometer;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@Slf4j
public class ParsePrometheusMetricsTest {

    private static final String REGEX = "^([a-zA-Z0-9_]+)(\\{[^{}]*\\})?\\s.*$";
    private static final Pattern PATTERN = Pattern.compile(REGEX);

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
//            Arguments.of("prometheus/vault-preferences-1.txt"),
//            Arguments.of("prometheus/worker-1.txt"),
            Arguments.of("prometheus/provisioning-1.txt")
//            Arguments.of("prometheus/worker-2.txt")
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void testParse(String filePath) throws IOException {
        final var resourceStream = Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(filePath));
        final List<String> lines = IOUtils.readLines(resourceStream, StandardCharsets.UTF_8);
        final List<String> result = lines.stream()
            .filter(e -> !StringUtils.startsWithAny(e, "#"))
            .map(this::parseMetricName)
            .distinct()
            .sorted()
            .toList();

        result.forEach(System.out::println);
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void testParseHelpNotes(String filePath) throws IOException {
        final var resourceStream = Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(filePath));
        final List<String> lines = IOUtils.readLines(resourceStream, StandardCharsets.UTF_8);
        final List<String> result = lines.stream()
            .filter(e -> StringUtils.startsWithAny(e, "# HELP"))
            .distinct()
            .sorted()
            .toList();

        result.forEach(System.out::println);
    }

    @Test
    public void testParseMetric() {
        var input1 = "kafka_producer_node_request_rate{client_id=\"logging:KAFKA_JSON\",kafka_version=\"3.3.1\",node_id=\"node-3\",} 0.5150214592274678";
        assertThat(parseMetricName(input1)).isEqualTo("kafka_producer_node_request_rate");
        var input2 = "system_load_average_1m 0.5";
        assertThat(parseMetricName(input2)).isEqualTo("system_load_average_1m");
    }

    private String parseMetricName(String input) {
        Matcher matcher = PATTERN.matcher(input);

        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new IllegalArgumentException("Matcher doesn't match input [%s]".formatted(input));
        }
    }

}
