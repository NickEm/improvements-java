package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import com.nickem.util.PermutationUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
 * LeetCode 713. Subarray Product Less Than K
 * Given an array of integers `nums` and an integer `k`, return the number of contiguous subarrays where the product of all
 * the elements in the subarray is strictly less than `k`.
 * */
/*TODO*/
@Slf4j
public class SubarrayProductLessThanK {

    public int numSubarrayProductLessThanK(int[] nums, int k) {
        int result = Arrays.stream(nums).filter(e -> e < k).sum();

        for (int i = 0; i < k; i++) {

        }
        for (int num : nums) {
//            for(int j)
        }

        return result;
    }

    public int[][] differentCombination(int[] nums, int length) {
        int[][] result = new int[nums.length][length];

        for (int i = 0; i < nums.length; i++) {

        }

        return null;
    }

    public int[][] combinations(int[] hold, int[] rest) {
        int[][] result = new int[rest.length][hold.length + 1];

        for (int i = 0; i < rest.length; i++) {
            int[] combination = new int[hold.length + 1];
            System.arraycopy(hold, 0, combination, 0, hold.length);
            combination[hold.length] = rest[i];
            result[i] = combination;
        }

        return result;
    }

    @Test
    void testGenerator() {
        final Set<int[]> result = new HashSet<>();
        final int[] prefix = new int[0];
        final int[] input = new int[] {1, 2, 3};
        PermutationUtil.permutationsIntArray(input, prefix, 3, result);
        log.info("{}", result.stream().map(e -> Arrays.stream(e).boxed().collect(Collectors.toList()))
            .collect(Collectors.toList()));
    }



    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(new int[]{1, 2, 3}, 0, 0),
            Arguments.of(new int[]{10, 5, 2, 6}, 100, 8)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(int[] nums, int k, int expected) {
        assertThat(numSubarrayProductLessThanK(nums, k)).isEqualTo(expected);
    }


    private static Stream<Arguments> assertCombinationProvider() {
        return Stream.of(
            Arguments.of(new int[]{1, 2, 3}, 0, 0),
            Arguments.of(new int[]{10, 5, 2, 6}, 100, 8)
        );
    }

    @ParameterizedTest
    @MethodSource("assertCombinationProvider")
    public void testCombination(int[] nums, int k, int expected) {
        assertThat(numSubarrayProductLessThanK(nums, k)).isEqualTo(expected);
    }
}
