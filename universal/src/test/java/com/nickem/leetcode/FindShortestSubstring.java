package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class FindShortestSubstring {

    private int shortestSubstringLength(String original, String part) {
        final List<String> originalArray = Arrays.stream(original.split("")).collect(Collectors.toList());
        final List<String> partArray = Arrays.stream(part.split("")).collect(Collectors.toList());

        int partEntryIndex = 0;
        int shortestLength = 0;
        int currentSum = 0;
        List<String> currentArray = new ArrayList<>(partArray);
        for (String s : originalArray) {
            if (s.equals(partArray.get(partEntryIndex))) {
                currentSum++;
            } else if (partArray.size() > partEntryIndex + 1 && s.equals(partArray.get(partEntryIndex + 1))) {
                /*this is processing all entries except last in the part*/
                currentArray.remove(partArray.get(partEntryIndex));
                currentSum++;
                partEntryIndex++;
                if (currentArray.size() == 1 && s.equals(partArray.get(partArray.size() - 1))) {
                    if (shortestLength > 0) {
                        shortestLength = Math.min(shortestLength, currentSum);
                    } else {
                        shortestLength = currentSum;
                    }
                    currentSum = 0;
                    currentArray = new ArrayList<>(partArray);
                }
            } else {
                partEntryIndex = 0;
                currentSum = 0;
                currentArray = new ArrayList<>(partArray);
            }
        }

        return shortestLength;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of("abbcabbacc", "abc", 4),
            Arguments.of("aaaaadddddabgceevwevgwegewaaddcaaadacaaaaadddddca", "adc", 4),
            Arguments.of("aaaaadddddwaddc", "adc", 4)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(String original, String part, int expected) {
        assertThat(shortestSubstringLength(original, part)).isEqualTo(expected);
    }


    private int shortestSubstringLengthStale(String original, String part) {
        int shortestEntry = 0;
        ArrayDeque<String> originalStack = Arrays.stream(part.split(""))
            .collect(Collectors.toCollection(ArrayDeque::new));

        int counter = 0;
        Queue<String> tempStack = originalStack.clone();
        final String[] letters = original.split("");

        for (String letter : part.split("")) {

        }
        for (String letter : letters) {
            if (letter.equals(tempStack.peek())) {
                tempStack.poll();
                counter++;
                if (originalStack.size() == 0) {
                    shortestEntry = counter;
                    counter = 0;
                    tempStack = originalStack.clone();
                }
            }
        }

        return shortestEntry;
    }

}
