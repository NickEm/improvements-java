package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * https://leetcode.com/problems/minimum-remove-to-make-valid-parentheses/
 *
 * 1249. Minimum Remove to Make Valid Parentheses
 *
 * Given a string s of '(' , ')' and lowercase English characters.
 *
 * Your task is to remove the minimum number of parentheses ( '(' or ')', in any positions ) so that
 * the resulting parentheses string is valid and return any valid string.
 * Formally, a parentheses string is valid if and only if:
 * * It is the empty string, contains only lowercase characters, or
 * * It can be written as AB (A concatenated with B), where A and B are valid strings, or
 * * It can be written as (A), where A is a valid string.
 */
@Slf4j
public class MinimumRemoveToMakeValidParentheses {

    public String minRemoveToMakeValid(String input) {
        String result = input;
        Stack<String> braces = new Stack<>();
        LinkedList<Integer> toRemove = new LinkedList<>();
        for (int i = 0; i < input.split("").length; i++) {
            final String character = input.substring(i, i + 1);
            if (character.equals("(")) {
                braces.push("(");
                toRemove.add(i);
            } else if (character.equals(")")) {
                if (braces.isEmpty()) {
                    toRemove.add(i);
                } else if (braces.peek().equals("(")) {
                    braces.pop();
                    toRemove.removeLast();
                }
            }
        }

        for (int i = 0; i < toRemove.size(); i++) {
            final int index = toRemove.get(i) - i;
            result = result.substring(0, index) + (index + 1 < result.length() ? result.substring(index + 1) : "");
        }

        return result;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of("lee(t(c)o)de)", ImmutableList.of("lee(t(c)o)de", "lee(t(co)de)" , "lee(t(c)ode)")),
            Arguments.of("a)b(c)d", ImmutableList.of("ab(c)d")),
            Arguments.of("))((", ImmutableList.of(""))
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final String input, final List<String> expected) {
        assertThat(minRemoveToMakeValid(input)).isIn(expected);
    }
}
