package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class FirstBadVersionTest {

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(5, 4),
            Arguments.of(8, 5),
            Arguments.of(36, 18),
            Arguments.of(38, 20),
            Arguments.of(79, 9),
            Arguments.of(1134, 335)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final int input, final int firstBadVersion) {
        FirstBadVersion instance = new FirstBadVersion() {
            protected boolean isBadVersion(int version) {
                return version >= firstBadVersion;
            }
        };
        assertThat(instance.firstBadVersion(input)).isEqualTo(firstBadVersion);
    }

}
