package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ClosestPointsToOrigin {

    public int[][] kClosest(int[][] points, int k) {
        final PriorityQueue<int[]> pointsQueue = new PriorityQueue<>(k, (o1, o2) -> Double.compare(distance(o2), distance(o1)));
        for (int[] point : points) {
            if (pointsQueue.size() < k) {
                pointsQueue.offer(point);
            } else if (pointsQueue.size() == k && distance(point) < distance(Objects.requireNonNull(pointsQueue.peek()))) {
                pointsQueue.poll();
                pointsQueue.offer(point);
            }
        }

        return pointsQueue.toArray(int[][]::new);
    }

    public int[][] traditionalClosest(int[][] points, int k) {
        return Arrays.stream(points)
            .sorted(Comparator.comparingDouble(this::distance))
            .limit(k)
            .toArray(int[][]::new);
    }

    public double distance(int[] point) {
        return Math.sqrt(Math.pow(point[0], 2) + Math.pow(point[1], 2));
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(new int[][]{
                    {1, 3}, {-2, 2}
                }, 1,
                new int[][]{{-2, 2}}
            ), Arguments.of(new int[][]{
                    {3, 3}, {1, 1}, {5, -1}, {-2, 4}, {-2, 3}
                }, 3,
                new int[][]{{3, 3}, {1, 1}, {-2, 3}}
            ), Arguments.of(new int[][]{
                    {0, 5}, {7, 8}, {3, 3}, {6, 3}, {5, 3}
                }, 3,
                new int[][]{{3, 3}, {0, 5}, {5, 3}}
            )
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final int[][] input, final int k, final int[][] expected) {
        final List<int[]> actual = Arrays.asList(kClosest(input, k));
        assertThat(actual).containsExactlyInAnyOrderElementsOf(Arrays.asList(expected));
    }

}
