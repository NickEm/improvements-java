package com.nickem.leetcode;


import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import com.nickem.util.PermutationUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
 * Given a characters array tasks, representing the tasks a CPU needs to do, where each letter represents a different task.
 * Tasks could be done in any order. Each task is done in one unit of time.
 * For each unit of time, the CPU could complete either one task or just be idle.
 * However, there is a non-negative integer n that represents the cooldown period between two same tasks (the same letter in the array),
 * that is that there must be at least n units of time between any two same tasks.
 * Return the least number of units of times that the CPU will take to finish all the given tasks.
 * */
@Slf4j
public class TaskSchedulerTest {

    /*
     * A idle idle --> length 3
     * A idle idle --> length 3
     * */

    Set<String[]> possibleTaskSchedules(final String[] tasks) {
        final Set<List<String>> combinations = PermutationUtil.permutations(Arrays.asList(tasks));
//        combinations.forEach(e -> System.out.println(prepareLine(e)));
        return combinations.stream().map(e -> e.toArray(new String[0])).collect(Collectors.toSet());
    }

    private String prepareLine(List<String> input) {
        final String collect = input.stream().collect(Collectors.joining("\",\""));
        return String.format("ImmutableList.of(%s)", collect);
    }

    int countMinimalTime(final String[] tasks, final int coolDown) {
        final Set<String[]> possibleSchedules = possibleTaskSchedules(tasks);
        return possibleSchedules.stream().mapToInt(e -> countExecutionTime(e, coolDown)).min().getAsInt();
    }

    int countExecutionTime(final String[] tasks, final int coolDown) {
        List<String> tasksToDo = new LinkedList<>();
        for (int currentIndex = 0; currentIndex < tasks.length; currentIndex++) {
            final String task = tasks[currentIndex];
            if (tasksToDo.contains(task)) {
                for (int i = tasksToDo.size() - 1; i >= Math.max(0, tasksToDo.size() - coolDown); i--) {
                    final String taskToDo = tasksToDo.get(i);
                    if (taskToDo.equals(task)) {
                        addCoolDownOperation(tasksToDo, coolDown - (tasksToDo.size() - i) + 1);
                        break;
                    }
                }
                tasksToDo.add(task);
            } else {
                tasksToDo.add(task);
            }
        }

        log.info("For tasks {} result is {}", tasks,  tasksToDo.size());
        return tasksToDo.size();
    }

    void addCoolDownOperation(final List<String> tasksToDo, final int times) {
        for (int i = 0; i < times; i++) {
            tasksToDo.add("idle");
        }
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(new String[]{"A", "A"}, 2, 4),
            Arguments.of(new String[]{"B", "B", "B"}, 2, 7),
            Arguments.of(new String[]{"A", "A", "A", "B", "B", "B"}, 2, 8),
            Arguments.of(new String[]{"A", "A", "A", "B", "B", "B"}, 0, 6),
            Arguments.of(new String[]{"A", "A", "A", "A", "A", "A", "B", "C", "D", "E", "F", "G"}, 2, 16)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final String[] tasks, final int coolDown, final int expected) {
        assertThat(countMinimalTime(tasks, coolDown)).isEqualTo(expected);
    }

    @Test
    public void testCollectionVsArray() {
        Set<List<String>> setOfLists = new HashSet<>();

        setOfLists.add(ImmutableList.of("A", "B", "C"));
        setOfLists.add(ImmutableList.of("A", "C", "B"));
        setOfLists.add(ImmutableList.of("A", "C", "B"));
        assertThat(setOfLists.size()).isEqualTo(2);

        Set<String[]> setOfArrays = new HashSet<>();
        setOfArrays.add(new String[]{"A", "B", "C"});
        setOfArrays.add(new String[]{"A", "C", "B"});
        setOfArrays.add(new String[]{"A", "C", "B"});
        /*Arrays don't work with equals and hashmaps as usual collections*/
        assertThat(setOfArrays.size()).isEqualTo(3);
    }

    @Test
    public void testArrayList() {
        List<String> list = new ArrayList<>();
        list.add("A");
        list.add("B");
        list.add("D");

        list.add(2, "C");

        assertThat(list.size()).isEqualTo(3);
    }
}
