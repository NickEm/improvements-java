package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
 * LeetCode 1222. Queens That Can Attack the King
 * On an 8x8 chessboard, there can be multiple Black Queens and one White King.
 * Given an array of integer coordinates queens that represents the positions of the Black Queens,
 * and a pair of coordinates king that represent the position of the White King,
 * return the coordinates of all the queens (in any order) that can attack the King.
 */
public class QueensThatCanAttackTheKingTest {

    /*
     * Input: queens = [[0,1],[1,0],[4,0],[0,4],[3,3],[2,4]], king = [0,0]
     * Output: [[0,1],[1,0],[3,3]]
     * Explanation:
     * The queen at [0,1] can attack the king cause they're in the same row.
     * The queen at [1,0] can attack the king cause they're in the same column.
     * The queen at [3,3] can attack the king cause they're in the same diagnal.
     * The queen at [0,4] can't attack the king cause it's blocked by the queen at [0,1].
     * The queen at [4,0] can't attack the king cause it's blocked by the queen at [1,0].
     * The queen at [2,4] can't attack the king cause it's not in the same row/column/diagnal as the king.
     * */
    public List<List<Integer>> queensAttacksTheKing(int[][] queens, int[] king) {
        List<List<Integer>> result = new ArrayList<>();
        int[][] sortedQueens = sortByClosest(queens, king);
        for (int[] queen : sortedQueens) {
            if (inTheSameColumn(queen, king) || inTheSameRow(queen, king) || inTheSameDiagonal(queen, king)) {
                if (inNotBlocked(queen, king, result)) {
                    result.add(Arrays.stream(queen).boxed().collect(Collectors.toList()));
                }
            }
        }
        return result.stream().sorted(
            (o1, o2) -> ComparisonChain.start()
                .compare(o1.get(0), o2.get(0))
                .compare(o1.get(1), o2.get(1))
                .result()
        ).collect(Collectors.toList());
    }

    public int[][] sortByClosest(int[][] queens, int[] king) {
        return Arrays.stream(queens).sorted(Comparator.comparingDouble(o -> distance(o, king))).toArray(int[][]::new);
    }

    public double distance(int[] queen, int[] king) {
        return Math.sqrt(Math.pow(Math.abs(queen[0] - king[0]), 2) + Math.pow(Math.abs(queen[1] - king[1]), 2));
    }

    public boolean inTheSameRow(int[] queen, int[] figure) {
        return queen[0] == figure[0];
    }

    public boolean inTheSameRow(int[] queen, int[] king, int[] attackingQueen) {
        return inTheSameRow(queen, king) && inTheSameRow(king, attackingQueen);
    }

    public boolean inTheSameColumn(int[] queen, int[] figure) {
        return queen[1] == figure[1];
    }

    public boolean inTheSameColumn(int[] queen, int[] king, int[] attackingQueen) {
        return inTheSameColumn(queen, king) && inTheSameColumn(king, attackingQueen);
    }

    public boolean inTheSameDiagonal(int[] queen, int[] figure) {
        return Math.abs(queen[0] - figure[0]) == Math.abs(queen[1] - figure[1]);
    }

    public boolean closerThanKingOnTheDiagonal(int[] queen, int[] attackingQueen, int[] king) {
        return (attackingQueen[0] < king[0] && queen[0] < king[0]) ||
            (attackingQueen[0] > king[0] && queen[0] > king[0]);
    }

    public boolean inNotBlocked(int[] queen, int[] king, List<List<Integer>> queens) {
        boolean result = true;
        for (List<Integer> attackingQ : queens) {
            int[] attackingQueen = new int[]{attackingQ.get(0), attackingQ.get(1)};
            if (inTheSameRow(queen, king, attackingQueen) ||
                inTheSameColumn(queen, king, attackingQueen) ||
                (inTheSameDiagonal(queen, attackingQueen) && closerThanKingOnTheDiagonal(queen, attackingQueen,
                    king))) {
                result = false;
                break;
            }
        }
        return result;
    }


    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(new int[][]{{0, 0}, {1, 1}, {2, 2}, {3, 4}, {3, 5}, {4, 4}, {4, 5}}, new int[]{3, 3},
                ImmutableList.of(ImmutableList.of(2, 2), ImmutableList.of(3, 4), ImmutableList.of(4, 4))),
            Arguments.of(new int[][]{{0, 1}, {1, 0}, {4, 0}, {0, 4}, {3, 3}, {2, 4}}, new int[]{0, 0},
                ImmutableList.of(ImmutableList.of(0, 1), ImmutableList.of(1, 0), ImmutableList.of(3, 3)))
        );
    }

    @Test
    public void sortByClosestTest() {
        final int[][] queens = {{4, 5}, {3, 5}, {2, 2}, {4, 4}};
        final int[] king = new int[]{0, 0};
        assertThat(sortByClosest(queens, king)).isDeepEqualTo(new int[][]{{2, 2}, {4, 4}, {3, 5}, {4, 5}});
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(int[][] queens, int[] king, List<List<Integer>> expected) {
        assertThat(queensAttacksTheKing(queens, king)).isEqualTo(expected);
    }

}
