package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class GenerateParentheses {

    public List<String> generateParenthesis(int pairs) {
        Set<String> result = new TreeSet<>();
        backwards(result, "", 0, 0, pairs);
        return new ArrayList<>(result);
    }

    private void backwards(Set<String> result, String current, int open, int close, int pairs) {
        if(current.length() == pairs * 2) {
            result.add(current);
            return;
        }

        if (open < pairs) {
            backwards(result, current + "(", open + 1, close, pairs);
        }
        if (close < open) {
            backwards(result, current + ")", open, close + 1, pairs);
        }

    }


    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(1, ImmutableList.of("()")),
            Arguments.of(2, ImmutableList.of("()()", "(())")),
            Arguments.of(3, ImmutableList.of("((()))","(()())","(())()","()(())","()()()")),
            Arguments.of(4, ImmutableList.of("(((())))","(()()())","((()))()","()((()))","()()()()", "(())()()", "()()(())", "((()()))", "((())())", "(()(()))", "(()())()", "(())(())", "()(()())", "()(())()"))
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final int pairs, final List<String> expected) {
        assertThat(generateParenthesis(pairs)).containsExactlyInAnyOrderElementsOf(expected);
    }

}
