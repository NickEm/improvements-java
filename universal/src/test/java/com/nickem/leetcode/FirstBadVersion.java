package com.nickem.leetcode;

import lombok.extern.slf4j.Slf4j;

/**
 * https://leetcode.com/problems/first-bad-version/
 *
 * 278. First Bad Version
 *
 * Given two strings `input` and `word`, return an array of all the start indices of `word` anagrams in `input`.
 * You may return the answer in any order.
 * An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
 *
 * */
@Slf4j
public abstract class FirstBadVersion {

    public int firstBadVersion(int n) {
        int high = n;
        int low = 0;

        int lastBadIndex = 0;

        while (low <= high) {
            int mid = low  + ((high - low) / 2);
            log.info("mid {}", mid);
            if (isBadVersion(mid)) {
                lastBadIndex = mid;
                high = mid;
            } else {
                if (lastBadIndex == mid + 1) {
                    break;
                }
                low = mid;
            }
        }

        return lastBadIndex;
    }

    protected abstract boolean isBadVersion(int version);
}
