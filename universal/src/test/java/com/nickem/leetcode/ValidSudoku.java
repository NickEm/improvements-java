package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/*
* 36. Valid Sudoku
*
* Determine if a 9 x 9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:
* * Each row must contain the digits 1-9 without repetition.
* * Each column must contain the digits 1-9 without repetition.
* * Each of the nine 3 x 3 sub-boxes of the grid must contain the digits 1-9 without repetition.
* Note:
* * A Sudoku board (partially filled) could be valid but is not necessarily solvable.
* * Only the filled cells need to be validated according to the mentioned rules.
*
* */
@Slf4j
public class ValidSudoku {

    private final static List<Character> VALID_CHAR_ARRAY = Arrays.asList('1','2','3','4','5','6','7','8','9');

    public boolean isValidSudoku(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            if (lineIsInvalid(board[i])) {
                log.info("Row [{}] is invalid {}", i, Arrays.toString(board[i]));
                return false;
            }
            final char[] column = getColumn(board, i);
            if (lineIsInvalid(column)) {
                log.info("Column [{}] is invalid {}", i, Arrays.toString(column));
                return false;
            }

            if ((i + 1) % 3 == 0) {
                List<Character> squareList1 = new ArrayList<>();
                List<Character> squareList2 = new ArrayList<>();
                List<Character> squareList3 = new ArrayList<>();

                for (int j = i + 1 - 3; j < i + 1; j++) {
                    for (int k = 0; k < board[j].length; k++) {
                        if (k < 3) {
                            squareList1.add(board[j][k]);
                        } else if (k < 6) {
                            squareList2.add(board[j][k]);
                        } else {
                            squareList3.add(board[j][k]);
                        }
                    }
                }

                if (lineIsInvalid(toChars(squareList1))) {
                    log.info("Square [{}]:1 is invalid {}", i, squareList1);
                    return false;
                }
                if (lineIsInvalid(toChars(squareList2))) {
                    log.info("Square [{}]:2 is invalid {}", i, squareList2);
                    return false;
                }

                if (lineIsInvalid(toChars(squareList2))) {
                    log.info("Square [{}]:3 is invalid {}", i, squareList3);
                    return false;
                }
            }

        }

        return true;
    }

    private char[] toChars(List<Character> squareList1) {
        return squareList1.stream().map(Object::toString).collect(Collectors.joining("")).toCharArray();
    }

    public static char[] getColumn(char[][] array, int index) {
        char[] column = new char[array[0].length];
        for (int i = 0; i < column.length; i++) {
            column[i] = array[i][index];
        }
        return column;
    }

    public boolean lineIsInvalid(char[] line) {
        boolean result = false;
        final List<Character> current = new ArrayList<>(VALID_CHAR_ARRAY);
        for (char c : line) {
            if (c != '.') {
                if (current.contains(c)) {
                    current.remove(Character.valueOf(c));
                } else {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(new char[][]{
                {'5','3','.','.','7','.','.','.','.'},
                {'6','.','.','1','9','5','.','.','.'},
                {'.','9','8','.','.','.','.','6','.'},
                {'8','.','.','.','6','.','.','.','3'},
                {'4','.','.','8','.','3','.','.','1'},
                {'7','.','.','.','2','.','.','.','6'},
                {'.','6','.','.','.','.','2','8','.'},
                {'.','.','.','4','1','9','.','.','5'},
                {'.','.','.','.','8','.','.','7','9'}}, true),
            Arguments.of(new char[][]{
                {'8','3','.','.','7','.','.','.','.'},
                {'6','.','.','1','9','5','.','.','.'},
                {'.','9','8','.','.','.','.','6','.'},
                {'8','.','.','.','6','.','.','.','3'},
                {'4','.','.','8','.','3','.','.','1'},
                {'7','.','.','.','2','.','.','.','6'},
                {'.','6','.','.','.','.','2','8','.'},
                {'.','.','.','4','1','9','.','.','5'},
                {'.','.','.','.','8','.','.','7','9'}}, false)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final char[][] input, final boolean expected) {
        assertThat(isValidSudoku(input)).isEqualTo(expected);
    }

    @Test
    public void test1() {

    }
}
