package com.nickem.leetcode;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import com.nickem.util.PermutationUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * https://leetcode.com/problems/find-all-anagrams-in-a-string/
 *
 * 438. Find All Anagrams in a String
 *
 * Given two strings `input` and `word`, return an array of all the start indices of `word` anagrams in `input`.
 * You may return the answer in any order.
 * An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
 *
 * */
@Slf4j
public class FindAllAnagrams {

    public List<Integer> findAnagrams(String input, String word) {
        final List<Integer> result = new ArrayList<>();
        final List<String> permutations = PermutationUtil.permutations(word);
        for (int i = 0; i < input.split("").length - word.length() + 1; i++) {
            final String checkChunk = input.substring(i, i + word.length());
            if (permutations.contains(checkChunk)) {
                result.add(i);
            }
        }

        return result;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of("cbaebabacd", "abc", ImmutableList.of(0, 6)),
            Arguments.of("abab", "ab", ImmutableList.of(0, 1, 2))
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final String input, final String word, final List<Integer> expected) {
        assertThat(findAnagrams(input, word)).isEqualTo(expected);
    }

}
