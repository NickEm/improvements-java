package com.nickem.leetcode;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.Stack;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * https://leetcode.com/problems/score-of-parentheses/
 *
 * 856. Score of Parentheses
 *
 * Given a balanced parentheses string s, return the score of the string.
 *
 * The score of a balanced parentheses string is based on the following rule:
 * * "()" has score 1.
 * * AB has score A + B, where A and B are balanced parentheses strings.
 * * (A) has score 2 * A, where A is a balanced parentheses string.
 *
 */
public class ScoreOfParentheses {

    public int scoreOfParentheses(String s) {
        int sum = 0;
        boolean shouldMultiply = false;
        final Stack<String> stack = new Stack<>();
        for (String chunk : s.split("")) {
            if (stack.isEmpty()) {
                stack.push(chunk);
            }
            if (chunk.equals(")") && stack.peek().equals("(")) {
                stack.pop();
                if (!shouldMultiply) {
                    sum += 1;
                    shouldMultiply = true;
                } else {
                    sum = 2 * sum;
                }
            } else {
                shouldMultiply = false;
                stack.push(chunk);
            }
        }

        return sum;
    }

    public int scoreOfParenthesesExplained(String s) {
        int sum = 0;
        final Stack<Integer> stack = new Stack<>();
        for (String chunk : s.split("")) {
            if (chunk.equals("(")) {
                stack.push(sum);
                sum = 0;
            } else {
                sum = stack.pop() + Math.max(sum*2, 1);
            }
        }

        return sum;
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of("()", 1),
            Arguments.of("(())", 2),
            Arguments.of("()()", 2),
            Arguments.of("((()))", 4),
            Arguments.of("((()()))", 8),
            Arguments.of("((())())", 6)
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final String input, final int expected) {
        assertThat(scoreOfParenthesesExplained(input)).isEqualTo(expected);
    }

}
