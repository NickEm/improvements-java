package com.nickem.interview;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.collect.ImmutableList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import lombok.Data;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


/*
 * http://nexla.com/
 * */
public class InterviewNexla {

    @Data
    static class Officer {

        String id;
        List<Officer> dependencies;

        public Officer(String id, List<Officer> dependencies) {
            this.id = id;
            this.dependencies = dependencies;
        }

        public Officer(String id) {
            this(id, Collections.emptyList());
        }

        @Override
        public String toString() {
            return "{" + id + "}";
        }
    }

    public List<Officer> getRoute(List<Officer> officers) {
        final Set<Officer> result = new LinkedHashSet<>();
        for (Officer officer : officers) {
            final LinkedHashSet<Officer> officerTree = new LinkedHashSet<>();
            findLastDocument(officerTree, officer);
            result.addAll(officerTree);
        }
        return new ArrayList<>(result);
    }


    public Set<Officer> findLastDocument(Set<Officer> result, Officer officer) {
        if (officer.getDependencies().size() < 1) {
            return result;
        } else {
            for (Officer dependency : officer.getDependencies()) {
                if (!result.contains(dependency)) {
                    findLastDocument(result, dependency);
                    result.add(dependency);
                }
            }
            if (!result.contains(officer)) {
                result.add(officer);
            }
            return result;
        }
    }

    private static List<Officer> prepareOfficer2Deps() {
        final Officer initialFirst = new Officer("passport");
        final Officer initialSecond = new Officer("identificationCode");

        return ImmutableList.of(new Officer("internationPassport", ImmutableList.of(initialFirst, initialSecond)));
    }

    private static List<Officer> prepareOfficer3Deps() {
        final Officer passport = new Officer("passport");
        final Officer identificationCode = new Officer("identificationCode");

        final Officer internationPassport = new Officer("internationPassport", ImmutableList.of(passport, identificationCode));
        return ImmutableList.of(new Officer("visa", ImmutableList.of(internationPassport)));
    }

    private static Officer prepareOfficer(String id, List<Officer> dependencies) {
        return new Officer(id, dependencies);
    }

    private static Stream<Arguments> assertionProvider() {
        return Stream.of(
            Arguments.of(prepareOfficer2Deps(), "[{passport}, {identificationCode}, {internationPassport}]"),
            Arguments.of(prepareOfficer3Deps(), "[{passport}, {identificationCode}, {internationPassport}, {visa}]")
        );
    }

    @ParameterizedTest
    @MethodSource("assertionProvider")
    public void test(final List<Officer> input, String expected) {
        assertThat(getRoute(input).toString()).isEqualTo(expected);
    }
}
