package com.nickem.cache;

import static org.assertj.core.api.Assertions.assertThat;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.Test;

public class GuavaCache {

    @Test
    public void offsetToEpochAndBack() throws InterruptedException {
        Cache<String, String> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(1, TimeUnit.SECONDS)
            .build();
        cache.put("one-key", "one-value");
        assertThat(cache.getIfPresent("one-key")).isEqualTo("one-value");
        Thread.sleep(500);
        assertThat(cache.getIfPresent("one-key")).isEqualTo("one-value");
        Thread.sleep(500);
        assertThat(cache.getIfPresent("one-key")).isNull();
    }

}
