package com.nickem.junit;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class BaseClassTwo extends BaseTest {

//    @BeforeClass
//    public static void setUpOnce() throws InterruptedException {
//        log.info("Setup once {}", BaseClassTwo.class.getName());
//        Thread.sleep(200);
//    }
//
//    @AfterClass
//    public static void tearDownOnce() throws InterruptedException {
//        log.info("Tear down {}", BaseClassTwo.class.getName());
//        Thread.sleep(200);
//    }

    @Test
    public void testTwo() throws InterruptedException {
        log.info("Test two {}", BaseClassTwo.class.getName());
        Thread.sleep(200);
    }

}
