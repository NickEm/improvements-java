package com.nickem.junit;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

@Slf4j
public class BaseClassOne extends BaseTest {

    @BeforeAll
    public static void setUpOnceB() throws InterruptedException {
        log.info("Setup once {}", BaseClassOne.class.getName());
        Thread.sleep(200);
    }

    @AfterAll
    public static void tearDownOnceB() throws InterruptedException {
        log.info("Tear down {}", BaseClassOne.class.getName());
        Thread.sleep(200);
    }

    @Test
    public void testOne() throws InterruptedException {
        log.info("Test one {}", BaseClassOne.class.getName());
        Thread.sleep(200);
    }


    /*WHy don't we see parent's Before/AfterClass???*/

    /*Because if the name mathes between child <-> parent we don't seee, no match we see */
}
