package com.nickem.junit;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

@Slf4j
public class BaseTest {

    @BeforeAll
    public static void setUpOnce() throws InterruptedException {
        log.info("Setup once {}", BaseTest.class.getName());
        Thread.sleep(200);
    }

    @AfterAll
    public static void tearDownOnce() throws InterruptedException {
        log.info("Tear down {}", BaseTest.class.getName());
        Thread.sleep(200);
    }



}
