package com.nickem.mix;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.awaitility.Awaitility;
import org.awaitility.pollinterval.FibonacciPollInterval;
import org.junit.jupiter.api.Test;

@Slf4j
public class Examples {

    @Test
    public void test() {
        final String filename = "test/photos/image.jpg";
        assertThat(FilenameUtils.getBaseName(filename)).isEqualTo("image");
        assertThat(FilenameUtils.getExtension(filename)).isEqualTo("jpg");
        assertThat(FilenameUtils.getName(filename)).isEqualTo("image.jpg");
        assertThat(FilenameUtils.getPath(filename)).isEqualTo("test/photos/");
    }

    @Test
    public void dummy() throws ExecutionException, InterruptedException {
        Awaitility.await(format("waiting for key %s to be deleted", "ems-test"))
                  .atMost(Duration.ofSeconds(5))
                  .pollInterval(FibonacciPollInterval.fibonacci(12, TimeUnit.MILLISECONDS)) // 144ms
                  .until(() -> {
                      log.info("this");
                      return false;
                  });
    }

    @Test
    public void smth() {
        final Integer maxPlusOne = Integer.MAX_VALUE + 1;
        assertThat(maxPlusOne).isEqualTo(Integer.MIN_VALUE);
    }

}
