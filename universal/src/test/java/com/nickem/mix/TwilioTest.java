package com.nickem.mix;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.Account;
import com.twilio.rest.api.v2010.Account.Status;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class TwilioTest {

    public static final String ACCOUNT_SID = "AC2c9c5ec2e28851259441b378c7e3aebe";
    public static final String AUTH_TOKEN = "";

    @Test
    public void testSuspendSubaccount() {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        final Account account =
                Account.updater("ACd56ce61f5c5618e98b1f281fdb4cec22").setStatus(Status.SUSPENDED).update();
        log.info("Account is {}", account);
    }

}