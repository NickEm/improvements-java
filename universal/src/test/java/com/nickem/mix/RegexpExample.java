package com.nickem.mix;

import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class RegexpExample {

    private static final Pattern PATTERN = Pattern.compile("[\\da-z.-]+");

    /*
    * Tests to
    *
    * ALL - means all the available realms now and any upcoming in the future.
    */
    @Test
    public void test() {
        final String simple = "123ab";
        log.info("{}: {}", simple, PATTERN.matcher(simple).matches());

        final String withDash = "123-ab";
        log.info("{}: {}", withDash, PATTERN.matcher(withDash).matches());

        final String withDots = "123.ab";
        log.info("{}: {}", withDots, PATTERN.matcher(withDots).matches());

        final String random1 = "messaging.chat.react-dashboard-pages";
        log.info("{}: {}", random1, PATTERN.matcher(random1).matches());

        final String random2 = "enable-usage-trigger-reads-to-twilio-counters";
        log.info("{}: {}", random2, PATTERN.matcher(random2).matches());
    }
}
