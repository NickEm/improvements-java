package com.nickem.inheritance;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

@Slf4j
public abstract class BaseTest {

    @BeforeAll
    public static void before() {
        log.info("Prepare");
    }

    @AfterAll
    public static void afterClass() {
        log.info("Clean up");
    }

}
