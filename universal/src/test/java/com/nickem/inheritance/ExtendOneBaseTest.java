package com.nickem.inheritance;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

@Slf4j
public class ExtendOneBaseTest extends BaseTest {

    @Test
    public void test1() {
        log.info("ExtendOne");
    }

}
