```shell
kubectl -n monitoring port-forward pod/prometheus-monitoring-kube-prometheus-prometheus-0 9091:9090
```

```shell
sum(fms_transfer_file_time_seconds_sum{tenant="test_tenant_sftp"})
```

```shell
sum by(size) (fms_transfer_file_time_seconds_sum{tenant="test_tenant_sftp"})
label_values(fms_transfer_file_time_seconds{tenant="test_tenant_sftp"},size)

smt
```

## RED

system_load_average_1m{container="$application"}
system_cpu_count{container="$application"}

#### vault-api
``` 
{__name__="http_server_requests_seconds_count", cluster="k4-amd-oltp-stg-vault-sbx-02-uw2", container="vault-api", data_env="sandbox", deploy_env="vault", endpoint="main", environment="dev-vault-sandbox", exception="HttpMediaTypeNotSupportedException", infra_env="dev", instance="10.17.91.204:8081", job="vault-api/vault-api", method="POST", namespace="vault-api", outcome="CLIENT_ERROR", pod="vault-api-8575969d76-btzv5", prometheus="monitoring/monitoring-kube-prometheus-prometheus", prometheus_replica="prometheus-monitoring-kube-prometheus-prometheus-0", status="415", uri="/tokens", vgs_kube_cluster="k4-amd-oltp-stg-vault-sbx-02-uw2"}
{__name__="http_server_requests_seconds_count", cluster="k4-amd-oltp-stg-vault-sbx-02-uw2", container="vault-api", data_env="sandbox", deploy_env="vault", endpoint="main", environment="dev-vault-sandbox", exception="None", infra_env="dev", instance="10.17.91.204:8081", job="vault-api/vault-api", method="GET", namespace="vault-api", outcome="CLIENT_ERROR", pod="vault-api-8575969d76-btzv5", prometheus="monitoring/monitoring-kube-prometheus-prometheus", prometheus_replica="prometheus-monitoring-kube-prometheus-prometheus-0", status="401", uri="root", vgs_kube_cluster="k4-amd-oltp-stg-vault-sbx-02-uw2"}
```

{container="vault-api", infra_env="dev", data_env="sandbox"}

#### FMS
labels(entrypoint)
label_values(fms_operation_success_total{container="file-management-service"},entrypoint)
label_values(fms_operation_success_total, entrypoint)

### Alertmanager
https://hub.docker.com/r/prom/alertmanager
https://grafana.com/blog/2020/02/25/step-by-step-guide-to-setting-up-prometheus-alertmanager-with-slack-pagerduty-and-gmail/
 k -n file-management apply /Users/mmorozov/git-repos/vgs/verygood.gitops/k3-amd-oltp-stg-vault-sbx-02-uw2/product/protocols/file-management-service/prometheus-rules.yaml


fms_operation_started_total{xB3Traceid="fbceecf07d11f827d35c562ae9e3e59a"} ~ 08:43:10 GMT
fms_operation_finished_total{xB3Traceid="fbceecf07d11f827d35c562ae9e3e59a"} ~ 08:55:00 GMT

### TODO

# HELP kafka_producer_request_rate The number of requests sent per second
# TYPE kafka_producer_request_rate gauge

# HELP kafka_producer_request_total The total number of requests sent
# TYPE kafka_producer_request_total counter

# HELP kafka_producer_topic_record_send_total The total number of records sent for a topic.
# TYPE kafka_producer_topic_record_send_total counter

# HELP kafka_producer_record_error_total The total number of record sends that resulted in errors
# TYPE kafka_producer_record_error_total counter

# HELP kafka_producer_topic_record_error_total The total number of record sends that resulted in errors for a topic
# TYPE kafka_producer_topic_record_error_total counter

# HELP kafka_producer_request_latency_avg The average request latency in ms
# TYPE kafka_producer_request_latency_avg gauge

# HELP kafka_producer_request_latency_max The maximum request latency in ms
# TYPE kafka_producer_request_latency_max gauge
---

# HELP kafka_producer_outgoing_byte_total The total number of outgoing bytes sent to all servers
# TYPE kafka_producer_outgoing_byte_total counter

# HELP kafka_producer_incoming_byte_total The total number of bytes read off all sockets
# TYPE kafka_producer_incoming_byte_total counter

# HELP kafka_producer_topic_byte_total The total number of bytes sent for a topic.
# TYPE kafka_producer_topic_byte_total counter

# HELP kafka_producer_node_incoming_byte_total The total number of incoming bytes
# TYPE kafka_producer_node_incoming_byte_total counter

# HELP kafka_producer_node_outgoing_byte_total The total number of outgoing bytes
# TYPE kafka_producer_node_outgoing_byte_total counter


