package com.nickem;

import static org.assertj.core.api.Assertions.assertThat;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.junit.jupiter.api.Test;

@Slf4j
class MixTest {

    @Test
    public void test() {
        final String filename = "test/photos/image.jpg";
        assertThat(FilenameUtils.getBaseName(filename)).isEqualTo("image");
        assertThat(FilenameUtils.getExtension(filename)).isEqualTo("jpg");
        assertThat(FilenameUtils.getName(filename)).isEqualTo("image.jpg");
        assertThat(FilenameUtils.getPath(filename)).isEqualTo("test/photos/");
        assertThat(FilenameUtils.getPathNoEndSeparator("csv/data/raw/05-with-pan-and-headers.csv"))
            .isEqualTo("csv/data/raw");
        assertThat(FilenameUtils.getPath(FilenameUtils.getPathNoEndSeparator("csv/data/raw/05-with-pan-and-headers.csv")))
            .isEqualTo("csv/data/");
    }

}