package com.nickem;

import com.google.common.collect.Iterables;
import com.nickem.loader.AbstractProxyClient;
import com.nickem.loader.ApplicationContext;
import com.nickem.loader.ApplicationContext.ApplicationContextBuilder;
import com.nickem.loader.DataProvider;
import com.nickem.loader.ForwardProxyClient;
import com.nickem.loader.LoadMode;
import com.nickem.loader.ProxyType;
import com.nickem.loader.ReverseProxyClient;
import com.nickem.loader.converted.CsvFileWriter;
import com.nickem.loader.domain.CreditCard;
import com.nickem.loader.domain.VgsResult;
import com.nickem.util.CommonUtil;
import io.vavr.control.Try;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

@Slf4j
public class Main {

//    --parallel 1 --type REVERSE --tenant tntvek1tyeh --mode FIXED_RATE --rate 1000 --data csv/data/raw/05-with-pan-and-headers.csv
//    --parallel 50 --type REVERSE --tenant tntidpshpuz --mode FIXED_RATE --data csv/data/raw/1K-with-pan-and-headers.csv
//    --parallel 50 --type REVERSE --tenant tntidpshpuz --mode FIXED_RATE --data csv/data/raw/1K-with-pan-and-headers.csv
//    --parallel 30 --type REVERSE --tenant tnte5l9aiyo --mode FIXED_RATE --data csv/data/raw/30K-with-pan-and-headers.csv
//    --parallel 1 --type REVERSE --tenant tntidpshpuz --mode MAX_THROUGHPUT --data csv/data/raw/05-with-pan-and-headers.csv

    public static void main(String[] args) {
        final Main instance = new Main();
        final ApplicationContext context = instance.prepareApplicationContext(args);

        final AbstractProxyClient proxyClient;
        switch (context.getProxyType()) {
            case FORWARD -> proxyClient = new ForwardProxyClient(context.getTenant(), context.getUpstream(),
                context.getUsername(), context.getPassword());
            case REVERSE -> proxyClient = new ReverseProxyClient(context.getTenant());
            default -> throw new IllegalArgumentException("No 'type' for proxy is provided.");
        }

        final DataProvider dataProvider = new DataProvider(context.getLoadFileData());
        switch (context.getMode()) {
            case FIXED_RATE_SINGLE -> instance.throughSingleScheduler(context, proxyClient, dataProvider);
            case FIXED_RATE -> instance.throughScheduler(context, proxyClient, dataProvider);
            case MAX_THROUGHPUT -> instance.throughCardsMax(context, proxyClient, dataProvider);
        }
    }

    public ApplicationContext prepareApplicationContext(String[] args) {
        Options options = new Options();
        options.addOption("type", "type", true, "Type of the loading proxy");
        options.addOption("p", "parallel", true, "How much threads to use while loading");
        options.addOption("t", "tenant", true, "Tenant id");
        options.addOption("u", "username", true, "Username in case of forward-proxy");
        options.addOption("ups", "upstream", true, "Upstream in case of forward-proxy");
        options.addOption("pass", "password", true, "Password in case of forward-proxy");
        options.addOption("m", "mode", true, "Load mode, by default: {MAX_THROUGHPUT}");
        options.addOption("r", "rate", true, "In milliseconds. How often to execute parallel requests count");
        options.addOption("d", "data", true, "Data file from which to perform load");

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("TODO", options);
                System.exit(0);
            }
            final ApplicationContextBuilder builder = ApplicationContext.builder();
            builder.parallelExecutions(Integer.parseInt(cmd.getOptionValue("parallel")));
            builder.proxyType(ProxyType.valueOf(cmd.getOptionValue("type")));
            builder.tenant(cmd.getOptionValue("tenant"));
            builder.upstream(cmd.getOptionValue("upstream"));
            builder.username(cmd.getOptionValue("username"));
            builder.password(cmd.getOptionValue("password"));
            builder.mode(LoadMode.valueOf(cmd.getOptionValue("mode")));
            if (cmd.hasOption("rate")) {
                builder.rate(Integer.parseInt(cmd.getOptionValue("rate")));
            }
            builder.loadFileData(cmd.getOptionValue("data"));
            return builder.build();
        } catch (ParseException e) {
            log.error("Exception while parsing command line", e);
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("TODO", options);
            System.exit(1);
            return null;
        }
    }

    @SneakyThrows
    private void throughSingleScheduler(final ApplicationContext context, final AbstractProxyClient proxyClient,
                                        final DataProvider dataProvider) {
        final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(
            context.getParallelExecutions());
        final long startTime = System.nanoTime();
        final List<CreditCard> tokenizedCards = new ArrayList<>();

        final CountDownLatch finishTheExecution = new CountDownLatch(1);
        final Iterator<String> cardsIterator = dataProvider.getCreditCardsStringified().iterator();
        final Runnable runnable = () -> {
            if (cardsIterator.hasNext()) {
                try {
                    final HttpResponse<String> response = proxyClient.loadCall(cardsIterator.next());
                    tokenizedCards.add(CreditCard.parseCreditCard(VgsResult.parse(response.body()).getData()));
                } catch (Exception e) {
                    log.error("Exception on processing result", e);
                }
            } else {
                finishTheExecution.countDown();
            }
        };
        executorService.scheduleAtFixedRate(runnable, 0, 1000, TimeUnit.MILLISECONDS);

        finishTheExecution.await();
        CsvFileWriter.writeToResultFile(context, tokenizedCards);

        log.info("Execution totally took time: {}", CommonUtil.findTimeLap(startTime, System.nanoTime()));
        executorService.shutdown();
    }

    @SneakyThrows
    private void throughScheduler(final ApplicationContext context, final AbstractProxyClient proxyClient,
                                  final DataProvider dataProvider) {
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        final ExecutorService taskExecutor = Executors.newFixedThreadPool(context.getParallelExecutions());
        final long startTime = System.nanoTime();
        final List<CreditCard> tokenizedCards = new CopyOnWriteArrayList<>();

        final List<String> cards = dataProvider.getCreditCardsStringified();
        final Iterator<List<String>> iterator = Iterables.partition(cards, context.getParallelExecutions()).iterator();

        final CountDownLatch tasksCompleted = new CountDownLatch(cards.size());
        final Runnable runnable = () -> {
            if (iterator.hasNext()) {
                final List<String> partition = iterator.next();
                for (String card : partition) {
                    Runnable task = () -> {
                        try {
                            final HttpResponse<String> response = proxyClient.loadCall(card);
                            tokenizedCards.add(CreditCard.parseCreditCard(VgsResult.parse(response.body()).getData()));
                        } catch (Exception e) {
                            log.error("Exception on processing result [{}]", card);
                            log.error("Stacktrace:", e);
                            tasksCompleted.countDown();
                        } finally {
                            tasksCompleted.countDown();
                        }
                    };
                    taskExecutor.execute(task);
                }
            }
        };

        scheduler.scheduleAtFixedRate(runnable, 0, 1000, TimeUnit.MILLISECONDS);

        tasksCompleted.await();
        taskExecutor.shutdown();
        scheduler.shutdown();
        proxyClient.close();
        CsvFileWriter.writeToResultFile(context, tokenizedCards);

        log.info("Execution totally took time: {}", CommonUtil.findTimeLap(startTime, System.nanoTime()));
    }

    @SneakyThrows
    private void throughCardsMax(final ApplicationContext context, final AbstractProxyClient proxyClient,
                                 final DataProvider dataProvider) {
        final ExecutorService executorService = Executors.newFixedThreadPool(context.getParallelExecutions());
        final long startTime = System.nanoTime();
        try {
            int roundIndex = 1;
            if (context.isInfiniteExecution()) {
                while (true) {
                    roundIndex = executeRound(context, proxyClient, dataProvider, executorService, roundIndex);
                }
            } else {
                executeRound(context, proxyClient, dataProvider, executorService, roundIndex);
            }
        } finally {
            log.info("Execution totally took time: {}", CommonUtil.findTimeLap(startTime, System.nanoTime()));
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        }
    }

    private int executeRound(ApplicationContext context, AbstractProxyClient proxyClient,
                             DataProvider dataProvider, ExecutorService executorService, int roundIndex) {
        final long roundStartTime = System.nanoTime();
        final List<CreditCard> tokenizedCards = new ArrayList<>();
        List<CompletableFuture<Void>> futures = new ArrayList<>();
        final List<String> cards = dataProvider.getCreditCardsStringified();
        for (String card : cards) {
            final Runnable runnable = () -> {
                try {
                    final HttpResponse<String> response = proxyClient.loadCall(card);
                    tokenizedCards.add(CreditCard.parseCreditCard(VgsResult.parse(response.body()).getData()));
                } catch (Exception e) {
                    log.error("Exception happened with card {}", card);
                    log.error("Stacktrace:", e);
                    log.info("Cooldown waiting");
                    Try.of(() -> {
                        Thread.sleep(2000);
                        return null;
                    });
                }
            };
            futures.add(CompletableFuture.runAsync(runnable, executorService));
        }
        final Iterable<List<CompletableFuture<Void>>> partitions = Iterables.partition(futures,
            context.getParallelExecutions());
        int partitionIndex = 1;
        for (List<CompletableFuture<Void>> partition : partitions) {
            final long startPartitionTime = System.nanoTime();
            CompletableFuture.allOf(partition.toArray(new CompletableFuture[0])).join();
            log.info("Execution of partition [{}]: {}", partitionIndex,
                CommonUtil.findTimeLap(startPartitionTime, System.nanoTime()));
            partitionIndex++;
        }
        log.info("Execution of round [{}]: {}", roundIndex++,
            CommonUtil.findTimeLap(roundStartTime, System.nanoTime()));
        CsvFileWriter.writeToResultFile(context, tokenizedCards);
        return roundIndex;
    }

}
