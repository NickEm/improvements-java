package com.nickem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nickem.loader.ReverseProxyClient;
import com.nickem.loader.domain.CreditCard;
import java.util.Map;

public class SingleRequestMain {

    /*
    * curl -v --trace-time "https://tntyjhsui6e.sandbox.verygoodproxy.io/post" \
  -H "Content-type: application/json" \
  -H "iteration_count: 2000000000" \
  -d '{"secret": "4716089049909260"}'
    * */

    public static void main(String[] args) throws JsonProcessingException {
        final ReverseProxyClient client = new ReverseProxyClient("tntyjhsui6e");
        final ObjectMapper mapper = new ObjectMapper();
        final CreditCard card = CreditCard.builder()
            .type("MasterCard")
            .number("5459690795701799")
            .name("Chelsey Spencer V")
            .cvv("723")
            .expiration("07/25")
            .build();
        client.loadCallWithHeaders(mapper.writeValueAsString(card), Map.of("iteration_count", "2000000000"));
    }

}
