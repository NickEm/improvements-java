package com.nickem.loader;

import com.nickem.util.CommonUtil;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ForwardProxyClient extends AbstractProxyClient {

    private final HttpClient client;

    private final URI targetUri;

    @SneakyThrows
    public ForwardProxyClient(String tenant, String upstream, String username, String password) {
        super(new ResultHolder());
        this.targetUri = new URI(upstream + "/post");
        System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
        System.setProperty("jdk.internal.httpclient.disableHostnameVerification", Boolean.TRUE.toString());

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, TRUST_ALL_CERTS, new SecureRandom());

        final String hostname = "%s.sandbox.verygoodproxy.io".formatted(tenant);
        ProxySelector proxySelector = ProxySelector.of(new InetSocketAddress(hostname, 8080));

        Authenticator authenticator = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password.toCharArray());
            }
        };

        this.client = HttpClient.newBuilder()
            .sslContext(sslContext)
            .proxy(proxySelector)
            .authenticator(authenticator)
            .build();
    }

    @SneakyThrows
    public HttpResponse<String> loadCall(final String body) {
        return loadCallWithHeaders(this.targetUri, body, Collections.emptyMap());
    }

    @Override
    public HttpResponse<String> loadCallWithHeaders(String body, Map<String, String> headers) {
        return loadCallWithHeaders(this.targetUri, body, headers);
    }

    @SneakyThrows
    public HttpResponse<String> loadCallWithHeaders(final URI targetUri, final String body, Map<String, String> headers) {
        final long startTime = System.nanoTime();
        HttpResponse<String> response = null;
        final Builder builder = HttpRequest.newBuilder()
            .uri(targetUri)
            .header("Content-type", "application/json")
            .POST(BodyPublishers.ofString(body))
            .version(Version.HTTP_1_1);

        for (Entry<String, String> entry : headers.entrySet()) {
            builder.header(entry.getKey(), entry.getValue());
        }
        try {
            response = client.send(builder.build(), BodyHandlers.ofString());
            processResponse(response);
        } catch (Exception e) {
            log.warn("Issue with sending request", e);
        } finally {
            log.debug("Execution of single call: {}", CommonUtil.findTimeLap(startTime, System.nanoTime()));
        }
        return response;
    }

    private static final TrustManager[] TRUST_ALL_CERTS = new TrustManager[]{
        new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {return null;}
            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
        }
    };

}
