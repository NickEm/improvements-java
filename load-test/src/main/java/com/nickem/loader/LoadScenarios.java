package com.nickem.loader;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;
import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.loader.domain.CreditCard;
import com.nickem.loader.domain.CreditCardRecordMapper;
import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoadScenarios {

//    public static void main(String[] args) throws Exception {
//        final LoadScenarios instance = new LoadScenarios();
//        final int parallelExecutions = 32;
//        final int loadRounds = 1;
//        final List<Tenant> tenants = instance.getTenants(100);
//        instance.scheduledExecutor(parallelExecutions, tenants);
//        instance.parallelExecutor(parallelExecutions, tenants);
//    }

    private void scheduledExecutor(int parallelExecutions, List<Tenant> tenants)
        throws JsonProcessingException, InterruptedException {
        final String dataFile = "csv/data/1K-with-pan-and-headers.csv";
        final ObjectMapper mapper = new ObjectMapper();
        final HttpClient client = HttpClient.newHttpClient();
        final ScheduledExecutorService executorService = Executors.newScheduledThreadPool(parallelExecutions);
        final List<CreditCard> cards = prepareRequestData(dataFile);

        throughTenantsScheduled(tenants, mapper, client, executorService, cards);
    }

    private void parallelExecutor(int parallelExecutions, List<Tenant> tenants)
        throws JsonProcessingException, InterruptedException {
        final String dataFile = "csv/data/0100-with-pan-and-headers.csv";
//        final String dataFile = "csv/data/30K-with-pan-and-headers.csv";
//        final String dataFile = "csv/data/05-with-pan-and-headers.csv";
//        final String dataFile = "csv/data/1K-with-pan-and-headers.csv";
        final ObjectMapper mapper = new ObjectMapper();
        final HttpClient client = HttpClient.newHttpClient();
        final ExecutorService executorService = Executors.newFixedThreadPool(parallelExecutions);
        final List<CreditCard> cards = prepareRequestData(dataFile);

        throughTenantsThenCards(parallelExecutions, tenants, mapper, client, executorService, cards);
    }

    private void throughTenantsScheduled(List<Tenant> tenants, ObjectMapper mapper,
                                         HttpClient client, ScheduledExecutorService executorService, List<CreditCard> cards)
        throws InterruptedException {

        final long startTime = System.nanoTime();
        try {
            for (Tenant i: tenants) {
                final Runnable runnable = () -> loadCall(client, i.getTenantId(), randomCard(mapper, cards));
                executorService.scheduleAtFixedRate(runnable, 0, 1, TimeUnit.SECONDS);
            }
        } finally {
            log.info("Execution totally took time: {}", findTimeLap(startTime, System.nanoTime()));
            if (!executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        }
    }

    private String randomCard(ObjectMapper mapper, List<CreditCard> cards) {
        try {
            return mapper.writeValueAsString(new Random().nextInt(cards.size()));
        } catch (JsonProcessingException e) {
            log.error("Error mapping card", e);
            throw new RuntimeException(e);
        }
    }

    private void throughTenantsThenCards(int parallelExecutions, List<Tenant> tenants, ObjectMapper mapper,
                                         HttpClient client, ExecutorService executorService, List<CreditCard> cards)
        throws JsonProcessingException, InterruptedException {

        final long startTime = System.nanoTime();
        try {
            for (Tenant i: tenants) {
                final long roundStartTime = System.nanoTime();

                List<CompletableFuture<Void>> futures = new ArrayList<>();

                for (CreditCard card : cards) {
                    final String cardString = mapper.writeValueAsString(card);
                    futures.add(
                        CompletableFuture.runAsync(() -> loadCall(client, i.getTenantId(), cardString), executorService));
                }
                final Iterable<List<CompletableFuture<Void>>> partitions =
                    Iterables.partition(futures, parallelExecutions);
                int count = 1;
                for (List<CompletableFuture<Void>> partition : partitions) {
                    final long startPartitionTime = System.nanoTime();
                    CompletableFuture.allOf(partition.toArray(new CompletableFuture[0])).join();
                    log.debug("Execution of partition [{}]: {}", count,
                        findTimeLap(startPartitionTime, System.nanoTime()));
                    count++;
                }
                log.info("Execution of round [{}]: {}", i.getTenantId(), findTimeLap(roundStartTime, System.nanoTime()));
            }
        } finally {
            log.info("Execution totally took time: {}", findTimeLap(startTime, System.nanoTime()));
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        }
    }

    private void throughCardsThenTenants(int parallelExecutions, List<Tenant> tenants, ObjectMapper mapper,
                                                HttpClient client, ExecutorService executorService,
                                                List<CreditCard> cards)
        throws JsonProcessingException, InterruptedException {
        final long startTime = System.nanoTime();
        try {
            for (CreditCard card : cards) {
                final long roundStartTime = System.nanoTime();
                final String cardString = mapper.writeValueAsString(card);
                List<CompletableFuture<Void>> futures = new ArrayList<>();

                for (Tenant i: tenants) {
                    final Runnable runnable = () -> loadCall(client, i.getTenantId(), cardString);
                    futures.add(CompletableFuture.runAsync(runnable, executorService));
                }
                final Iterable<List<CompletableFuture<Void>>> partitions = Iterables.partition(futures,
                    parallelExecutions);
                int count = 1;
                for (List<CompletableFuture<Void>> partition : partitions) {
                    final long startPartitionTime = System.nanoTime();
                    CompletableFuture.allOf(partition.toArray(new CompletableFuture[0])).join();
                    log.debug("Execution of partition [{}]: {}", count,
                        findTimeLap(startPartitionTime, System.nanoTime()));
                    count++;
                }
                log.info("Execution of round [{}]: {}", card, findTimeLap(roundStartTime, System.nanoTime()));
            }
        } finally {
            log.info("Execution totally took time: {}", findTimeLap(startTime, System.nanoTime()));
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        }
    }

    @SneakyThrows
    public void loadCall(final HttpClient client, final String tenant, final String body) {
        final long startTime = System.nanoTime();
        final HttpRequest request = HttpRequest.newBuilder()
            .uri(new URI("https://%s.sandbox.verygoodproxy.io/post".formatted(tenant)))
            .header("Content-type", "application/json")
            .POST(HttpRequest.BodyPublishers.ofString(body))
            .version(HttpClient.Version.HTTP_1_1)
            .build();
        try {
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                log.warn("Response code is {}", response.statusCode());
            }
//            log.info("Body {}", response.body());
        } catch (Exception e) {
            log.warn("Issue with sending request", e);
        } finally {
            log.debug("Execution of single call: {}", findTimeLap(startTime, System.nanoTime()));
        }
    }

    private List<CreditCard> prepareRequestData(String dataFile) {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final File input = new File(classLoader.getResource(dataFile).getPath());
        final CreditCardRecordMapper recordMapper = new CreditCardRecordMapper();
        final CsvParser<CreditCard> csvParser = new CsvParser<>(recordMapper);
        CsvParseResult<CreditCard> result = csvParser.parseFile(input);
        return result.entities();
    }

    private double findTimeLap(final long startTime, final long endTime) {
        final double average = ((double) (endTime - startTime)) / 1_000_000_000;
        BigDecimal bd = BigDecimal.valueOf(average);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    private List<Tenant> getTenants(int limit) {
        final TenantRecordMapper recordMapper = new TenantRecordMapper();
        final CsvParser<Tenant> csvParser = new CsvParser<>(recordMapper);
        final ClassLoader classLoader = this.getClass().getClassLoader();
//        final String inputName = "csv/tenant/tenants-with-http-routes.csv";
        final String inputName = "csv/tenant/tenants-not-existed.csv";
        final File input = new File(classLoader.getResource(inputName).getPath());
        return csvParser.parseFile(input)
            .entities()
            .stream()
            .limit(limit)
            .collect(Collectors.toList());
    }

}
