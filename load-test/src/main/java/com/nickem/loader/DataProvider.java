package com.nickem.loader;

import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.loader.domain.CreditCard;
import com.nickem.loader.domain.CreditCardRecordMapper;
import java.io.InputStream;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataProvider {
    private final static Random RANDOM = new Random();
    private final List<CreditCard> creditCards;
    private final List<String> creditCardsStringified;

    public DataProvider(final String dataFile) {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final InputStream input = classLoader.getResourceAsStream(dataFile);
        final CreditCardRecordMapper recordMapper = new CreditCardRecordMapper();
        final CsvParser<CreditCard> csvParser = new CsvParser<>(recordMapper);
        CsvParseResult<CreditCard> result = csvParser.parseFile(input);
        creditCards = result.entities();
        creditCardsStringified = creditCards.stream()
            .map(CreditCard::stringifyCreditCard)
            .collect(Collectors.toList());

    }

    public List<CreditCard> getCreditCards() {
        return creditCards;
    }

    public List<String> getCreditCardsStringified() {
        return creditCardsStringified;
    }

    public String randomCardStringified() {
        return creditCardsStringified.get(RANDOM.nextInt(creditCards.size()));
    }

    public CreditCard randomCard() {
        return creditCards.get(RANDOM.nextInt(creditCards.size()));
    }

}
