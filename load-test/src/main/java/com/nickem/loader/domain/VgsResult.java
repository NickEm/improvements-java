package com.nickem.loader.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Data
@Slf4j
@JsonIgnoreProperties(ignoreUnknown = true)
public class VgsResult {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    String data;

    public static VgsResult parse(String stringifiedJson) {
        try {
            return MAPPER.readValue(stringifiedJson, VgsResult.class);
        } catch (JsonProcessingException e) {
            log.error("Error mapping card", e);
            throw new RuntimeException(e);
        }
    }

}
