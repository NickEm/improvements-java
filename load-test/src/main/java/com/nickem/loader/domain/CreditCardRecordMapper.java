package com.nickem.loader.domain;

import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;

public class CreditCardRecordMapper implements CsvRecordMapper<CreditCard> {

    private static final String TYPE = "Type";
    private static final String NUMBER = "Number";
    private static final String NAME = "Name";
    private static final String CVV = "CVV";
    private static final String EXPIRATION = "Expiration";

    public CreditCard map(String[] fields, CsvColumnMapper mapper) {
        return CreditCard.builder()
            .type(fields[mapper.getIndexByFieldName(TYPE)])
            .number(fields[mapper.getIndexByFieldName(NUMBER)])
            .name(fields[mapper.getIndexByFieldName(NAME)])
            .cvv(fields[mapper.getIndexByFieldName(CVV)])
            .expiration(fields[mapper.getIndexByFieldName(EXPIRATION)])
            .build();
    }

}
