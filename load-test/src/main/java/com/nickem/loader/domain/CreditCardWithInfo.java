package com.nickem.loader.domain;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
public class CreditCardWithInfo extends CreditCard {

    public CreditCardWithInfo(CreditCard card) {
        super(card.getType(), card.getNumber(), card.getName(), card.getCvv(), card.getExpiration());
    }

    @CsvBindByPosition(position = 5)
    @CsvBindByName(column = "Info")
    String info;



}
