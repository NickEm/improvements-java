package com.nickem.loader.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@AllArgsConstructor
@Data
@SuperBuilder(toBuilder = true)
public class CreditCard implements Comparable<CreditCard> {

    @CsvIgnore
    private final static ObjectMapper MAPPER = new ObjectMapper();

    @CsvBindByPosition(position = 0)
    @CsvBindByName(column = "Type")
    String type;
    @CsvBindByPosition(position = 1)
    @CsvBindByName(column = "Number")
    String number;
    @CsvBindByPosition(position = 2)
    @CsvBindByName(column = "Name")
    String name;
    @CsvBindByPosition(position = 3)
    @CsvBindByName(column = "CVV")
    String cvv;
    @CsvBindByPosition(position = 4)
    @CsvBindByName(column = "Expiration")
    String expiration;


    public static String stringifyCreditCard(CreditCard card) {
        try {
            return MAPPER.writeValueAsString(card);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error converting object to string", e);
        }
    }

    public static CreditCard parseCreditCard(String stringifiedCreditCard) {
        try {
            return MAPPER.readValue(stringifiedCreditCard, CreditCard.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error converting string to object", e);
        }
    }

    @Override
    public int compareTo(CreditCard o) {
        return this.getNumber().compareTo(o.getNumber());
    }
}
