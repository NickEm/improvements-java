package com.nickem.loader;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class ApplicationContext {

    ProxyType proxyType;
    String tenant;
    String upstream;
    String username;
    String password;
    int parallelExecutions;
    LoadMode mode;
    int rate;
    String loadFileData;
    boolean infiniteExecution;

}
