package com.nickem.loader;

import com.nickem.util.CommonUtil;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpClient.Version;
import java.net.http.HttpRequest;
import java.net.http.HttpRequest.BodyPublishers;
import java.net.http.HttpRequest.Builder;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ReverseProxyClient extends AbstractProxyClient {

    private final String tenant;
    private final HttpClient client;

    @SneakyThrows
    public ReverseProxyClient(String tenant) {
        super(new ResultHolder());
        this.tenant = tenant;
        this.client = HttpClient.newHttpClient();
    }

    @SneakyThrows
    public HttpResponse<String> loadCall(final String body) {
        return loadCallWithHeaders(body, Collections.emptyMap());
    }

    @SneakyThrows
    @Override
    public HttpResponse<String> loadCallWithHeaders(String body, Map<String, String> headers) {
        final long startTime = System.nanoTime();
        HttpResponse<String> response = null;

        final Builder builder = HttpRequest.newBuilder()
            .uri(new URI("https://%s.sandbox.verygoodproxy.io/post".formatted(tenant)))
            .header("Content-type", "application/json")
            .POST(BodyPublishers.ofString(body))
            .version(Version.HTTP_1_1);

        for (Entry<String, String> entry : headers.entrySet()) {
            builder.header(entry.getKey(), entry.getValue());
        }
        try {
            response = client.send(builder.build(), BodyHandlers.ofString());
            processResponse(response);
        } catch (Exception e) {
            log.warn("Issue with sending request", e);
        } finally {
            log.debug("Execution of single call took: {} sec", CommonUtil.findTimeLap(startTime, System.nanoTime()));
        }
        return response;
    }

}
