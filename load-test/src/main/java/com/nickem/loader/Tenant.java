package com.nickem.loader;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Tenant {

    String tenantId;
    String name;
    String username;
    String password;

}
