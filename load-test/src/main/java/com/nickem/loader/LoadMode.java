package com.nickem.loader;

public enum LoadMode {
    MAX_THROUGHPUT,
    FIXED_RATE_SINGLE,
    FIXED_RATE
}
