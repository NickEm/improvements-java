package com.nickem.loader.converted;

import com.nickem.loader.ApplicationContext;
import com.nickem.loader.domain.CreditCard;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;

@Slf4j
@UtilityClass
public class CsvFileWriter {

    @SneakyThrows
    public static void writeToResultFile(final ApplicationContext context, List<CreditCard> tokenizedCards) {
        final Path filePath = getFileToWriteTokenizedData(context);
        writeToResultFile(filePath, tokenizedCards);
    }

    @SneakyThrows
    public static <T> void writeToResultFile(Path filePath, List<T> cards) {
        try (Writer writer = new FileWriter(filePath.toFile())) {
            StatefulBeanToCsv<T> beanToCsv = new StatefulBeanToCsvBuilder<T>(writer)
                .withQuotechar(Character.MIN_VALUE)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();
            beanToCsv.write(cards);
        }
    }

    private static Path getFileToWriteTokenizedData(ApplicationContext context) {
        final String pathToDataFolder = FilenameUtils.getPathNoEndSeparator(
            FilenameUtils.getPathNoEndSeparator(context.getLoadFileData()));
        final String fileName = FilenameUtils.getName(context.getLoadFileData());
        Path filePath = Paths.get(
            "load-test/src/main/resources/%s/tokenized/%s/%s".formatted(pathToDataFolder, context.getTenant(),
                fileName));
        Path parentDir = filePath.getParent();
        try {
            if (!Files.exists(parentDir)) {
                Files.createDirectories(parentDir);
            }
        } catch (Exception e) {
            log.error("Exception while creating the file structure");
        }
        return filePath;
    }

}
