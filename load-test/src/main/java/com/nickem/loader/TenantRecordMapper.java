package com.nickem.loader;

import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;

public class TenantRecordMapper implements CsvRecordMapper<Tenant> {

    private static final String TENANT_ID = "tenant_id";
    private static final String NAME = "name";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    @Override
    public Tenant map(String[] fields, CsvColumnMapper mapper) {
        return Tenant.builder()
            .tenantId(fields[mapper.getIndexByFieldName(TENANT_ID)])
            .name(fields[mapper.getIndexByFieldName(NAME)])
            .username(fields[mapper.getIndexByFieldName(USERNAME)])
            .password(fields[mapper.getIndexByFieldName(PASSWORD)])
            .build();
    }

}
