package com.nickem.loader;

import java.io.Closeable;
import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractProxyClient implements Closeable {

    protected final ResultHolder resultHolder;

    protected AbstractProxyClient(ResultHolder resultHolder) {
        this.resultHolder = resultHolder;
    }

    protected void processResponse(HttpResponse<String> response) {
        if (response.statusCode() != 200) {
            log.warn("Response code is {}", response.statusCode());
            log.warn("headers {}", response.headers());
            log.warn("body {}", response.body());
        }
        resultHolder.processStatus(response.statusCode());
    }

    public abstract HttpResponse<String> loadCall(final String body);

    public abstract HttpResponse<String> loadCallWithHeaders(final String body, Map<String, String> headers);

    @Override
    public void close() throws IOException {
        resultHolder.close();
    }

}
