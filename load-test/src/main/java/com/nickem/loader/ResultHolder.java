package com.nickem.loader;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ResultHolder implements Closeable {

    private final AtomicInteger _2XX = new AtomicInteger(0);
    private final AtomicInteger _4XX = new AtomicInteger(0);
    private final AtomicInteger _5XX = new AtomicInteger(0);
    private final AtomicInteger _OTHER = new AtomicInteger(0);

    private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    public ResultHolder() {
        executor.scheduleAtFixedRate(this::printStats, 10, 10, TimeUnit.SECONDS);
    }

    public void printStats() {
        final String message = """
            
            2XX:    {}
            4XX:    {}
            5XX:    {}
            OTHER:  {}
            """;
        log.info(message, _2XX.get(), _4XX.get(), _5XX.get(), _OTHER.get());
    }

    public void processStatus(final int status) {
        if (status >= 200 && status < 300) {
            _2XX.getAndIncrement();
        } else if (status >= 400 && status < 500) {
            _4XX.getAndIncrement();
        } else if (status >= 500) {
            _5XX.getAndIncrement();
        } else {
            _OTHER.getAndIncrement();
        }
    }

    @Override
    public void close() throws IOException {
        executor.shutdown();
    }
}
