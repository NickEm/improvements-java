package com.nickem.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CommonUtil {

    public double findTimeLap(final long startTime, final long endTime) {
        final double average = ((double) (endTime - startTime)) / 1_000_000_000;
        BigDecimal bd = BigDecimal.valueOf(average);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
