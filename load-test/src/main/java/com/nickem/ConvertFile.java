package com.nickem;

import com.nickem.loader.DataProvider;
import com.nickem.loader.converted.CsvFileWriter;
import com.nickem.loader.domain.CreditCardWithInfo;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class ConvertFile {

    public static void main(String[] args) {
        final int count = 1024 * 1;

        String loadFileData = "csv/data/raw/with-headers/30K-with-pan-and-headers.csv";
        final DataProvider dataProvider = new DataProvider(loadFileData);

        final String fileName = "%s-%s.%s".formatted(FilenameUtils.getBaseName(loadFileData), "info-" + count,
            FilenameUtils.getExtension(loadFileData));
        final String pathToDataFolder = FilenameUtils.getPathNoEndSeparator(loadFileData);
        Path destinationFilePath = Paths.get(
            "load-test/src/main/resources/%s/%s".formatted(pathToDataFolder, fileName));

        List<CreditCardWithInfo> creditCards = dataProvider.getCreditCards().stream()
                .map(e -> new CreditCardWithInfo(e).toBuilder()
                    .info(RandomStringUtils.randomAlphabetic(count))
                    .build())
                    .collect(Collectors.toList());

        CsvFileWriter.writeToResultFile(destinationFilePath, creditCards);
    }

}
