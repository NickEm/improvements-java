package com.nickem.artillery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.nickem.artillery.domain.json.ArtilleryAggregate;
import com.nickem.artillery.domain.json.ArtilleryCounters;
import com.nickem.artillery.domain.json.ArtilleryResult;
import com.nickem.artillery.domain.json.ArtillerySummaryResponseTime;
import com.nickem.artillery.domain.yaml.ArtilleryLoadConfig;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import software.amazon.awssdk.services.s3.model.S3Object;

@Slf4j
public class ArtilleryAwsResultParser extends ArtilleryResultParser {

    /*Fix error502Count=0, error504Count=0 and show overall count*/
    public static final ObjectMapper JSON_OBJECT_MAPPER = new ObjectMapper();
    public static final ObjectMapper YAML_OBJECT_MAPPER = new ObjectMapper(new YAMLFactory());

    public static void main(String[] args) throws IOException {
        var awsRegion = "us-west-2";
        var awsBucket = "vault-dev-01-audits-01-artifact-19k6160zpr44j";
        var benchmarkId = "65";
        var awsKeyPrefix = "load-test/barometer/test-result/2024/01/26/%s/".formatted(benchmarkId);

//        var benchmarkId = "103";
//        var awsKeyPrefix = "load-test/barometer/test-result/2023/10/26/%s/".formatted(benchmarkId);

        final ArtilleryAwsResultParser instance = new ArtilleryAwsResultParser();
        final S3LoadClient s3LoadClient = new S3LoadClient(awsRegion, awsBucket, awsKeyPrefix);
        List<S3Object> jsonResultFiles = s3LoadClient.getJsonResultFiles();

        S3Object yamlConfigFile = s3LoadClient.getYamlConfigFile();

        Map<String, ArtilleryResult> results = new HashMap<>();
        for (S3Object s3Object : jsonResultFiles) {
            final ArtilleryResult content = JSON_OBJECT_MAPPER.readValue(
                s3LoadClient.getS3ObjectAsBytes(s3Object.key()), ArtilleryResult.class);
            results.put(s3Object.key(), content);
        }

        final ArtilleryLoadConfig artilleryConfig = YAML_OBJECT_MAPPER.readValue(
            s3LoadClient.getS3ObjectAsBytes(yamlConfigFile.key()), ArtilleryLoadConfig.class);

        final int rate = retrieveRate(artilleryConfig);

        if (results.isEmpty()) {
            log.error("No files were found to process");
        } else {
            final ArtilleryResult finalResult = instance.prepareFinalResult(results);
            log.info("Started at {}, Finished at {}, Result size {}",
                LocalDateTime.ofInstant(Instant.ofEpochMilli(finalResult.getAggregate().getFirstCounterAt()), ZoneOffset.UTC),
                LocalDateTime.ofInstant(Instant.ofEpochMilli(finalResult.getAggregate().getLastCounterAt()), ZoneOffset.UTC),
                results.size());

            log.info("Projected load is [{} RPS]", rate * results.size());
            log.info("{}", finalResult);
            log.info("{}", prepareTableResultLine(benchmarkId, rate * results.size(),
                artilleryConfig.getConfig().getPayload().getPath(), finalResult));
        }
    }

    private static String prepareTableResultLine(String runId, int load, String payloadFile, ArtilleryResult finalResult) {
        String loadDataFile = FilenameUtils.getName(payloadFile);
        final ArtilleryAggregate aggregate = finalResult.getAggregate();
        final ArtilleryCounters counters = aggregate.getCounters();
        String redLink = "[RED link](https://grafana.apps.verygood.systems/d/T22lePHmz/http-proxy-red?orgId=1&var-datasource=access-logger-dev&var-DataEnv=All&var-InfraEnv=All&var-ProxyType=All&var-GroupByInterval=1m&var-RuntimeEnv=All&var-TenantId=All&from=%s&to=%s)"
            .formatted(aggregate.getFirstCounterAt(), aggregate.getLastCounterAt());
        String jvmLink = "[JVM link](https://grafana.apps.verygood.systems/d/pWMbCaaGk/vault-application-metrics-telegraf?orgId=1&var-telegraf=vgs-dev-devops-metrics&var-DataEnv=sandbox&var-ApplicationName=reverse-proxy&var-GroupByInterval=1m&var-Host=All&from=%s&to=%s)"
            .formatted(aggregate.getFirstCounterAt(), aggregate.getLastCounterAt());

        int xx5 = counters.getError502Count() + counters.getError503Count() + counters.getError504Count();
        final ArtillerySummaryResponseTime summary = aggregate.getSummaries().getResponseTime();
        return """
            | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s | %s |
            """.formatted(runId, load, loadDataFile, "<todo>", summary.getMedian(), summary.getP90(), summary.getP95(), summary.getP99(), xx5, redLink, jvmLink);
    }


    private static int retrieveRate(final ArtilleryLoadConfig artilleryConfig){
        return Integer.parseInt(artilleryConfig.getConfig().getPhases().stream()
            .filter(e -> StringUtils.equals("peak load", e.getName()))
            .findFirst()
            .orElseThrow(() -> new NoSuchElementException("Rate isn't found un the config section."))
            .getArrivalRate());
    }

}
