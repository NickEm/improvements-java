package com.nickem.artillery;

import com.nickem.artillery.domain.json.ArtilleryAggregate;
import com.nickem.artillery.domain.json.ArtilleryCounters;
import com.nickem.artillery.domain.json.ArtilleryResult;
import com.nickem.artillery.domain.json.ArtillerySummary;
import com.nickem.artillery.domain.json.ArtillerySummaryResponseTime;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Map.Entry;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

@Slf4j
public abstract class ArtilleryResultParser {

    protected ArtilleryResult prepareFinalResult(Map<String, ArtilleryResult> results) {
        final ArtilleryResult finalResult = new ArtilleryResult(
            new ArtilleryAggregate(
                new ArtillerySummary(
                    new ArtillerySummaryResponseTime(
                        0d,
                        0d,
                        0d,
                        0d,
                        0d,
                        0d,
                        0d,
                        0d,
                        0d,
                        0d
                    )
                ),
                new ArtilleryCounters(),
                Long.MAX_VALUE,
                0
            )
        );

        final ArtilleryAggregate aggregate = finalResult.getAggregate();

        final ArtilleryCounters finalCounter = finalResult.getAggregate().getCounters();
        final ArtillerySummaryResponseTime finalTime = finalResult.getAggregate().getSummaries()
            .getResponseTime();
        Pair<String, Double> min = Pair.of("", 0D);
        Pair<String, Double> max = Pair.of("", 0D);
        for (Entry<String, ArtilleryResult> entry : results.entrySet()) {
            ArtilleryResult current = entry.getValue();
            final ArtilleryAggregate currentAggregate = current.getAggregate();
            final ArtilleryCounters currentCounter = currentAggregate.getCounters();
            finalCounter.setErrorTimeoutCount(
                finalCounter.getErrorTimeoutCount() + currentCounter.getErrorTimeoutCount());
            finalCounter.setError502Count(finalCounter.getError502Count() + currentCounter.getError502Count());
            finalCounter.setError503Count(finalCounter.getError503Count() + currentCounter.getError503Count());
            finalCounter.setError504Count(finalCounter.getError504Count() + currentCounter.getError504Count());

            final ArtillerySummaryResponseTime currentTime = currentAggregate.getSummaries()
                .getResponseTime();
            finalTime.setMin(finalTime.getMin() + currentTime.getMin());
            finalTime.setMax(finalTime.getMax() + currentTime.getMax());
            finalTime.setCount(finalTime.getCount() + currentTime.getCount());
            finalTime.setP50(finalTime.getP50() + currentTime.getP50());
            finalTime.setMedian(finalTime.getMedian() + currentTime.getMedian());
            finalTime.setP75(finalTime.getP75() + currentTime.getP75());
            finalTime.setP90(finalTime.getP90() + currentTime.getP90());
            finalTime.setP95(finalTime.getP95() + currentTime.getP95());
            finalTime.setP99(finalTime.getP99() + currentTime.getP99());
            finalTime.setP999(finalTime.getP999() + currentTime.getP999());

            if (max.getValue() <= currentTime.getMax()) {
                max = Pair.of(entry.getKey(), currentTime.getMax());
            }
            if (min.getValue() >= currentTime.getMin()) {
                min = Pair.of(entry.getKey(), currentTime.getMin());
            }
            if (currentAggregate.getFirstCounterAt() <= aggregate.getFirstCounterAt()) {
                aggregate.setFirstCounterAt(currentAggregate.getFirstCounterAt());
            }
            if (currentAggregate.getLastCounterAt() >= aggregate.getLastCounterAt()) {
                aggregate.setLastCounterAt(currentAggregate.getLastCounterAt());
            }
        }

        finalTime.setMin(roundDouble(min.getRight()));
        finalTime.setMax(roundDouble(max.getRight()));
        finalTime.setCount(roundDouble(finalTime.getCount() / results.size()));
        finalTime.setP50(roundDouble(finalTime.getP50() / results.size()));
        finalTime.setMedian(roundDouble(finalTime.getMedian() / results.size()));
        finalTime.setP75(roundDouble(finalTime.getP75() / results.size()));
        finalTime.setP90(roundDouble(finalTime.getP90() / results.size()));
        finalTime.setP95(roundDouble(finalTime.getP95() / results.size()));
        finalTime.setP99(roundDouble(finalTime.getP99() / results.size()));
        finalTime.setP999(roundDouble(finalTime.getP999() / results.size()));

        log.info("{}", max);

        return finalResult;
    }

    protected double roundDouble(Double number) {
        BigDecimal bd = BigDecimal.valueOf(number);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
