package com.nickem.artillery;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.autoscaling.AutoScalingClient;
import software.amazon.awssdk.services.autoscaling.model.AutoScalingGroup;
import software.amazon.awssdk.services.autoscaling.model.DescribeAutoScalingGroupsRequest;
import software.amazon.awssdk.services.autoscaling.model.DescribeAutoScalingGroupsResponse;
import software.amazon.awssdk.services.autoscaling.model.TagDescription;
import software.amazon.awssdk.services.cloudformation.CloudFormationClient;
import software.amazon.awssdk.services.cloudformation.model.CloudFormationException;
import software.amazon.awssdk.services.cloudformation.model.DescribeStackResourcesRequest;
import software.amazon.awssdk.services.cloudformation.model.DescribeStackResourcesResponse;
import software.amazon.awssdk.services.cloudformation.model.ListStacksRequest;
import software.amazon.awssdk.services.cloudformation.model.ListStacksResponse;
import software.amazon.awssdk.services.cloudformation.model.StackResource;
import software.amazon.awssdk.services.cloudformation.model.StackStatus;
import software.amazon.awssdk.services.cloudformation.model.StackSummary;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;
import software.amazon.awssdk.services.ec2.model.Ec2Exception;
import software.amazon.awssdk.services.ec2.model.Reservation;
import software.amazon.awssdk.services.ec2.model.Instance;
import software.amazon.awssdk.services.ec2.model.Tag;

@Slf4j
public class CloudFormationCleaner {

// AWS_ACCESS_KEY_ID=ASIAXVIFR6WSGIFMHMO3;AWS_SECRET_ACCESS_KEY=duQ84uARPRxp8ZdYWiR5Nsg3Sb0ngJf0Z6FkZU8M;AWS_SESSION_TOKEN=IQoJb3JpZ2luX2VjEHsaCXVzLWVhc3QtMSJGMEQCIFuj9dtF7y7jm34+NweDpC4T+ygAyl7dwEjO9her43BIAiAdGJ+cAtyaMenXezQlZ8co5LWbLnvCANQuCeoM90PZzSq4AwjU//////////8BEAQaDDUyNjY4MjAyNzQyOCIM99/JJtW0pkKlqwxPKowDRli6NneFo3LcRhKLWymf+olWrONrXiGpNA/AKhyllYoSmXzWwNCAi7j1p1UdNRLUDF2yd4xyez2+qpiZv5jIdjvrnB67AugM4U2pEP1wgIOTvmdS6j6GozEsEsQ4e1DFzWMW5TzjnqShWl+vdkr0o0TMS61kFEaoU889mP/IXnEWhkUGO1BDAkSZekOxxc1G3CgQYx7mUIdEoa/3ulzF/Yyg1+8XBR3f0VjZ4WlEs13nUR3vnHq7W2cKTz6fnEzpeB3vIRb+p5AUR0hK7LLGN6i/ADP7HTw5RPlJSeXRBPNZTN4TNgJ8oGE95hTae2ywF9WQxUerxwvD37Qw4sarX2wkm7PJTRyeo7Po8CN5beg1EdbN4SGRlkQOrvN+aNLw/+KBRbFzD/pp+YbE2bKwTZ3qG1RRQZc2FZGSB0r6BCrDip2GMf00WgRZML7vF4nbCfTxPm4oLcq6Uhp+dURNmS9PSH3hE5xxcZfES5t4jcRIMAAMF79WXxFmxuYBH4B+GHxT1sB9BRVh7EznMJGL6LEGOqcBAlJQ4BCTB76N4QIce58o0AulZJJG4XLQGkrcM4TLShmsnDb3QYmY8jmHm6H6R5QqK/frPXnwu2R7J3Eu908NH7BsuqhGfy58q8CKHtHGROk05ZxwVZoPmy2R28wk2v4Fq9Rp24OSS1fRLvP3ITSPrBBV8nansbBjgxoFq1SsFxU0Oik/6zkYOJIjyOS2QN3+5mXXUjAfrXEfWxPMGYz/T/bhWNOCct0=
//    DEV
// AWS_ACCESS_KEY_ID=ASIA43HUBFCEZNO3NPVY;AWS_SECRET_ACCESS_KEY=C3Idq/Q9cDJ4MWQOAPnbkekLhxLB1YsIKlmBJjig;AWS_SESSION_TOKEN=IQoJb3JpZ2luX2VjEB8aCXVzLWVhc3QtMSJHMEUCIGYdThTZiNNOV2JtOMYbAw8qVkZCz2bgvdUUqLymcQhqAiEAh8TEfyMESs5ox/irr+V4IwoHXofKxjiZPaRmUtBNwO4qrwMIeBAFGgw4ODMxMjc1NjAzMjkiDKljH9hlgp21Cd3s5iqMA0hINnRbY/Kj9QhE6DVgduteLvM1zLNfEPsdpiobMl/41rZvVEY212esqwt6fthu5aLTVNe/I3FEViKiSmVChXoOCabAd630v7y+CN/eljH+SevTfd3Yfc7BV6WDMjr5Glqbjj/5Opn3zKqu/45+npNa6c1DBM+OieVLlnimKiYZwMaYq+EmJEx78LwybW/V5dPiPAsiH5k9ix5TBD158+0acHUb9aWvRTqhp7YJGqdTaLWtGQzeYC2a64990NGGonF8lnD2wuJcS2/k0d9BSElvKwuWQOwaVvE5GoCXTVqgt95lg3WZN5CSDVuDwB+vQGs4kyFgOTABWvftzRknrJd03Hblk2hQsMuJQetWBuenmq9cEgZCTmfKcMFGmfAz2piLqW6mzXLktYfoFPRQGEbGUsUUx9VwiOI9SexF4xWngbviyzDe0rWjdvnIxZcxyK74Wg8fdyAUxgf35wfhEeo47SQUGlOi0DOpTmuXMYbOXdwFaTE3BBFYh+5/uD8dsl9pIWyKpn9ExtByezDN6NOxBjqmAQJfDPvWNIwNQISFdU9/6Gv2X+u2SdkXzKjR7pPHnjS9vNvp73p2NDO25IFxViJ1Gjvz4ZHLLysvheQyPOtdKF5z/DiDBMqkKmRn4VPYOcKp052sJUMZdWAvUaWszpZODGU6+ZG8r5pffQkzAamaE1KaWbrNe70emBtFJ0sKlsVA2UO74RpygULRpBuZxWEsenZfyPCBfcc/CJ0QKT3QKrVdrpe6+OI=
    public static void main(String[] args) {
        CloudFormationCleaner instance = new CloudFormationCleaner();

//        final String awsRegion = "us-west-2";
        final String awsRegion = "us-east-1";
//        final String awsRegion = "eu-central-1";
        final Region region = Region.of(awsRegion);

        CloudFormationClient cfClient = CloudFormationClient.builder()
            .region(region)
            .build();

        AutoScalingClient asClient = AutoScalingClient.builder()
            .region(region)
            .build();

        Ec2Client ec2 = Ec2Client.builder()
            .region(region)
            .build();

        instance.listEc2(ec2, "term");

//        instance.listCloudFormationWithUnusedAsg(instance, cfClient, asClient);
    }

    public void listCloudFormationWithUnusedAsg(CloudFormationCleaner instance, CloudFormationClient cfClient,
                                  AutoScalingClient asClient) {
        //        final List<String> stackNames = List.of("vgs-prod-live-terminator3-iso-10-08");
//        Predicate<StackSummary> stackPredicate = (stack) -> stackNames.contains(stack.stackName());
        Predicate<StackSummary> stackPredicate = (stack) ->
            stack.stackName().contains("term")
                && StackStatus.DELETE_COMPLETE != stack.stackStatus()
                && StackStatus.DELETE_IN_PROGRESS != stack.stackStatus();
        final List<StackSummary> stacks = instance.listStacksByFilter(cfClient, stackPredicate);
        int countOfInstances = 0;
        int countOfStacks = 0;
        int countOfUsedInstances = 0;
        int countOfUsedStacks = 0;
        String dataEnvKey = "vg:data_env";
//        String dataEnvValue = "sandbox";
        String dataEnvValue = "live";
//        String dataEnvValue = "live-eu-1";
//        String dataEnvValue = "";
        for (StackSummary stack : stacks) {
            DescribeStackResourcesRequest request = DescribeStackResourcesRequest.builder()
                .stackName(stack.stackId())
                .build();

            DescribeStackResourcesResponse response = cfClient.describeStackResources(request);

            for (StackResource resource : response.stackResources()) {
                if ("AWS::AutoScaling::AutoScalingGroup".equals(resource.resourceType())) {
                    DescribeAutoScalingGroupsRequest asRequest = DescribeAutoScalingGroupsRequest.builder()
                        .autoScalingGroupNames(resource.physicalResourceId())
                        .build();
                    DescribeAutoScalingGroupsResponse asResponse = asClient.describeAutoScalingGroups(asRequest);
                    final List<AutoScalingGroup> autoScalingGroups = asResponse.autoScalingGroups();
                    if (CollectionUtils.isNotEmpty(autoScalingGroups)) {
                        if (autoScalingGroups.size() == 1) {
                            final AutoScalingGroup autoScalingGroup = autoScalingGroups.get(0);
                            if (CollectionUtils.isEmpty(autoScalingGroup.targetGroupARNs())) {
                                if (StringUtils.isNotBlank(dataEnvValue)) {
                                    for (TagDescription tag : autoScalingGroup.tags()) {
                                        if (dataEnvKey.equals(tag.key()) && dataEnvValue.equals(tag.value())) {
                                            log.warn("ASG has no targets! StackId [{}]", stack.stackName());
                                            countOfInstances += autoScalingGroup.instances().size();
                                            countOfStacks += 1;
                                        }
                                    }
                                } else {
                                    log.warn("ASG has no targets! StackId [{}]", stack.stackName());
                                    countOfInstances += autoScalingGroup.instances().size();
                                    countOfStacks += 1;
                                }
                            } else {
                                log.warn("USED [{} : {}]", stack.stackName(), autoScalingGroup.instances().size());
                                countOfUsedStacks +=1;
                                for (TagDescription tag : autoScalingGroup.tags()) {
                                    if (dataEnvKey.equals(tag.key()) && dataEnvValue.equals(tag.value())) {
                                        countOfUsedInstances += autoScalingGroup.instances().size();
                                    }
                                }
                            }
                        } else {
                            log.warn("ASG is of size [{}]", autoScalingGroups.size());
                        }
                    } else {
                        log.warn("ASG is empty!");
                    }
                } else {
                    log.debug("Resource is of type [{}]", resource.resourceType());
                }
            }
        }

        log.info("Count of unused instances: {}", countOfInstances);
        log.info("Count of unused stacks: {}", countOfStacks);
        log.info("Count of used instances: {}", countOfUsedInstances);
        log.info("Count of used stacks: {}", countOfUsedStacks);
    }

    public void listEc2(Ec2Client ec2, String term) {
        int countOfTerms = 0;
        int countOfUsed = 0;
        Map<String, List<String>> stackToInstances = new HashMap<>();
        try {
            DescribeInstancesRequest request = DescribeInstancesRequest.builder().build();
            boolean done = false;

            while (!done) {
                DescribeInstancesResponse response = ec2.describeInstances(request);

                boolean instanceToCheck;
                boolean instanceIsUsed;
                for (Reservation reservation : response.reservations()) {
                    for (Instance instance : reservation.instances()) {
                        instanceToCheck = false;
                        instanceIsUsed = false;
                        for (Tag tag : instance.tags()) {
                            if ("Name".equals(tag.key()) && StringUtils.containsIgnoreCase(tag.value(), term)) {
                                instanceToCheck = true;
                                countOfTerms++;
                            }
                        }
                        if (instanceToCheck) {
                            for (Tag tag: instance.tags()) {
                                if ("aws:autoscaling:groupName".equals(tag.key())) {
                                    countOfUsed++;
                                    instanceIsUsed = true;
                                }
                                if ("aws:cloudformation:stack-name".equals(tag.key())) {
                                    stackToInstances.compute(tag.value(), addInstanceFn(instance));
                                }
                            }
                            if (!instanceIsUsed) {
                                log.info("Unused instance: {}", instance.instanceId());
                            }
                        }
                    }
                }

                if (response.nextToken() == null) {
                    done = true;
                } else {
                    request = request.toBuilder().nextToken(response.nextToken()).build();
                }
            }
            log.info("Number on listed instances: {}", countOfTerms);
            log.info("Number on listed instances with ASG: {}", countOfUsed);

            Map<String, Integer> grouped = new HashMap<>();
            for (Entry<String, List<String>> entry : stackToInstances.entrySet()) {
                grouped.put(entry.getKey(), entry.getValue().size());
            }
            log.info("Grouped {}", grouped);
            log.info("{}", stackToInstances);
        } catch (Ec2Exception e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
    }

    private static BiFunction<String, List<String>, List<String>> addInstanceFn(Instance instance) {
        return (key, value) -> {
            if (value == null) {
                final List<String> instances = new ArrayList<>();
                instances.add(instance.instanceId());
                return instances;
            } else {
                  value.add(instance.instanceId());
                  return value;
            }
        };
    }

    public List<StackSummary> listStacksByFilter(CloudFormationClient cfClient, Predicate<StackSummary> predicate) {
        List<StackSummary> result = new ArrayList<>();
        try {
            boolean done = false;
            String nextToken = null;
            while (!done) {
                ListStacksRequest request = ListStacksRequest.builder()
                    .nextToken(nextToken)
                    .build();
                ListStacksResponse response = cfClient.listStacks(request);

                for (StackSummary stack : response.stackSummaries()) {
                    if (predicate.evaluate(stack)) {
                        result.add(stack);
                    }

                }

                nextToken = response.nextToken();
                if (nextToken == null) {
                    done = true;
                }
            }
        } catch (CloudFormationException e) {
            System.err.println("Error listing stacks: " + e.awsErrorDetails().errorMessage());
            System.exit(1);
        }

        return result.stream()
            .sorted(Comparator.comparing(StackSummary::creationTime))
            .collect(Collectors.toList());
    }

}
