package com.nickem.artillery;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nickem.artillery.domain.json.ArtilleryResult;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ArtilleryLocalResultParser extends ArtilleryResultParser{

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static void main(String[] args) throws IOException {
        final ArtilleryLocalResultParser instance = new ArtilleryLocalResultParser();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
        final List<String> resultFiles = List.of(
            "artillery/result/2023-10-12/20/tntgbrutkak-reverse_proxy_cc-21-barometer-rp-tntgbrutkak-fjcgp-results.json"
        );
        Map<String, ArtilleryResult> results = new HashMap<>();
        for (String resultFile : resultFiles) {
            final InputStream resourceAsStream = classLoader.getResourceAsStream(resultFile);
            results.put(resultFile, OBJECT_MAPPER.readValue(resourceAsStream, ArtilleryResult.class));
        }

        final ArtilleryResult finalResult = instance.prepareFinalResult(results);
        log.info("{}", finalResult);
    }

}
