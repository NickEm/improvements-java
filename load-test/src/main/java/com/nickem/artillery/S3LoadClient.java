package com.nickem.artillery;

import java.util.List;
import java.util.NoSuchElementException;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;

public class S3LoadClient {

    private final S3Client s3Client;
    private final String bucket;
    private final String basePath;
    private final ListObjectsV2Response listObjResponse;

    public S3LoadClient(String awsRegion, String bucket, String basePath) {
        this.s3Client = S3Client.builder()
            .region(Region.of(awsRegion))
            .build();
        this.bucket = bucket;
        this.basePath = basePath;
        this.listObjResponse = s3Client.listObjectsV2(
            ListObjectsV2Request.builder()
                .bucket(bucket)
                .prefix(basePath)
                .build()
        );
    }

    public List<S3Object> getJsonResultFiles() {
        return listObjResponse.contents().stream()
            .filter(s3Object -> s3Object.key().endsWith(".json"))
            .toList();
    }

    public S3Object getYamlConfigFile() {
        return listObjResponse.contents().stream()
            .filter(s3Object -> s3Object.key().endsWith(".yaml"))
            .findFirst()
            .orElseThrow(() -> new NoSuchElementException("Config <yaml> file isn't found."));
    }

    public byte[] getS3ObjectAsBytes(String s3Key) {
        final GetObjectRequest objectRequest = GetObjectRequest.builder()
            .bucket(bucket)
            .key(s3Key)
            .build();
        ResponseBytes<GetObjectResponse> getObjectResponseBytes =
            s3Client.getObject(objectRequest, ResponseTransformer.toBytes());
        return getObjectResponseBytes.asByteArray();
    }

}
