package com.nickem.artillery.domain.yaml;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtilleryConfig {

    List<ArtilleryPhase> phases;
    ArtilleryPayload payload;

}
