package com.nickem.artillery.domain.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtilleryCounters {

    @JsonProperty("errors.ETIMEDOUT")
    int errorTimeoutCount;

    @JsonProperty("errors.ECONNRESET")
    int errorResetCount;

    @JsonProperty("http.codes.502")
    int error502Count;

    @JsonProperty("http.codes.503")
    int error503Count;

    @JsonProperty("http.codes.504")
    int error504Count;

}
