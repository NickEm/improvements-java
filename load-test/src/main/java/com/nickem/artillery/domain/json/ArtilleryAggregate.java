package com.nickem.artillery.domain.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtilleryAggregate {

    ArtillerySummary summaries;
    ArtilleryCounters counters;
    long firstCounterAt;
    long lastCounterAt;

}
