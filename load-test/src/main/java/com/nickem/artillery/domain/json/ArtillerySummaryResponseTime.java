package com.nickem.artillery.domain.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtillerySummaryResponseTime {

    private Double min;
    private Double max;
    private Double count;
    private Double p50;
    private Double median;
    private Double p75;
    private Double p90;
    private Double p95;
    private Double p99;
    private Double p999;

}
