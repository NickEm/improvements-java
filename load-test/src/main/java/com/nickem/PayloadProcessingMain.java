package com.nickem;

import com.google.common.collect.Sets;
import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.loader.domain.CreditCard;
import com.nickem.loader.domain.CreditCardRecordMapper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PayloadProcessingMain {

    public static void main(String[] args)
        throws IOException, CsvRequiredFieldEmptyException, CsvDataTypeMismatchException {
        final PayloadProcessingMain instance = new PayloadProcessingMain();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
//        final Set<CreditCard> creditCards = entities.stream().limit(30_000).collect(Collectors.toCollection(TreeSet::new));
        final List<CreditCard> rawCards = instance.readResourceToPayload(classLoader,
            "csv/data/raw/with-headers/30K-with-pan-and-headers.csv");
        final List<CreditCard> tokenizedCards = instance.readResourceToPayload(classLoader, "csv/data/tokenized/tntrsw7hxmz/30K-with-pan-and-headers.csv");
        final Set<String> difference = instance.findDifference(instance.getNames(rawCards),
            instance.getNames(tokenizedCards));

        final List<CreditCard> creditCards = rawCards.stream()
            .filter(e -> difference.contains(e.getName()))
            .toList();


//        log.info("Unique cards names [{}]", creditCards.size());
//        log.info("Unique cards numbers [{}]", creditCards.stream().map(CreditCard::getNumber).collect(Collectors.toSet()).size());

        Path filePath = Paths.get("load-test/src/main/resources/csv/data/raw/fullfill-batch.csv");

        try (Writer writer = new FileWriter(filePath.toFile())) {
            StatefulBeanToCsv<CreditCard> beanToCsv = new StatefulBeanToCsvBuilder<CreditCard>(writer)
                .withQuotechar(Character.MIN_VALUE)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();
            beanToCsv.write(creditCards.iterator());
        }
    }

    private List<CreditCard> readResourceToPayload(final ClassLoader classLoader, final String resource) {
        final InputStream inputRaw = classLoader.getResourceAsStream(resource);
        final CreditCardRecordMapper recordMapper = new CreditCardRecordMapper();
        final CsvParser<CreditCard> csvParser = new CsvParser<>(recordMapper);
        CsvParseResult<CreditCard> result = csvParser.parseFile(inputRaw);
        return result.entities();
    }

    private Set<String> findDifference(Set<String> source, Set<String> destination) {
        return Sets.difference(source, destination);
    }

    private Set<String> getNames(List<CreditCard> cards){
        return cards.stream().map(CreditCard::getName).collect(Collectors.toSet());
    }

}
