#!/usr/bin/env bash

set -x -e

java \
  ${ADDITIONAL_JVM_OPTIONS} \
  -jar ${SERVICE_JAR:-/app/load-test.jar} \
  --parallel ${PARALLEL_EXECUTIONS:-16} \
  --type ${PROXY_TYPE:-FORWARD} \
  --tenant ${TENANT_ID} \
  --username ${USERNAME} \
  --password ${PASSWORD} \
  --mode ${MODE:-MAX_THROUGHPUT}
