package com.nickem;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PatchBumpVersion {

    private static final String VERSION_REGEXP = "(<version>)(2)\\.(7)\\.([0-9]+)(-SNAPSHOT</version>)";
    private static final Pattern VERSION_PATTERN = Pattern.compile(VERSION_REGEXP);

    public static void main(String[] args) throws IOException {
        /*TODO: This case works good only when there only single entrance of matched version*/
        PatchBumpVersion instance = new PatchBumpVersion();
        final List<String> mvnArguments = List.of("mvn", "clean", "install", "-DskipTests", "-Dmaven.javadoc.skip=true");

        final String libraryPath = "/Users/mmorozov/git-repos/vgs/vault-proxy-common";
        final Path libraryPomPath = Paths.get(libraryPath + "/pom.xml");
        String sourceContent = new String(Files.readAllBytes(libraryPomPath));
        String newVersion = instance.bumpVersion(sourceContent);
        if (newVersion != null) {
            Files.write(libraryPomPath, sourceContent.replaceFirst(VERSION_REGEXP, newVersion).getBytes());
        }
        ExternalApplicationExecutionService.nativeCall(libraryPath, mvnArguments);

        final String targetPath = "/Users/mmorozov/git-repos/vgs/vault";
        final Path targetPomPath = Paths.get(targetPath + "/pom.xml");
        String destinationTargetContent = new String(Files.readAllBytes(targetPomPath));
        if (newVersion != null) {
            Files.write(targetPomPath, destinationTargetContent.replaceFirst(VERSION_REGEXP, newVersion).getBytes());
        }
        ExternalApplicationExecutionService.nativeCall(targetPath, mvnArguments);
    }

    private String bumpVersion(String sourceContent) {
        Matcher matcher = VERSION_PATTERN.matcher(sourceContent);
        if (matcher.find()) {
            int majorVersion = Integer.parseInt(matcher.group(2));
            int minorVersion = Integer.parseInt(matcher.group(3));
            int patchVersion = Integer.parseInt(matcher.group(4)) + 1; // Increment patch version

            return matcher.group(1) + majorVersion + "." + minorVersion + "." + patchVersion + matcher.group(5);
        } else {
            log.warn("No version match was found!");
            return null;
        }
    }

}
