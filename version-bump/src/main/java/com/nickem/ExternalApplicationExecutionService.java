package com.nickem;

import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@UtilityClass
public class ExternalApplicationExecutionService {

    public String nativeCall(final String directoryPath, final List<String> commands) {
        log.info("Executing command {} ...", commands);
        final ProcessBuilder pb = new ProcessBuilder(commands);
        try {
            if (StringUtils.isNotBlank(directoryPath)) {
                pb.directory(new File(directoryPath));
            }
            final Process process = pb.start();
            final InputStream is = process.getInputStream();

            byte[] data = ByteStreams.toByteArray(is);
            String response = new String(data);

            log.info("External application call response: [{}]", response);
            return response;
        } catch (final IOException e) {
            log.error("Error running native commands: {}", commands, e);
            /*TODO: Fix by throwing corresponding exception*/
            throw new RuntimeException(e);
        }
    }

    public String nativeCall(final List<String> commands) {
        return nativeCall("", commands);
    }
}
