package com.nickem;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.domain.CreditCard;
import com.nickem.domain.ListOfCreditCards;
import com.nickem.mapper.CreditCardRecordMapper;
import com.opencsv.exceptions.CsvException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;

@Slf4j
public class CsvProcessor {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static void main(String[] args) throws IOException, CsvException {
        String outputFileExtension = "json";
        CsvProcessor instance = new CsvProcessor();
        final String inputFileName = "0.5K-with-pan-and-headers.csv";
        final String inputFileFolder = "csv/input/";
        final ClassLoader classLoader = instance.getClass().getClassLoader();
        final File input = new File(classLoader.getResource(inputFileFolder + inputFileName).getPath());
        final String baseName = FilenameUtils.getBaseName(inputFileName);
        final CreditCardRecordMapper recordMapper = new CreditCardRecordMapper();
        final CsvParser<CreditCard> csvParser = new CsvParser<>(recordMapper);
        CsvParseResult<CreditCard> result = csvParser.parseFile(input);

        String outputFile = baseName + "." + outputFileExtension;
        final FileOutputStream outputStream = new FileOutputStream(outputFile);

        OBJECT_MAPPER.writeValue(outputStream, new ListOfCreditCards(result.entities()));
    }

}
