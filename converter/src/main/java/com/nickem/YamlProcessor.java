package com.nickem;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class YamlProcessor {

    private static final Logger log = LoggerFactory.getLogger(YamlProcessor.class);

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper(new YAMLFactory());
    public static final TypeReference<Map<String, String>> RESULT_TYPE = new TypeReference<>() {};

    public static void main(String[] args) throws Exception {
        new YamlProcessor().process();
    }

    private void process() throws Exception {
        final var path = Objects.requireNonNull(getClass().getClassLoader().getResource("configuration.yaml")).getPath();
        final Map<String, String> config = OBJECT_MAPPER.readValue(new FileInputStream(path), RESULT_TYPE);
        log.info("Config: {}", config);
    }
}
