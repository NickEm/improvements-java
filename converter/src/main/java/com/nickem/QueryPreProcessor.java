package com.nickem;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class QueryPreProcessor {

    public static void main(String[] args) throws IOException, CsvException {
        final QueryPreProcessor instance = new QueryPreProcessor();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
        final String inputName = "csv/athena/terminator/2023-02-seso-labor-traces-5xx-exist-in-athena.csv";
        final File input = new File(classLoader.getResource(inputName).getPath());

        final FileReader filereader = new FileReader(input);
        final CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        final CSVReader csvReader = new CSVReaderBuilder(filereader)
            .withCSVParser(parser)
            .build();
        List<String[]> allLines = csvReader.readAll();
        boolean isForHoneyComb = true;
        String result = isForHoneyComb ? instance.honeyCombQuery(allLines) : instance.athenaQuery(allLines);
        System.out.println(result);
    }

    private String honeyCombQuery(List<String[]> allLines) {
        String template = "%s,";
        StringBuilder result = new StringBuilder("trace.trace_id in ");
        for (String[] line : allLines) {
            final String l = template.formatted(line[0]);
            result.append(l);
        }
        return result.toString();
    }

    private String athenaQuery(List<String[]> allLines) {
        String template = "b3_trace_id LIKE '%s%s' OR \n";
        StringBuilder result = new StringBuilder();
        for (String[] line : allLines) {
            final String l = template.formatted(line[0], "%");
            result.append(l);
        }
        return result.toString();
    }

}
