package com.nickem;

import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.domain.BitwardenEntry;
import com.nickem.domain.LastPassEntry;
import com.nickem.mapper.LastPassRecordMapper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class CsvToCsvConverter {

    public static void main(String[] args)
        throws IOException, CsvRequiredFieldEmptyException, CsvDataTypeMismatchException {
        final ClassLoader classLoader = CsvToCsvConverter.class.getClassLoader();
        final InputStream input = classLoader.getResourceAsStream("csv/lp.csv");
        final LastPassRecordMapper recordMapper = new LastPassRecordMapper();
        final CsvParser<LastPassEntry> csvParser = new CsvParser<>(recordMapper);
        CsvParseResult<LastPassEntry> result = csvParser.parseFile(input);

        final List<BitwardenEntry> resultEntities = result.entities().stream()
            .map(e -> BitwardenEntry.builder()
                .login_uri(e.getUrl())
                .login_username(e.getUsername())
                .login_password(e.getPassword())
                .name(e.getName())
                .folder(e.getGrouping())
                .favorite(e.getFav())
                .build())
            .collect(Collectors.toList());

        Path filePath = Paths.get("/Users/mmorozov/git-repos/own/improvements-java/converter/bitwarden.csv");

        try (Writer writer = new FileWriter(filePath.toFile())) {
            StatefulBeanToCsv<BitwardenEntry> beanToCsv = new StatefulBeanToCsvBuilder<BitwardenEntry>(writer)
                .withQuotechar(Character.MIN_VALUE)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();

            beanToCsv.write(resultEntities);
        }
    }

}
