package com.nickem;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.nickem.domain.BetaFeature;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonProcessor {

    private static final Logger log = LoggerFactory.getLogger(JsonProcessor.class);

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
     static {
         OBJECT_MAPPER.registerModule(new JavaTimeModule());
         OBJECT_MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
         OBJECT_MAPPER.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false);
         OBJECT_MAPPER.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
     }

    public static final TypeReference<List<BetaFeature>> BETA_LIST_TYPE_REF = new TypeReference<>() {};

    public static void main(String[] args) throws IOException {
        new JsonProcessor().processBetas();
    }

    public void processBetas() throws IOException {
        processBetas("bf-new.json", "bf-new-sorted.json");
    }

    private void processBetas(final String inputFile, final String outputFile) throws IOException {
        log.debug("Reading list of betas [{}]...", inputFile);
        List<BetaFeature> oldBetas = OBJECT_MAPPER.readValue(new FileInputStream(inputFile), BETA_LIST_TYPE_REF);
        List<BetaFeature> sorted = oldBetas.stream()
                                           .map(this::adjustEntity)
                                            .sorted(Comparator.comparing(BetaFeature::getKey))
                                            .collect(Collectors.toList());
        log.debug("Size of betas [{}]: {}", inputFile, oldBetas.size());
        OBJECT_MAPPER.writeValue(new FileOutputStream(outputFile), sorted);
        log.debug("Processing of [{}] is finished.", inputFile);
    }

    private BetaFeature adjustEntity(final BetaFeature beta) {
        final OffsetDateTime now = OffsetDateTime.of(2021, 2, 10, 0, 0, 0, 0, ZoneOffset.UTC);
        beta.setDateCreated(now);
        beta.setDateUpdated(now);
        return beta;
    }
}
