package com.nickem;

import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.domain.AthenaTerminatorEntry;
import com.nickem.mapper.AthenaTerminatorEntryMapper;
import java.io.File;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class TerminatorProcessor {

    public static void main(String[] args) {
        final AthenaTerminatorEntryMapper recordMapper = new AthenaTerminatorEntryMapper();
        final CsvParser<AthenaTerminatorEntry> csvParser = new CsvParser<>(recordMapper);

        final TerminatorProcessor instance = new TerminatorProcessor();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
//        final String inputName = "csv/athena/terminator/2023-02-seso-labor-200-0.csv";
//        final String inputName = "csv/athena/terminator/2023-02-seso-labor-502-0.csv";
        final String inputName = "csv/athena/terminator/2023-02-seso-labor-5xx-14-15.csv";
        final File input = new File(classLoader.getResource(inputName).getPath());
        final CsvParseResult<AthenaTerminatorEntry> result = csvParser.parseFile(input);

//        boolean isForHoneyComb = false;
        boolean isForHoneyComb = true;
        final String delimiter = isForHoneyComb ? "," : "','";
//        final String delimiter = "";
        final List<AthenaTerminatorEntry> entities = result.entities();
        final String traces = result.entities().stream()
            .map(e -> StringUtils.substringBefore(e.getB3TraceId(), "-"))
            .filter(e -> !e.equals("noop"))
//            .map(e -> "message LIKE '%" + e +"%' OR \n")
            .limit(500)
            .collect(Collectors.joining(delimiter));

//        System.out.println(traces);
        System.out.println("trace.trace_id in %s".formatted(traces));
//        System.out.println("trace.trace_id in %s".formatted(traces));
    }

}
