package com.nickem;

import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.domain.AthenaVaultEntry;
import com.nickem.mapper.AthenaVaultEntryMapper;
import java.io.File;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class AthenaProcessor {

    public static void main(String[] args) {
        final AthenaVaultEntryMapper recordMapper = new AthenaVaultEntryMapper();
        final CsvParser<AthenaVaultEntry> csvParser = new CsvParser<>(recordMapper);

        final AthenaProcessor instance = new AthenaProcessor();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
//        final String inputName = "csv/athena/starttime-vault-preferences-1000-milicore-2023-01.csv";
//        final String inputName = "csv/athena/startup/starttime-atlas-horizon-3-core-2023-02.csv";
//        final String inputName = "csv/athena/startup/starttime-atlas-horizon-2-core-2023-02.csv";
        final String inputName = "csv/athena/startup/starttime-vault-management-1000-milicore-2023-01.csv";
//        final String inputName = "csv/athena/starttime-vault-api-1500-milicore-2023-01.csv";
//        final String inputName = "csv/athena/starttime-forward-proxy-2-core-2023-01.csv";
//        final String inputName = "csv/athena/starttime-forward-proxy-2500milicore-2022-12.csv";
//        final String inputName = "csv/athena/starttime-forward-proxy-3-core-2023-01.csv";
//        final String inputName = "csv/athena/starttime-2023-01-03-2-core-reverse-proxy.csv";
//        final String inputName = "csv/athena/starttime-2022-12-20-knox-1-core.csv";
//        final String inputName = "csv/athena/starttime-2023-01-reverse-proxy.csv";
        final File input = new File(classLoader.getResource(inputName).getPath());
        final CsvParseResult<AthenaVaultEntry> result = csvParser.parseFile(input);
        int count = 0;
        double totalAppStartTime = 0d;
        double totalJvmStartTime = 0d;
        for (AthenaVaultEntry entity : result.entities()) {
            final double appStartTime = Double.parseDouble(StringUtils.substringBetween(entity.getMessage(), "Application in ",
                "seconds "));
            final double jvmStartTime = Double.parseDouble(StringUtils.substringBetween(entity.getMessage(), "(JVM running for ",
                ")"));
            totalAppStartTime+=appStartTime;
            totalJvmStartTime+=jvmStartTime;
            count++;
        }
        log.info("Average APP start time: {}", totalAppStartTime / count);
        log.info("Average JVM start time: {}", totalJvmStartTime / count);
    }

    /* atlas-horizon with 1 core */
    /* Started SpringBootCamelApplication in 65 seconds (JVM running for 90) */

    /* atlas-horizon with 2 core */
    /* Started SpringBootCamelApplication in 21 seconds (JVM running for 32) */

    /* atlas-horizon with 3 core */
    /* Started SpringBootCamelApplication in 23 seconds (JVM running for 33) */

    /* vault-management with 1 core */
    /* Started HttpApplication in 88 seconds (JVM running for 115) */

    /* vault-preference with 1 core */
    /* Started HttpApplication in 62.7 seconds (JVM running for 78) */

    /* vault-api with 1.5*/
    /* Started HttpApplication in 42 seconds (JVM running for 58) */

    /* vault-api with 2*/
    /* Started HttpApplication in 33.7 seconds (JVM running for 44) */

    /*2 core*/
    /*2023-01-03-reverse-proxy 54,5 | 76*/


    /*2,5 core*/
    /*2022-11-forward-proxy 53,5 73*/
    /*2022-12-forward-proxy 52,5 73,5*/
    /*2022-11-reverse-proxy 53 72,5*/
    /*2022-12-reverse-proxy 52 72*/


}
