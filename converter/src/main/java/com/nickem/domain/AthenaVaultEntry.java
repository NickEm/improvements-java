package com.nickem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Value
@Builder
public class AthenaVaultEntry {

    String timestamp;
//    String infraEnv;
//    String dataEnvironment;
    String message;

}
