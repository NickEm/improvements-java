package com.nickem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.SortedSet;

public class BetaFeature {

    @JsonProperty
    private int autoId;

    @JsonProperty
    private String key;

    @JsonProperty
    private String featureFlag;

    @JsonProperty
    private Long accountFlag;

    @JsonProperty
    private Long countryFlag;

    @JsonProperty
    private boolean excludeEmployees;

    @JsonProperty
    private boolean enabled;

    @JsonProperty
    private OffsetDateTime dateCreated;

    @JsonProperty
    private OffsetDateTime dateUpdated;

    private transient SortedSet<String> enabledRealms;

    private BetaFeature() {
        super();
    }

    public BetaFeature(final String key) {
        this(key, null, null, null, false);
    }

    public BetaFeature(final String key,
                       final String featureFlag,
                       final Long accountFlag,
                       final Long countryFlag,
                       final boolean excludeEmployees) {
        this(key, 0, featureFlag, accountFlag, countryFlag, excludeEmployees, false, null, null);
    }

    public BetaFeature(final String key,
                final int autoId,
                final String featureFlag,
                final Long accountFlag,
                final Long countryFlag,
                final boolean excludeEmployees,
                final boolean enabled,
                final OffsetDateTime dateCreated,
                final OffsetDateTime dateUpdated) {
        super();
        this.key = key;
        this.autoId = autoId;
        this.featureFlag = featureFlag;
        this.accountFlag = accountFlag;
        this.countryFlag = countryFlag;
        this.excludeEmployees = excludeEmployees;
        this.enabled = enabled;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }

    public String getKey() {
        return key;
    }


    public int getAutoId() {
        return autoId;
    }

    public void setAutoId(final int autoId) {
        this.autoId = autoId;
    }

    public String getFeatureFlag() {
        return featureFlag;
    }

    public void setFeatureFlag(final String featureFlag) {
        this.featureFlag = featureFlag;
    }

    public Long getAccountFlag() {
        return accountFlag;
    }

    public void setAccountFlag(final Long accountFlag) {
        this.accountFlag = accountFlag;
    }

    public Long getCountryFlag() {
        return countryFlag;
    }

    public void setCountryFlag(final Long countryFlag) {
        this.countryFlag = countryFlag;
    }

    public boolean isExcludeEmployees() {
        return excludeEmployees;
    }

    public void setExcludeEmployees(final boolean excludeEmployees) {
        this.excludeEmployees = excludeEmployees;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    public SortedSet<String> getEnabledRealms() {
        return enabledRealms;
    }

    public void setEnabledRealms(final SortedSet<String> enabledRealms) {
        this.enabledRealms = enabledRealms;
    }

    public OffsetDateTime getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(final OffsetDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public OffsetDateTime getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(final OffsetDateTime dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BetaFeature)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        final BetaFeature that = (BetaFeature) o;
        return autoId == that.autoId &&
               excludeEmployees == that.excludeEmployees &&
               enabled == that.enabled &&
               Objects.equals(key, that.key) &&
               Objects.equals(featureFlag, that.featureFlag) &&
               Objects.equals(accountFlag, that.accountFlag) &&
               Objects.equals(countryFlag, that.countryFlag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), key, autoId, featureFlag, accountFlag, countryFlag, excludeEmployees,
                            enabled);
    }

    @Override
    public String toString() {
        return "BetaFeature{" +
               "key='" + key + '\'' +
               ", autoId=" + autoId +
               ", featureFlag='" + featureFlag + '\'' +
               ", accountFlag=" + accountFlag +
               ", countryFlag=" + countryFlag +
               ", excludeEmployees=" + excludeEmployees +
               ", enabled=" + enabled +
               "} " + super.toString();
    }
}
