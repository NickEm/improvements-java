package com.nickem.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Slf4j
public class CreditCard {

    private final static ObjectMapper MAPPER = new ObjectMapper();

    String type;
    String number;
    String name;
    String cvv;
    String expiration;

    public static String stringifyCreditCard(CreditCard card) {
        try {
            return MAPPER.writeValueAsString(card);
        } catch (JsonProcessingException e) {
            log.error("Error mapping card", e);
            throw new RuntimeException(e);
        }
    }

    public static CreditCard parseCreditCard(String stringifiedCreditCard) {
        try {
            return MAPPER.readValue(stringifiedCreditCard, CreditCard.class);
        } catch (JsonProcessingException e) {
            log.error("Error mapping card", e);
            throw new RuntimeException(e);
        }
    }

}
