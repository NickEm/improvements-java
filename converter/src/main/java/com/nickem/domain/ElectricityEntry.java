package com.nickem.domain;

import java.time.LocalDate;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ElectricityEntry {

    LocalDate daytime;
    int dayValue;
    int nightValue;

}
