package com.nickem.domain;

import com.nickem.domain.LingualeoPage.Word;
import java.util.List;
import lombok.Data;

@Data
public class LingualeoPage {

    public String apiVersion;

    public List<PageData> data;

    @Data
    public static class Word {
        public String wordValue;
        public String combinedTranslation;
        public String picture;
    }

    @Data
    public static class PageData {
        public int groupCount;
        public String groupName;
        public List<Word> words;
    }

}

