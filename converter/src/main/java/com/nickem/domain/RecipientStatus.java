package com.nickem.domain;

public enum RecipientStatus {
    ACTIVE,
    INACTIVE
}
