package com.nickem.domain;

import java.util.List;
import lombok.Value;

@Value
public class ListOfCreditCards {

    List<CreditCard> cards;

}
