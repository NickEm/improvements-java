package com.nickem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Slf4j
public class LastPassEntry {

    String url;
    String username;
    String password;
    String totp;
    String extra;
    String name;
    String grouping;
    String fav;

}
