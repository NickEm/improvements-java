package com.nickem.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class AthenaTerminatorEntry {

    String timestamp;
    String tenant;
    String b3TraceId;

}
