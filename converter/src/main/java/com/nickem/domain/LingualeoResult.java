package com.nickem.domain;

import com.nickem.domain.LingualeoPage.Word;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LingualeoResult {

    public List<Word> data;

}
