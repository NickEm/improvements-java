package com.nickem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class BitwardenEntry {

    String folder;
    String favorite;
    String type;

    String name;
    String notes;
    String fields;
    String reprompt;
    String login_uri;
    String login_username;
    String login_password;
    String login_totp;

}
