package com.nickem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opencsv.bean.CsvBindByName;
import java.io.Serializable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.jackson.Jacksonized;

@NoArgsConstructor
@Data
@Jacksonized
@SuperBuilder(toBuilder = true)
public class UserRequest implements Serializable {


    @CsvBindByName(column = "BOTNAME")
    @JsonProperty("botName")
    private String botName;

    @CsvBindByName(column = "RECIPIENT")
    @JsonProperty("recipient")
    private String recipient;

    @CsvBindByName(column = "CHATID")
    @JsonProperty("chatId")
    private Long chatId;

    @CsvBindByName(column = "LANGUAGE")
    @JsonProperty("language")
    private String language;

    @CsvBindByName(column = "ACTIONSTATE")
    @JsonProperty("actionState")
    private String actionState;

    @CsvBindByName(column = "STATUS")
    @JsonProperty("status")
    private RecipientStatus status;

}
