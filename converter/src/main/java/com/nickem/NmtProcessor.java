package com.nickem;

import com.nickem.nmt.legacy.MemoryRecord;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NmtProcessor {

    public static final Pattern TOTAL_PATTERN = Pattern.compile(
        "([^\\(]+): reserved=(\\d+)KB, committed=(\\d+)KB");
    public static final Pattern CATEGORY_PATTERN = Pattern.compile(
        "(-\\s+)([^\\(]+) \\(reserved=(\\d+)KB, committed=(\\d+)KB\\)");

    public static void main(String[] args) throws Exception {
        final NmtProcessor instance = new NmtProcessor();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
        final String inputName = "nmt/minute-snapshot-13-30/nmt-2023-05-11-13-30-00.txt";

        final InputStream stream = classLoader.getResource(inputName).openStream();
        final String input = new String(stream.readAllBytes(), StandardCharsets.UTF_8);

        final String[] lines = input.split(System.lineSeparator());
        final Optional<MemoryRecord> memoryRecord = Arrays.stream(lines)
            .filter(e -> TOTAL_PATTERN.matcher(e).find())
            .findFirst()
            .flatMap(instance::parseTotal);

        final List<MemoryRecord> memoryCategories = Arrays.stream(input.split(System.lineSeparator()))
            .filter(e -> CATEGORY_PATTERN.matcher(e).find())
            .map(instance::parseCategory)
            .filter(Optional::isPresent)
            .map(Optional::get)
            .toList();
        log.info("{}", memoryRecord.orElse(null));
        log.info("{}", memoryCategories);
    }

    public Optional<MemoryRecord> parseTotal(String line) {
        Matcher matcher = TOTAL_PATTERN.matcher(line);
        if (matcher.find()) {
            String name = matcher.group(1).trim();
            int reservedMemoryKb = Integer.parseInt(matcher.group(2));
            int committedMemoryKb = Integer.parseInt(matcher.group(3));
            return Optional.of(new MemoryRecord(name, reservedMemoryKb, committedMemoryKb));
        } else {
            log.warn("NVM line [{}] couldn't be parsed", line);
            return Optional.empty();
        }
    }

    public Optional<MemoryRecord> parseCategory(String line) {
        Matcher matcher = CATEGORY_PATTERN.matcher(line);
        if (matcher.find()) {
            String name = matcher.group(2).trim();
            int reservedMemoryKb = Integer.parseInt(matcher.group(3));
            int committedMemoryKb = Integer.parseInt(matcher.group(4));
            return Optional.of(new MemoryRecord(name, reservedMemoryKb, committedMemoryKb));
        } else {
            log.warn("NVM line [{}] couldn't be parsed", line);
            return Optional.empty();
        }
    }

}
