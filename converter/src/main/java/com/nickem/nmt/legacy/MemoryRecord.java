package com.nickem.nmt.legacy;

public record MemoryRecord(String name, int reservedKb, int committedKb) {

}
