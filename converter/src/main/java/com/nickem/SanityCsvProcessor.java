package com.nickem;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class SanityCsvProcessor {

    public static void main(String[] args) throws IOException, CsvException {
        final SanityCsvProcessor instance = new SanityCsvProcessor();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
        final String inputName = "csv/athena/larky-incident-requests-breakdown-typed.csv";
        final File input = new File(classLoader.getResource(inputName).getPath());

        final FileReader filereader = new FileReader(input);
        final CSVParser parser = new CSVParserBuilder().withSeparator(',').build();
        final CSVReader csvReader = new CSVReaderBuilder(filereader)
            .withCSVParser(parser)
            .build();
        List<String[]> allLines = csvReader.readAll();

        final File output = new File(classLoader.getResource("csv/athena/larky-incident-requests-breakdown-typed-sanitized.csv").getPath());
        CSVWriter writer = new CSVWriter(new FileWriter(output));

        for (String[] line : allLines) {
            final String[] sanitize = instance.sanitize(line);
            writer.writeNext(sanitize);
        }
        writer.flush();
        writer.close();
    }

    private String[] sanitize(final String[] input) {
        final String[] sanitized = new String[input.length];
        for (int i = 0; i < input.length; i++) {
            sanitized[i] = StringUtils.trim(input[i]);
        }

        return sanitized;
    }


}
