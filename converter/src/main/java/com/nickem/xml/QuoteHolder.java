package com.nickem.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JacksonXmlRootElement(localName = "quotes")
public class QuoteHolder {

    @JacksonXmlProperty(localName = "owner")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Owner> owners;
}

