package com.nickem.xml;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/*
 * https://stackoverflow.com/questions/54872538/deserialize-flat-array-in-xml-by-jackson-to-list-of-pojo
 * https://stackify.com/java-xml-jackson/
 *
 */
@Slf4j
public class QuoteParser {

    public static void main(final String[] args) throws IOException {
        final var instance = new QuoteParser();
        final var owners = instance.parse();
        final var classPathResource = Objects.requireNonNull(QuoteParser.class.getClassLoader().getResource(
            "quotes.sql")).getPath();
        for (final Owner owner : owners) {
            log.debug("{}", owner);
            final var statement = QuoteParser.prepareInsertStatement(owner);
            writeQuotesToResource(statement, Path.of(classPathResource));
        }
    }

    private List<Owner> parse() throws IOException {
        final XmlMapper mapper = new XmlMapper();
        final var path = Objects.requireNonNull(getClass().getClassLoader().getResource("quotes.xml")).getPath();
        final QuoteHolder value = mapper.readValue(new FileInputStream(path), QuoteHolder.class);

        return value.getOwners();
    }

    private static String prepareInsertStatement(final Owner owner) {

        final var insertPattern = "('%s', '%s', '%s')";
        final var builder =
                new StringBuilder("INSERT INTO telegram_bots.quotes (content, author, language) VALUES \n");

        builder.append(owner.getQuotes().stream()
                             .map(quote -> String.format(insertPattern,
                                                         sanitizeInsertValue(quote),
                                                         sanitizeInsertValue(owner.getName()),
                                                         "ua"))
                             .collect(Collectors.joining(",\n")));

        builder.append(";\n\n");
        return builder.toString();
    }

    private static void writeQuotesToResource(final String insertStm, final Path path) throws
            IOException {
        final byte[] strToBytes = insertStm.getBytes();
        Files.write(path, strToBytes, StandardOpenOption.APPEND);
    }

    private static String sanitizeInsertValue(final String value) {
        final var quoteReplace = StringUtils.replace(value, "'", "''");
        return StringUtils.replace(quoteReplace, "$", "[$]");
    }

}
