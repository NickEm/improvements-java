package com.nickem.mapper;

import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;
import com.nickem.domain.CreditCard;
import com.nickem.domain.LastPassEntry;

public class LastPassRecordMapper implements CsvRecordMapper<LastPassEntry> {

    private static final String TYPE = "Type";
    private static final String NUMBER = "Number";
    private static final String NAME = "Name";
    private static final String CVV = "CVV";
    private static final String EXPIRATION = "Expiration";

//    String url;
//    String username;
//    String password;
//    String totp;
//    String extra;
//    String name;
//    String grouping;
//    String fav;

    public LastPassEntry map(String[] fields, CsvColumnMapper mapper) {
        return LastPassEntry.builder()
            .url(fields[mapper.getIndexByFieldName("url")])
            .username(fields[mapper.getIndexByFieldName("username")])
            .password(fields[mapper.getIndexByFieldName("password")])
            .totp(fields[mapper.getIndexByFieldName("totp")])
            .extra(fields[mapper.getIndexByFieldName("extra")])
            .extra(fields[mapper.getIndexByFieldName("extra")])
            .name(fields[mapper.getIndexByFieldName("name")])
            .grouping(fields[mapper.getIndexByFieldName("grouping")])
            .fav(fields[mapper.getIndexByFieldName("fav")])
            .build();
    }

}
