package com.nickem.mapper;

import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;
import com.nickem.domain.ElectricityEntry;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ElectricityEntryMapper implements CsvRecordMapper<ElectricityEntry> {

    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    private static final String daytime = "daytime";
    private static final String day = "day";
    private static final String night = "night";

    @Override
    public ElectricityEntry map(String[] fields, CsvColumnMapper mapper) {
        final String stringDaytime = fields[mapper.getIndexByFieldName(daytime)];

        final LocalDate date = LocalDate.parse(stringDaytime, FORMATTER);

        return ElectricityEntry.builder()
            .daytime(date)
            .dayValue(Integer.parseInt(fields[mapper.getIndexByFieldName(day)]))
            .nightValue(Integer.parseInt(fields[mapper.getIndexByFieldName(night)]))
            .build();
    }
}
