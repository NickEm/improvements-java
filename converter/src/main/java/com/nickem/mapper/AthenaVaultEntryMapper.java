package com.nickem.mapper;

import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;
import com.nickem.domain.AthenaVaultEntry;

public class AthenaVaultEntryMapper implements CsvRecordMapper<AthenaVaultEntry> {

    private static final String TIMESTAMP = "timestamp";
    private static final String INFRA_ENV = "infra_env";
    private static final String DATA_ENVIRONMENT = "data_environment";
    private static final String MESSAGE = "message";

    @Override
    public AthenaVaultEntry map(String[] fields, CsvColumnMapper mapper) {
        return AthenaVaultEntry.builder()
            .timestamp(fields[mapper.getIndexByFieldName(TIMESTAMP)])
//            .infraEnv(fields[mapper.getIndexByFieldName(INFRA_ENV)])
//            .dataEnvironment(fields[mapper.getIndexByFieldName(DATA_ENVIRONMENT)])
            .message(fields[mapper.getIndexByFieldName(MESSAGE)])
            .build();
    }
}
