package com.nickem.mapper;

import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;
import com.nickem.domain.AthenaTerminatorEntry;

public class AthenaTerminatorEntryMapper implements CsvRecordMapper<AthenaTerminatorEntry> {

    private static final String TIMESTAMP = "time";
    private static final String TENANT = "tenant";
    private static final String B3_TRACE_ID = "b3_trace_id";

    @Override
    public AthenaTerminatorEntry map(String[] fields, CsvColumnMapper mapper) {
        return AthenaTerminatorEntry.builder()
            .timestamp(fields[mapper.getIndexByFieldName(TIMESTAMP)])
            .tenant(fields[mapper.getIndexByFieldName(TENANT)])
            .b3TraceId(fields[mapper.getIndexByFieldName(B3_TRACE_ID)])
            .build();
    }
}
