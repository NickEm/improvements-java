package com.nickem.mapper;

import com.nickem.csv.CsvColumnMapper;
import com.nickem.csv.CsvRecordMapper;
import com.nickem.domain.RecipientStatus;
import com.nickem.domain.UserRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserRequestMapper implements CsvRecordMapper<UserRequest> {

    @Override
    public UserRequest map(String[] fields, CsvColumnMapper mapper) {
        try {
            return UserRequest.builder()
                .actionState(fields[mapper.getIndexByFieldName("ACTIONSTATE")])
                .botName(fields[mapper.getIndexByFieldName("BOTNAME")])
                .chatId(Long.parseLong(fields[mapper.getIndexByFieldName("CHATID")]))
                .language(fields[mapper.getIndexByFieldName("LANGUAGE")])
                .recipient(fields[mapper.getIndexByFieldName("RECIPIENT")])
                .status(RecipientStatus.valueOf(fields[mapper.getIndexByFieldName("STATUS")]))
                .build();
        } catch (Exception e) {
            log.error("Error with parsing record", e);
            throw e;
        }
    }

}
