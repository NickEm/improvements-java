package com.nickem;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.domain.RandomUser;
import com.nickem.domain.RecipientStatus;
import com.nickem.domain.UserRequest;
import com.nickem.mapper.UserRequestMapper;
import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class UserRequestProcessor {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    public static final TypeReference<List<RandomUser>> TYPE_REFERENCE = new TypeReference<>() {};

    public static final List<String> LANGUAGES = List.of("uk", "en", "es", "pt", "zh", "hi", "bn", "ar", "he");

    public static final List<String> ACTION_STATES = List.of("read", "write", "contribute", "observe", "generate",
        "moderate", "administrator", "support", "validate", "maintenance");


    /* To get random data https://random-data-api.com/documentation */
    /* To replace `sed -zi 's/ACTIONSTATE,BOTNAME,CHATID,LANGUAGE,RECIPIENT,STATUS\n//g' converter/src/main/resources/users/400000-users.csv` */
    /* To replace `sed -zi 's/ACTIONSTATE,BOTNAME,CHATID,LANGUAGE,RECIPIENT,STATUS\n//g' converter/src/main/resources/users/200K-users.csv` */
    /* To replace `sed -zi 's/ACTIONSTATE,BOTNAME,CHATID,LANGUAGE,RECIPIENT,STATUS\n//g' converter/src/main/resources/users/100K-users.csv` */
//

    public static void main(String[] args)
        throws IOException, CsvRequiredFieldEmptyException, CsvDataTypeMismatchException, URISyntaxException, InterruptedException {
        final UserRequestProcessor instance = new UserRequestProcessor();
        int maxItems = 1_000_000;

        List<String> filePaths = List.of("users/100K-users.csv", "users/200K-users.csv", "users/300K-1-users.csv",
            "users/300K-users.csv", "users/400K-users.csv");

        final Set<UserRequest> result = new TreeSet<>(Comparator.comparing(UserRequest::getRecipient));
        for (String filePath : filePaths) {
            result.addAll(instance.readUsersFromFileWithCsv(filePath));
        }
        log.info("User list with size {} is parsed.", result.size());

        final HttpClient httpClient = HttpClient.newHttpClient();

        int pageSize = 100;
        int currentItems = 0;
        AtomicLong chatId = new AtomicLong(0L);
        while (currentItems < maxItems) {
            final List<RandomUser> randomUsers = getRandomUsers(httpClient, pageSize);

            for (RandomUser e : randomUsers) {
                final String name = prepareName(e);
                final String botname = StringUtils.abbreviate(e.getEmployment().getTitle(), 64);
                final UserRequest userRequest = instance.generateUserRequestBasedOnPosition(name, botname, chatId);
                result.add(userRequest);
            }
            currentItems = result.size();
            log.info("Current size is [{}].", currentItems);
        }
        log.info("Needed size is reached.");

        final AtomicLong chatIdIndex = new AtomicLong(0L);
        final List<UserRequest> finalResult = result.parallelStream()
            .map(e -> e.toBuilder().chatId(chatIdIndex.incrementAndGet()).build())
            .collect(Collectors.toList());

        Path filePath = getFileToWrite(maxItems);
        try (Writer writer = new FileWriter(filePath.toFile(), true)) {
            StatefulBeanToCsv<UserRequest> beanToCsv = new StatefulBeanToCsvBuilder<UserRequest>(writer)
                .withQuotechar(Character.MIN_VALUE)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();

            beanToCsv.write(finalResult.iterator());
        }
        log.info("User list write to file is finished.");

    }

    public UserRequest generateUserRequestBasedOnPosition(String name, String botname, AtomicLong chatId) {
        return UserRequest.builder()
                        .botName(botname)
                        .recipient(name)
                        .chatId(chatId.incrementAndGet())
                        .language(prepareLanguage())
                        .status(RecipientStatus.ACTIVE)
                        .actionState(prepareActionState())
                        .build();
    }

    private static int writeToFile(int maxItems, Path filePath, int currentItems, List<UserRequest> resultEntities)
        throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        try (Writer writer = new FileWriter(filePath.toFile(), true)) {
            StatefulBeanToCsv<UserRequest> beanToCsv = new StatefulBeanToCsvBuilder<UserRequest>(writer)
                .withQuotechar(Character.MIN_VALUE)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .build();

            beanToCsv.write(resultEntities);
            currentItems += resultEntities.size();
            log.info("Processed [{}] items out of [{}]", currentItems, maxItems);
        }
        return currentItems;
    }

    private List<UserRequest> readUsersFromFile(String filePath) {
//        final String inputName = "users/200K-users.csv";
        log.info("User list parsing for file [{}] has started...", filePath);
        final UserRequestMapper recordMapper = new UserRequestMapper();
        final CsvParser<UserRequest> csvParser = new CsvParser<>(recordMapper);
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final InputStream input = classLoader.getResourceAsStream(filePath);
        final CsvParseResult<UserRequest> result = csvParser.parseFile(input);
        log.info("User list with size {} is parsed.", result.entities().size());
        return result.entities();
    }

    private List<UserRequest> readUsersFromFileWithCsv(String filePath) {
        log.info("User list parsing for file [{}] has started...", filePath);
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final InputStream input = classLoader.getResourceAsStream(filePath);
        List<UserRequest> result = new CsvToBeanBuilder(new InputStreamReader(input))
            .withType(UserRequest.class)
            .withSeparator(',')
            .build()
            .parse();
        log.info("User list with size {} is parsed.", result.size());
        return result;
    }

    private static String prepareName(RandomUser e) {
        return e.getFirstName() + StringUtils.SPACE + e.getLastName();
    }

    private static String prepareLanguage() {
        final Random random = new Random();
        return LANGUAGES.get(random.nextInt(LANGUAGES.size()));
    }

    private static String prepareActionState() {
        final Random random = new Random();
        return ACTION_STATES.get(random.nextInt(ACTION_STATES.size()));
    }

    private static Path getFileToWrite(int number) {
        Path filePath = Paths.get("converter/src/main/resources/users/%s-users.csv".formatted(number));
        Path parentDir = filePath.getParent();
        try {
            if (!Files.exists(parentDir)) {
                Files.createDirectories(parentDir);
            }
        } catch (Exception e) {
            log.error("Exception while creating target file");
        }
        return filePath;
    }

    private static List<RandomUser> getRandomUsers(final HttpClient httpClient, final int pageSize)
        throws InterruptedException {
        try {
            final HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://random-data-api.com/api/v2/users?size=%s".formatted(pageSize)))
                .GET()
                .build();

            HttpResponse<String> response = httpClient.send(request, BodyHandlers.ofString());
            return OBJECT_MAPPER.readValue(response.body(), TYPE_REFERENCE);
        } catch (URISyntaxException| IOException| InterruptedException e) {
            log.error("Exception raised", e);
            Thread.sleep(1000);
        }
        return Collections.emptyList();
    }

}
