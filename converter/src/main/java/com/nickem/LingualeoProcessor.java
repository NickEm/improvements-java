package com.nickem;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nickem.domain.LingualeoPage;
import com.nickem.domain.LingualeoPage.Word;
import com.nickem.domain.LingualeoResult;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import lombok.SneakyThrows;

public class LingualeoProcessor {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final String FILENAME_PATTERN = "lingualeo/data (%s).json";
    static {
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public static final TypeReference<LingualeoPage> TYPE_REF = new TypeReference<>() {};

    @SneakyThrows
    public static void main(String[] args) {

        List<String> fileNames = new ArrayList<>();
        for (int i = 1; i < 49; i++) {
            fileNames.add(String.format(FILENAME_PATTERN, i));
        }
        final LingualeoProcessor instance = new LingualeoProcessor();
        final List<Word> words = instance.process(fileNames);
        System.out.printf("List size: [%s]", words.size());
        instance.dumpResult(words);
    }

    @SneakyThrows
    private void dumpResult(List<Word> words) {
        final FileOutputStream outputStream = new FileOutputStream(
            getClass().getClassLoader().getResource("lingualeo/data-dump.json").getPath());
        OBJECT_MAPPER.writeValue(outputStream, new LingualeoResult(words));
    }

    public List<Word> process(final List<String> fileNames) {
        return fileNames.stream()
            .map(this::process)
            .flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<Word> process(final String fileName) {
        LingualeoPage page = OBJECT_MAPPER.readValue(getClass().getClassLoader().getResourceAsStream(fileName), TYPE_REF);
        return page.getData().stream().flatMap(e -> e.getWords().stream()).collect(Collectors.toList());
    }
}
