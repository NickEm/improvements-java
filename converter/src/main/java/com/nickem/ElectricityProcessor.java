package com.nickem;

import com.nickem.csv.CsvParseResult;
import com.nickem.csv.CsvParser;
import com.nickem.domain.ElectricityEntry;
import com.nickem.mapper.ElectricityEntryMapper;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ElectricityProcessor {

    public static void main(String[] args) {
        final ElectricityEntryMapper recordMapper = new ElectricityEntryMapper();
        final CsvParser<ElectricityEntry> csvParser = new CsvParser<>(recordMapper);

        final ElectricityProcessor instance = new ElectricityProcessor();
        final ClassLoader classLoader = instance.getClass().getClassLoader();
        final String inputName = "csv/electricity.csv";
        final File input = new File(classLoader.getResource(inputName).getPath());
        final CsvParseResult<ElectricityEntry> result = csvParser.parseFile(input);
        final TreeMap<LocalDate, ElectricityEntry> collect = result.entities().stream()
            .collect(Collectors.toMap(ElectricityEntry::getDaytime, e -> e, (o, n) -> n, TreeMap::new));
        instance.fillGapsWithAverageValues(collect);
        final File output = new File(classLoader.getResource("csv/electricity-backfill.csv").getPath());
        instance.writeToCsv(collect, output);
    }

    private void fillGapsWithAverageValues(TreeMap<LocalDate, ElectricityEntry> treeMap) {
        final TreeMap<LocalDate, ElectricityEntry> gapsMap = new TreeMap<>();
        final Iterator<Entry<LocalDate, ElectricityEntry>> entryIterator = treeMap.entrySet().iterator();
        Entry<LocalDate, ElectricityEntry> prev = entryIterator.next();
        while (entryIterator.hasNext()) {
            final Entry<LocalDate, ElectricityEntry> current = entryIterator.next();
            /*Will work only for difference in 2 monthes*/
            if (current.getKey().getMonthValue() - prev.getKey().getMonthValue() > 1) {
                final LocalDate middleMonth = LocalDate.of(prev.getKey().getYear(),
                    (current.getKey().getMonthValue() + prev.getKey().getMonthValue()) / 2, 1);
                final int dayValue = (current.getValue().getDayValue() + prev.getValue().getDayValue()) / 2;
                final int nightValue = (current.getValue().getNightValue() + prev.getValue().getNightValue()) / 2;
                final ElectricityEntry entry = ElectricityEntry.builder()
                    .daytime(middleMonth)
                    .dayValue(dayValue)
                    .nightValue(nightValue)
                    .build();

                gapsMap.put(middleMonth, entry);
                prev = current;
            }
        }

        log.info("Gaps map is {}", gapsMap);
        treeMap.putAll(gapsMap);
    }

    private void writeToCsv(TreeMap<LocalDate, ElectricityEntry> treeMap, final File output) {
        try (
            CSVWriter writer = new CSVWriter(new FileWriter(output))) {
            for (ElectricityEntry value : treeMap.values()) {
                String[] entry = new String[3];
                entry[0] = value.getDaytime().format(ElectricityEntryMapper.FORMATTER);
                entry[1] = String.valueOf(value.getDayValue());
                entry[2] = String.valueOf(value.getNightValue());

                writer.writeNext(entry);
            }
            writer.flush();
        } catch (IOException e){
            log.warn("Exception is raised on write", e);
        }
    }

    /*
    *
    * 2023-03-12 18:54
    * 18406.92
    * 8205.33
    * 26612.25
    *
    * 2023-03-9 12:25
    * 18371.79
    * 8195.74
    * 26567.55
    *
    * difference 78.5 - 0.57KW/h
    * 35.13
    * 9.59
    * 44.7
    *
    * */

}
