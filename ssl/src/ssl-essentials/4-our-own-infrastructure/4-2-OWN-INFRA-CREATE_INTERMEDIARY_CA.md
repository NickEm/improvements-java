#### Creating our own CA 

To create directory tree
```shell
mkdir certs crl csr newcerts private
```

To generate intermediate key for CA
```shell
openssl genrsa -aes256 -out ./intermediary/private/intermediate.key 4096
```
this certificate should have very low restricted permission on the file system. This mean can be red only by the owner
```shell
chmod 400 ./intermediary/private/intermediate.key
``` 

To generate intermediate Certificate Signing Requests 
```shell
openssl req -config ./intermediary/openssl_intermediary.cnf -new -sha256 -key ./intermediary/private/intermediate.key -out ./intermediary/csr/intermediate.csr
```

To sign key with root CA 
```shell
openssl ca -config openssl_ca.cnf -extensions v3_intermediate_ca -days 3650 -notext -md sha256 \
  -in ./intermediary/csr/intermediate.csr -out ./intermediary/certs/intermediate.crt
```

To read certificate details (useful for debugging)
```shell
openssl x509 -in ./intermediary/certs/intermediate.crt -noout -text
```

Let's restrict intermediate certificate to be red only for everyone, but not modified 
```shell
chmod 400 ./intermediary/certs/intermediate.crt
``` 
