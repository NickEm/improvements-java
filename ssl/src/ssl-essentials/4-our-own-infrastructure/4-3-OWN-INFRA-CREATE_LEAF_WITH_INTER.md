#### Creating our own CA 

To create directory tree
```shell
mkdir certs csr private
```

To generate intermediate key for CA
```shell
openssl genrsa -out ./leaf/private/quotes-generator.net.key 2048
```
this certificate should have very low restricted permission on the file system. This mean can be red only by the owner
**NOTE**: We don't need to password protect our certificate


To generate leaf Certificate Signing Requests
```shell
openssl req -config ./leaf/quotes-generator.net.cnf -new -sha256 -key ./leaf/private/quotes-generator.net.key -out ./leaf/csr/quotes-generator.net.csr
```

To sign leaf key with intermediate CA
```shell
openssl ca -config ./intermediary/openssl_intermediary.cnf -extensions server_cert -days 730 -notext -md sha256 \
  -in ./leaf/csr/quotes-generator.net.csr -out ./leaf/certs/quotes-generator.net.crt
```

**Leaf certificate isn't signed by root CA, but by intermediate, so can't be used on it's own in the server. 
It needs to be bundled with intermediate, and bundled result should be put to the server. this is Chain Of Trust**
```shell
cat ./leaf/certs/quotes-generator.net.crt ./intermediary/certs/intermediate.crt > ./server-certs/quotes-generator.net-with-inter.crt
```

We need to add to the server bundled leaf with intermediate and private key for leaf cert to serve TLS correctly. 
