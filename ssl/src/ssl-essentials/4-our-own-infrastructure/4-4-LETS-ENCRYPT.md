#### [Working with Let's Encrypt certbot](https://certbot.eff.org/instructions)

Installation
```shell
sudo apt-get install certbot
```

We can also install some integration for service if supported. Tomcat wasn't supported 
```shell
sudo apt-get install python3-certbot-apache
```

```shell
sudo certbot certonly --standalone -d <your-domain.com>
```

We need to add to our server this generated certificate, as output we got paths to generated certificates. 
