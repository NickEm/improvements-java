#### Creating our own CA 

To generate key for CA
```shell
openssl genrsa -aes256 -out ./private/ca.key 4096
```
NOTE: Passphrase is standard for `ssl`.

this certificate should have very low restricted permission on the file system. This mean can be red only by the owner  
```shell
chmod 400 ./private/ca.key
```

To generate CA
```shell
openssl req -config openssl_ca.cnf -key ./private/ca.key -extensions v3_ca -sha256 -days 7300 -new -x509 -out certs/ca.crt
```