
##### To generate self-signed certificate
```shell
openssl req -x509 -sha256 -days 365 -newkey rsa:4096 -keyout ./private.key -out ./certificate.crt
```

```shell
openssl req -x509 -sha256 -subj "/CN=${SERVER_CN}" -nodes -days 365 -newkey rsa:4096 -keyout ./private.key -out ./certificate.crt 
```
* `subj` - if we don't specify we would be asked for Common Name (it's required) and other details
* `nodes` - says we don't want passphrase for private key generated

##### To read the certificate
```shell
openssl x509 -in ./certificate.crt -text -noout
```
