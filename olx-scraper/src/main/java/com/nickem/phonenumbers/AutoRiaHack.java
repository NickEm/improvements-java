package com.nickem.phonenumbers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mykola Morozov
 */
public class AutoRiaHack {

    private static final String DETAILS_PAGE_PATTERN = "https://c-ua1.riastatic.com/demo/bu/searchPage/v2/view/auto/%s/%s/%s?lang_id=2";
    private static final String PAGE_PATTERN = "https://auto.ria.com/blocks_search_ajax/search/?state%5B0%5D=5&s_yers%5B0%5D=0&po_yers%5B0%5D=0&currency=1&countpage=10&page=";
    private static final String PAGE_PATTERN_OPEL = "https://auto.ria.com/blocks_search_ajax/search/?noCache=1&searchType=&countpage=10&bodystyle[0]=3&s_yers[0]=2010&po_yers[0]=2014&category_id=1&marka_id[0]=56&model_id[0]=3121&price_do=14000&currency=1&gearbox[0]=1&custom=1&abroad=2&user_id=5422194";

    public static void main(String[] args) {
//        makeRequest();
//        File inputFile = new File("unique_phone_book.txt");
//        File outputFile = new File("cleaned_olx_phone_book.txt");
//        NumberCleaner.cleanPhoneNumbers(inputFile, outputFile);

    }

    private void makeRequest() {
        Integer pageCount = 2524;
        File phoneBook = new File("autoria_phone_book.txt");
        for (int pageNumber = 0; pageNumber <= pageCount; pageNumber++) {
            try {

                URL url = new URL(PAGE_PATTERN + pageNumber);
                URLConnection connection = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String result = StringUtils.EMPTY;
                String line;

                while ((line = in.readLine()) != null) {
                    result += line;
                }
                in.close();
                parsePageResult(phoneBook, result);
                System.out.println("Page number is " + pageNumber);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void parsePageResult(File phoneBook, String jsonObj) {
        JSONObject obj = new JSONObject(jsonObj);
        JSONObject ids = obj.getJSONObject("result").getJSONObject("search_result");

        JSONArray arr = ids.getJSONArray("ids");
        Set<String> phoneNumbers = new TreeSet<>();
        for (int i = 0; i < arr.length(); i++) {
            makeDetailsCall(phoneNumbers, arr.get(i).toString());
        }

        try (PrintWriter writer = new PrintWriter(new FileWriter(phoneBook, true))) {
            phoneNumbers.stream().forEach(e -> {
                String phone = e.replaceAll("^38", StringUtils.EMPTY);
                phone = phone.replace("+38", StringUtils.EMPTY);
                phone = phone.replaceAll(" ", StringUtils.EMPTY);
                phone = phone.replaceAll("-", StringUtils.EMPTY);
                phone = phone.replaceAll("\\(", StringUtils.EMPTY);
                phone = phone.replaceAll("\\)", StringUtils.EMPTY);
                writer.println(phone);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String roundForAuto(String number) {
        boolean increment = false;
        number = number.substring(0, number.length() - 1);
        String strDecisionToRound = number.substring(number.length() - 1, number.length());
        Integer decisionToRound = Integer.parseInt(strDecisionToRound);
        if (decisionToRound > 4) {
            increment = true;
        }
        String strNumberToRound = number.substring(number.length() - 2, number.length() - 1);
        String coreNumber = number.substring(0, number.length() - 2);
        Integer numberToRound = Integer.parseInt(strNumberToRound);
        if (increment) {
            numberToRound++;
        }

        return coreNumber + numberToRound;
    }

    private void makeDetailsCall(Set<String> phoneNumbers, String number) {
        try {
            String numberRounded = roundForAuto(number);

            String numberDoubleRounded = roundForAuto(numberRounded);
            String urlStr = String.format(DETAILS_PAGE_PATTERN, numberDoubleRounded, numberRounded, number);
            URL url = new URL(urlStr);
            URLConnection connection = url.openConnection();
            BufferedReader in;
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = StringUtils.EMPTY;
            String line;

            while ((line = in.readLine()) != null) {
                result += line;
            }

            JSONObject obj = new JSONObject(result);
            JSONObject userPhone = obj.getJSONObject("userPhoneData");
            phoneNumbers.add(
                userPhone.get("phoneId") != null ? userPhone.get("phoneId").toString() : StringUtils.EMPTY);
            phoneNumbers.add(userPhone.get("phone") != null ? userPhone.get("phone").toString() : StringUtils.EMPTY);
            in.close();
            Thread.sleep(50);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*https://auto.ria.com/blocks_search_ajax/search/?state%5B0%5D=5&s_yers%5B0%5D=0&po_yers%5B0%5D=0&currency=1&countpage=10&page=2*/
    /*https://auto.ria.com/blocks_search_ajax/search/?state[0]=5&s_yers[0]=0&po_yers[0]=0&currency=1&marka_id[0]=0&model_id[0]=0&countpage=10*/
}
