package com.nickem.phonenumbers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Mykola Morozov
 */
public class OlxHack {

    private static final String PHONE_PATTERN = "http://olx.ua/uk/ajax/misc/contact/phone/%s/";
    private static final String PAGE_PATTERN = "http://olx.ua/uk/lvov/?page=%s";
    /*private static final Pattern COUPLE_OF_NUMBER_REGEXP = Pattern.compile("<span.*>(.+)</span>");*/
    /*private static final Pattern COUPLE_OF_NUMBER_REGEXP = Pattern.compile("(<span\\sclass=\"block\">(.*?)</span>)");*/
    private static final Pattern COUPLE_OF_NUMBER_REGEXP = Pattern.compile("<spanclass=\"block\">(.*?)<\\/span>");
    private static final Pattern SINGLE_NUMBER_REGEXP = Pattern.compile("\\{\"value\":\"(.+)\"\\}");
    private static final Pattern URL_REGEXP = Pattern.compile("http://olx.ua/uk/obyavlenie/.*ID(.*).html#");


    public static void main(String[] args) {
//        makeTheRequest();
        NumberCleaner.filterUniquePhoneNumbers(new File("filtered_phones_result.txt"));
//        compilePattern();
    }

    private void compilePattern() {
        Set<String> phoneNumbers = new TreeSet<>();
        BufferedReader in;
        try {
            in = new BufferedReader(new FileReader(new File("file_to_parse.txt")));

            String line;

            while ((line = in.readLine()) != null) {
                Matcher coupleMatcher = COUPLE_OF_NUMBER_REGEXP.matcher(line);
                while (coupleMatcher.find()) {
                    phoneNumbers.add(coupleMatcher.group(1));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        phoneNumbers.stream().map(this::fixNumber).filter(StringUtils::isNotBlank).forEach(System.out::println);

    }

    private void makeTheRequest() {
        Integer pageCount = 500;
        for(int pageNumber = 1; pageNumber <= pageCount; pageNumber++) {
            File phoneBook = new File("phone_book.txt");
            Set<String> advertisementIds = new TreeSet<>();
            Set<String> phoneNumbers = new TreeSet<>();
            try {
                URL url = new URL(String.format(PAGE_PATTERN, pageNumber));
                URLConnection connection = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;

                while ((line = in.readLine()) != null) {
                    Matcher matcher = URL_REGEXP.matcher(line);
                    if (matcher.find()) {
                        advertisementIds.add(matcher.group(1));
                    }
                }
                in.close();

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                for (String id : advertisementIds) {
                    URL url = new URL(String.format(PHONE_PATTERN, id));
                    URLConnection connection = url.openConnection();
                    BufferedReader in;
                    try {
                        in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    } catch (IOException e) {
                        continue;
                    }
                    String line;

                    while ((line = in.readLine()) != null) {
                        Matcher coupleMatcher = COUPLE_OF_NUMBER_REGEXP.matcher(line);
                        Matcher singleMatcher = SINGLE_NUMBER_REGEXP.matcher(line);
                        while (coupleMatcher.find()) {
                            phoneNumbers.add(coupleMatcher.group(1));
                        }
                        if (singleMatcher.find()) {
                            phoneNumbers.add(singleMatcher.group(1));
                        }

                    }
                    in.close();
                    Thread.sleep(200);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            /*System.out.println(phoneNumbers.size());*/
            try (PrintWriter writer = new PrintWriter(new FileWriter(phoneBook, true))) {
                phoneNumbers.stream().forEach(e -> {
                    String phone = e.replaceAll("^38", StringUtils.EMPTY);
                    phone = phone.replace("+38", StringUtils.EMPTY);
                    phone = phone.replaceAll(" ", StringUtils.EMPTY);
                    phone = phone.replaceAll("-", StringUtils.EMPTY);
                    phone = phone.replaceAll("\\(", StringUtils.EMPTY);
                    phone = phone.replaceAll("\\)", StringUtils.EMPTY);
                    writer.println(phone);
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(String.format("Retrieving page number: %s", pageNumber));
        }
//        phoneNumbers.stream().forEach(System.out::println);
    }

    private String fixNumber(String phone) {
        phone = phone.replaceAll("^38", StringUtils.EMPTY);
        phone = phone.replace("+38", StringUtils.EMPTY);
        phone = phone.replace("+", StringUtils.EMPTY);
        phone = phone.replaceAll(" ", StringUtils.EMPTY);
        phone = phone.replaceAll("-", StringUtils.EMPTY);
        phone = phone.replaceAll("\\(", StringUtils.EMPTY);
        phone = phone.replaceAll("\\)", StringUtils.EMPTY);

        if (phone.length() < 7 ) {
            return StringUtils.EMPTY;
        } else {
            return phone;
        }
    }
}
