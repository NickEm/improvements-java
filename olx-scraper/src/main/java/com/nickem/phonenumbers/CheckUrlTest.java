package com.nickem.phonenumbers;

import com.google.common.collect.Iterables;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse.BodyHandlers;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;

@Slf4j
public class CheckUrlTest {

    public static void main(String[] args) throws Exception {
        final int partitionSize = 64;

        final HttpClient client = HttpClient.newHttpClient();
        final ExecutorService executorService = Executors.newFixedThreadPool(partitionSize);

        final long startTime = System.nanoTime();

        /*243_500_000; i < 243_800_000*/
        /*242_000_000; i < 243_000_000*/
        try {
            List<CompletableFuture<Pair<String, Integer>>> futures = new ArrayList<>();
            for (int i = 242_000_000; i < 243_000_000; i++) {
                final HttpRequest request = HttpRequest.newBuilder()
                    .uri(new URI("https://olx-ua.pollychenniie.store/" + i))
                    .GET()
                    .version(HttpClient.Version.HTTP_1_1)
                    .build();

                futures.add(CompletableFuture.supplyAsync(() -> loadCall(client, request), executorService));
            }

            final Iterable<List<CompletableFuture<Pair<String, Integer>>>> partitions = Iterables.partition(futures, partitionSize);
            log.info("There are #{} of tasks divided on #{} results in #{} partitions", futures.size(), partitionSize, futures.size() / partitionSize);
            int count = 1;
            for (List<CompletableFuture<Pair<String, Integer>>> partition : partitions) {
                final long startPartitionTime = System.nanoTime();
                CompletableFuture.allOf(partition.toArray(new CompletableFuture[0])).join();
//                log.info("Execution of partition [{}]: {}", count, findTimeLap(startPartitionTime, System.nanoTime()));
                count++;
            }
        } finally {
            log.info("Execution totally took time: {}", findTimeLap(startTime, System.nanoTime()));
            if (!executorService.awaitTermination(1, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        }

    }

    @SneakyThrows
    private static Pair<String, Integer> loadCall(final HttpClient client, final HttpRequest request) {
        final int status = client.send(request, BodyHandlers.ofString()).statusCode();
        if (status == 200) {
            log.info("Exists [{}]", request.uri().toString());
        }
        return Pair.of(request.uri().toString(), status);
    }

    private static double findTimeLap(final long startTime, final long endTime) {
        final double average = ((double) (endTime - startTime)) / 1_000_000_000;
        BigDecimal bd = BigDecimal.valueOf(average);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
