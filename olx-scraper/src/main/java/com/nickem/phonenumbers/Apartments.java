package com.nickem.phonenumbers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Mykola Morozov
 */
public class Apartments {

    private static final String BASE_URI = "https://www.olx.ua/uk/ajax/search/list/";

    private static final Pattern COUPLE_OF_NUMBER_REGEXP = Pattern.compile("<spanclass=\"block\">(.*?)<\\/span>");
    private static final Pattern SINGLE_NUMBER_REGEXP = Pattern.compile("\\{\"value\":\"(.+)\"\\}");
    private static final Pattern URL_REGEXP = Pattern.compile("http://olx.ua/uk/obyavlenie/.*ID(.*).html#");

//    Знайдений 7 оголошень

    public static void main(String[] args) throws IOException {
//        view:
//min_id:
//q: Перфецького
//search[city_id]: 176
//search[region_id]: 0
//search[district_id]:
//search[dist]: 0
//search[category_id]: 1600

        Map<String, String> params = new HashMap<>();
        params.put("q", "Перфецького");
        params.put("search[city_id]", "176");
        params.put("search[region_id]", "0");
        params.put("search[dist]", "0");
        params.put("search[category_id]", "1600");

        Document doc = Jsoup.connect(BASE_URI).data(params).post();
//        Elements links = doc.select("a[href]");
//        Elements media = doc.select("[src]");
//        Elements imports = doc.select("link[href]");

//        Element offerTable = doc.getElementById("offers_table");
        Elements advertisements = doc.getElementsByAttributeValue("summary", "Оголошення");
//        final Elements offers = offerTable.getElementsByTag("tr");

//        This is for Знайдений 7 оголошень
        Elements countEl = doc.getElementsByAttributeValue("data-cy", "search_results_info_results_count");
        for (Element element : countEl) {
            final Element count = element.selectFirst("h2");
        }
//        countEl
//           <h2>Знайдений 7 оголошень</h2>
    }
}
