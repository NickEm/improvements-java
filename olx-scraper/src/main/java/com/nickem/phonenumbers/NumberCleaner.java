package com.nickem.phonenumbers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Mykola Morozov
 */
public class NumberCleaner {

    public static void cleanPhoneNumbers(File inputFile, File outputFile) {
        Set<String> resultSet = new TreeSet<>();
        try (BufferedReader in = new BufferedReader(new FileReader(inputFile))) {
            String line;
            while ((line = in.readLine()) != null) {
                if(line.length() >= 7){
                    if(line.contains("50") || line.contains("63") || line.contains("68") || line.contains("93")
                            || line.contains("96") || line.contains("97") || line.contains("98") || line.contains("99")) {
                        resultSet.add(line);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (PrintWriter writer = new PrintWriter(new FileWriter(outputFile, true))) {
            resultSet.stream().forEach(writer::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void filterUniquePhoneNumbers(File outputFile) {
        Set<String> resultSet = new TreeSet<>();
        putLinesFromFileToSet(resultSet, new File("cleaned_autoria_phone_book.txt"));
        putLinesFromFileToSet(resultSet, new File("cleaned_olx_phone_book.txt"));

        try (PrintWriter writer = new PrintWriter(new FileWriter(outputFile, true))) {
            resultSet.stream().forEach(writer::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void putLinesFromFileToSet(Set<String> set, File inputFile) {
        try (BufferedReader in = new BufferedReader(new FileReader(inputFile))) {
            String line;
            while ((line = in.readLine()) != null) {
                set.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
